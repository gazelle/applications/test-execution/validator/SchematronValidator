## @file transformer.py
#  @author Abderrazek Boufahja
#  @brief replace the namespace of all schematrons


import dircache

list_output = dircache.listdir("./ihe_pcc_all/")
for outp in list_output:
    if len(outp.split("."))>1:
        if outp.split(".")[1] == "sch":
            fich = open("./ihe_pcc_all/" + outp,'r');
            fill = fich.read()
            fill = fill.replace("schema xmlns=\"http://www.ascc.net/xml/schematron\"","schema xmlns=\"http://purl.oclc.org/dsdl/schematron\"")
            fich.close()
            fich = open("./ihe_pcc_all/" + outp,'w')
            fich.write(fill)
            fich.close()
                        
            



