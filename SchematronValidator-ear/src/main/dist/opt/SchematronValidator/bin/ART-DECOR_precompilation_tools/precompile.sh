#!/bin/bash
#$1 = /opt/SchematronValidator_prod/bin/ART-DECOR_precompilation_tools
#$2 = /opt/SchematronValidator_prod/bin/schematron/arsenal/Test_SAR_specialistica/_pre_compilation
#$3 = nom du fichier .sch
#Copy all xsl and jar from ART-DECOR_precompilation_tools to _pre_compilation
cp -r $1/*.xsl $1/*.jar $2
#Create _pre_compilation folder if necessary
if [ ! -d "$2" ]; then
        echo "Creating directory $2"
        mkdir $2
fi
#Move to precompilation folder
cd $2
#Set ROOT_PATH as the current folder
ROOT_PATH=`pwd`

echo "<File>"$3"<\\File>" >> /tmp/log_schema.log
echo "<Operation1>precompilation du schematron : collage des include<\\Operation1>" >> /tmp/log_schema.log
#"Flatenize" the schematron in colle.sch -> colle.sch SHALL contain all dependencies of the schematron file
java -cp saxon9he.jar net.sf.saxon.Transform -s:$3 -xsl:iso_dsdl_include.xsl -o:colle.sch >> /tmp/log_schema.log
echo "<Operation2>precompilation du schematron : expansion des abstract<\\Operation2>" >> /tmp/log_schema.log
#Expand abstracts in the copy of the file
java -cp saxon9he.jar net.sf.saxon.Transform -s:colle.sch -xsl:iso_abstract_expand.xsl -o:$3 >> /tmp/log_schema.log
sed -i "s;doc('include/voc-;doc('$ROOT_PATH/include/voc-;g" $3 >> /tmp/log_schema.log
echo "<Result>PASSED<\\Result>" >> /tmp/log_schema.log

#remove all xsl and jar and colle.sch that was a intermediate file
rm $2/*.xsl $2/*.jar $2/colle.sch >> /tmp/log_schema.log

