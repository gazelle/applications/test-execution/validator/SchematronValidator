-- Update Schematron Validator from 2.3.1 to 2.4.0
-- Author: hra

-- *** Update table object type
ALTER TABLE sch_validator_object_type RENAME label_to_display to name ;

-- *** Update table schematron
ALTER TABLE sch_validator_schematron RENAME name to keyword ;
ALTER TABLE sch_validator_schematron RENAME label to name ;
ALTER TABLE sch_validator_schematron DROP CONSTRAINT sch_validator_schematron_label_key;

-- Missing sequence drop from 2.0.0 update
DROP SEQUENCE cmn_application_preference_id_seq;
DROP SEQUENCE user_role_id_seq;
DROP SEQUENCE user_id_seq;
DROP SEQUENCE cmn_number_of_results_per_page_id_seq;

INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), '/opt/SchematronValidator_prod/bin/XSDValidator-1.0-jar-with-dependencies.jar', 'xsd_1_1_validator_path');
