--
-- [ceoche] Update table sch_validator_schematron, Rename columns.
-- A simple column rename would have been enough if hibernate.hbm2ddl.auto where not set to 'update'...
-- But because we do not know how if the ear has been deployed before or after the db update, it is possible that new empty columns has already been
-- added, we then need to handle this possiblity.
--

ALTER TABLE sch_validator_schematron DROP IF EXISTS dfdl_schema_keyword ;
ALTER TABLE sch_validator_schematron RENAME COLUMN daffodil_oid TO dfdl_schema_keyword ;

ALTER TABLE sch_validator_schematron DROP IF EXISTS dfdl_transformation_needed ;
ALTER TABLE sch_validator_schematron RENAME COLUMN daffodil_transformation_needed TO dfdl_transformation_needed ;

UPDATE app_configuration SET variable = 'gazelle_transformation_url' WHERE variable = 'daffodil_transformation_url' ;
UPDATE app_configuration SET value = REPLACE(value, 'DaffodilTransformationGUI-ejb/DaffodilTransformationService/DaffodilTransformation', 'transformation-ejb/GazelleTransformationService/Transformation?wsdl') WHERE variable = 'gazelle_transformation_url' ;
