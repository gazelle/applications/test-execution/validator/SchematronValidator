alter table sch_validator_schematron add column xsd_version VARCHAR(255);
UPDATE sch_validator_schematron set xsd_version = '1.0';
alter table sch_validator_schematron add column author VARCHAR(255);
UPDATE sch_validator_schematron set author = 'NA';