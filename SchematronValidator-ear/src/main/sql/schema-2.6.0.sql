--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: comma_cat(text, text); Type: FUNCTION; Schema: public; Owner: gazelle
--

CREATE FUNCTION comma_cat(text, text) RETURNS text
    LANGUAGE sql
    AS $_$select case WHEN $2 is null or $2 = '' THEN $1 WHEN $1 is null or $1 = '' THEN $2 ELSE $1 || ', ' || $2 END$_$;


ALTER FUNCTION public.comma_cat(text, text) OWNER TO gazelle;

--
-- Name: list(text); Type: AGGREGATE; Schema: public; Owner: gazelle
--

CREATE AGGREGATE list(text) (
    SFUNC = comma_cat,
    STYPE = text,
    INITCOND = ''
);


ALTER AGGREGATE public.list(text) OWNER TO gazelle;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE app_configuration (
    id integer NOT NULL,
    value character varying(255),
    variable character varying(255)
);


ALTER TABLE app_configuration OWNER TO gazelle;

--
-- Name: app_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE app_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_configuration_id_seq OWNER TO gazelle;

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_application_preference_id_seq OWNER TO gazelle;

--
-- Name: cmn_event_tracking; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_event_tracking (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    login_id integer,
    object_class character varying(255) NOT NULL,
    object_id integer,
    type_event character varying(255)
);


ALTER TABLE cmn_event_tracking OWNER TO gazelle;

--
-- Name: cmn_event_tracking_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_event_tracking_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_event_tracking_id_seq OWNER TO gazelle;

--
-- Name: cmn_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_home (
    id integer NOT NULL,
    home_title character varying(255),
    iso3_language character varying(255),
    main_content text
);


ALTER TABLE cmn_home OWNER TO gazelle;

--
-- Name: cmn_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_home_id_seq OWNER TO gazelle;

--
-- Name: cmn_number_of_results_per_page_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_number_of_results_per_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_number_of_results_per_page_id_seq OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE cmn_path_linking_a_document (
    id integer NOT NULL,
    comment character varying(255),
    path character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE cmn_path_linking_a_document OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE cmn_path_linking_a_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cmn_path_linking_a_document_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO gazelle;

--
-- Name: revinfo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE revinfo (
    rev integer NOT NULL,
    revtstmp bigint
);


ALTER TABLE revinfo OWNER TO gazelle;

--
-- Name: sch_validator_object_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE sch_validator_object_type (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE sch_validator_object_type OWNER TO gazelle;

--
-- Name: sch_validator_object_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE sch_validator_object_type_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    description character varying(255),
    keyword character varying(255),
    label_to_display character varying(255)
);


ALTER TABLE sch_validator_object_type_aud OWNER TO gazelle;

--
-- Name: sch_validator_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE sch_validator_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sch_validator_object_type_id_seq OWNER TO gazelle;

--
-- Name: sch_validator_schematron; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE sch_validator_schematron (
    id integer NOT NULL,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    available boolean,
    description character varying(255),
    label character varying(255),
    name character varying(255) NOT NULL,
    path character varying(255),
    version character varying(255) NOT NULL,
    object_type_id integer,
    xsd_path character varying(255),
    need_report_generation boolean,
    provider character varying(255),
    transform_unknowns boolean,
    xsd_version character varying(255),
    author character varying(255),
    type character varying(255),
    dfdl_schema_keyword character varying(255),
    dfdl_transformation_needed boolean,
    use_relative_links boolean
);


ALTER TABLE sch_validator_schematron OWNER TO gazelle;

--
-- Name: sch_validator_schematron_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE sch_validator_schematron_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    last_changed timestamp without time zone,
    last_modifier_id character varying(255),
    available boolean,
    description character varying(255),
    label character varying(255),
    name character varying(255),
    path character varying(255),
    version character varying(255),
    object_type_id integer,
    xsd_path character varying(255)
);


ALTER TABLE sch_validator_schematron_aud OWNER TO gazelle;

--
-- Name: sch_validator_schematron_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE sch_validator_schematron_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sch_validator_schematron_id_seq OWNER TO gazelle;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO gazelle;

--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_role_id_seq OWNER TO gazelle;

--
-- Name: app_configuration app_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY app_configuration
    ADD CONSTRAINT app_configuration_pkey PRIMARY KEY (id);


--
-- Name: cmn_event_tracking cmn_event_tracking_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_event_tracking
    ADD CONSTRAINT cmn_event_tracking_pkey PRIMARY KEY (id);


--
-- Name: cmn_home cmn_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT cmn_home_pkey PRIMARY KEY (id);


--
-- Name: cmn_path_linking_a_document cmn_path_linking_a_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_path_linking_a_document
    ADD CONSTRAINT cmn_path_linking_a_document_pkey PRIMARY KEY (id);


--
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: sch_validator_object_type_aud sch_validator_object_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_object_type_aud
    ADD CONSTRAINT sch_validator_object_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: sch_validator_object_type sch_validator_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_object_type
    ADD CONSTRAINT sch_validator_object_type_pkey PRIMARY KEY (id);


--
-- Name: sch_validator_schematron_aud sch_validator_schematron_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_schematron_aud
    ADD CONSTRAINT sch_validator_schematron_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: sch_validator_schematron sch_validator_schematron_label_key; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_schematron
    ADD CONSTRAINT sch_validator_schematron_label_key UNIQUE (label);


--
-- Name: sch_validator_schematron sch_validator_schematron_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_schematron
    ADD CONSTRAINT sch_validator_schematron_pkey PRIMARY KEY (id);


--
-- Name: cmn_home uk_mv2quil5gwcd8bxyc76v4vyy1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY cmn_home
    ADD CONSTRAINT uk_mv2quil5gwcd8bxyc76v4vyy1 UNIQUE (iso3_language);


--
-- Name: sch_validator_schematron fk6e10b2d29c8f0237; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_schematron
    ADD CONSTRAINT fk6e10b2d29c8f0237 FOREIGN KEY (object_type_id) REFERENCES sch_validator_object_type(id);


--
-- Name: sch_validator_object_type_aud fka56ff8b7df74e053; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_object_type_aud
    ADD CONSTRAINT fka56ff8b7df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- Name: sch_validator_schematron_aud fkbe2327a3df74e053; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY sch_validator_schematron_aud
    ADD CONSTRAINT fkbe2327a3df74e053 FOREIGN KEY (rev) REFERENCES revinfo(rev);


--
-- PostgreSQL database dump complete
--

