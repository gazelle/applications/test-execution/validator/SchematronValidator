INSERT INTO app_configuration (id, value, variable)
  SELECT
    nextval('app_configuration_id_seq'),
    preference_value,
    preference_name
  FROM cmn_application_preference;

INSERT INTO app_configuration (id, value, variable) VALUES (
  nextval('app_configuration_id_seq'),
  '.*',
  'ip_login_admin'
);

INSERT INTO app_configuration (id, value, variable) VALUES (
  nextval('app_configuration_id_seq'),
  '20',
  'NUMBER_OF_ITEMS_PER_PAGE'
);

DROP TABLE cmn_application_preference;
DROP TABLE user_user_role;
DROP TABLE user_role_group;
DROP TABLE user_role;
DROP TABLE user_permission;
DROP TABLE "user";
DROP TABLE cmn_number_of_results_per_page;

INSERT INTO public.cmn_home (id, home_title, iso3_language, main_content) VALUES (1, 'SchematronValidator', 'fra', '<p>Bienvenue dans cette application dédiée à la configuration du validateur par l''ajout de références aux nouveaux schematrons Pour de plus amples informations concernant cette application, la documentation est disponible <a href="https://gazelle.ihe.net/">https://gazelle.ihe.net/</a> Pour toute question ou suggestion concernant cette application, merci de contacter l''administrateur (<a href="mailto:technical.manager@ihe-europe.net">Eric Poiseau</a>)</p>');
INSERT INTO public.cmn_home (id, home_title, iso3_language, main_content) VALUES (2, 'SchematronValidator', 'eng', '<p>Welcome to this web application dedicated to the configurion of the validator by adding new references to schematrons For futher information concerning this application, read documentation at <a href="https://gazelle.ihe.net/">https://gazelle.ihe.net/</a> For any questions or suggestions regarding this application, please contact the administrator (<a href="mailto:technical.manager@ihe-europe.net">Eric Poiseau</a>)</p>');

INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'Application Administrator', 'application_admin_title');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/content/schematron-based-document-validator', 'documentation_url');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/jira/projects/SCHVAL#selectedTab=com.atlassian.jira.plugin.system.project%3Achangelog-panel', 'application_release_notes_url');
UPDATE public.app_configuration SET value = 'Eric Poiseau' WHERE variable = 'application_admin_name';