package net.ihe.gazelle.schematron.preferences;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import java.io.Serializable;

/**
 * This class is used to store some of the preferences in the application state instead of querying several times the database
 *
 * @author aberge
 *
 */

@AutoCreate
@Name("applicationConfigurationProvider")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationConfigurationProviderLocal")
public class ApplicationConfigurationProvider implements Serializable, ApplicationConfigurationProviderLocal {

    /**
     *
     */
    private static final long serialVersionUID = 5004043953931007769L;
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfigurationProvider.class);

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    private Boolean applicationWorksWithoutCas;
    private Boolean ipLogin;
    private String ipLoginAdmin;
    private String applicationUrl;

    @Override
    @Create
    public void init() {
        this.applicationWorksWithoutCas = null;
        this.ipLogin = null;
        this.ipLoginAdmin = null;
        this.applicationUrl = null;
    }

    @Override
    @Destroy
    @Remove
    public void destroy() {
        this.init();
    }

    public static ApplicationConfigurationProviderLocal instance() {
        return (ApplicationConfigurationProviderLocal) Component.getInstance("applicationConfigurationProvider");
    }

    @Override
    public Boolean getApplicationWorksWithoutCas() {
        if (this.applicationWorksWithoutCas == null) {
            String valueAsString = applicationConfigurationDAO.getValue("application_works_without_cas");
            if (valueAsString == null) {
                this.applicationWorksWithoutCas = false;
            } else {
                this.applicationWorksWithoutCas = Boolean.valueOf(valueAsString);
            }
        }
        return this.applicationWorksWithoutCas;
    }

    @Override
    public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

    @Override
    public Boolean getIpLogin() {
        if (this.ipLogin == null) {
            String valueAsString = applicationConfigurationDAO.getValue("ip_login");
            if (valueAsString == null) {
                this.ipLogin = false;
            } else {
                this.ipLogin = Boolean.valueOf(valueAsString);
            }
        }
        return this.ipLogin;
    }

    @Override
    public String getIpLoginAdmin() {
        if (this.ipLoginAdmin == null) {
            this.ipLoginAdmin = applicationConfigurationDAO.getValue("ip_login_admin");
        }
        return this.ipLoginAdmin;
    }

    @Override
    public String getApplicationUrl() {
        if (this.applicationUrl == null) {
            this.applicationUrl = applicationConfigurationDAO.getValue("application_url");
        }
        return this.applicationUrl;
    }

    @Override
    public boolean isUserAllowedAsAdmin() {
        return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
    }
}
