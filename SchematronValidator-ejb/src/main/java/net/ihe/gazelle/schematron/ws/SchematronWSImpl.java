package net.ihe.gazelle.schematron.ws;

import com.rits.cloning.Cloner;
import net.ihe.gazelle.schematron.dao.ObjectTypeDAO;
import net.ihe.gazelle.schematron.dao.SchematronDAO;
import net.ihe.gazelle.schematron.model.ObjectType;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.SchFilePath;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.DatatypeConverter;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Stateless
@Name("SchematronWS")
@WebService(name = "Schematron", serviceName = "SchematronService", targetNamespace = "http://ws.validator.sch.gazelle.ihe.net/")
public class SchematronWSImpl implements SchematronWS {

    @In
    private ObjectTypeDAO objectTypeDAO;
    @In
    private SchematronDAO schematronDAO;
    @In
    private SchFilePath schFilePath;

    public List<Schematron> getAllSchematrons() throws SOAPException {
        final List<Schematron> schematrons = schematronDAO.getAllAvailableSchematrons();
        if (schematrons == null) {
            throw new SOAPException("No methods found");
        } else {
            final Cloner cloner = new Cloner();
            final List<Schematron> clones = cloner.deepClone(schematrons);
            for (final Schematron sch : clones) {
                sch.setDfdlSchemaKeyword(null);
                sch.setDfdlTransformationNeeded(null);
                sch.setUseRelativeLinks(null);
            }
            return clones;
        }
    }

    public List<Schematron> getSchematronsForAGivenType(
            @WebParam(name = "fileType") final String inType) throws SOAPException {
        if (inType == null || inType.length() == 0) {
            throw new SOAPException("You must provide a file type, valid values are CDA, HL7v3 ...");
        }

        final List<Schematron> schematrons = schematronDAO.getAvailableSchematronsForAGivenType(inType);
        final Cloner cloner = new Cloner();
        final List<Schematron> clones = cloner.deepClone(schematrons);

        if (clones == null) {
            throw new SOAPException("No schematron found for the type " + inType);
        } else {
            for (final Schematron sch : clones) {
                sch.setDfdlSchemaKeyword(null);
                sch.setDfdlTransformationNeeded(null);
                sch.setUseRelativeLinks(null);
            }
            return clones;
        }
    }

    //FIXME add findSchematronByKeyword method

    public String getSchematronByName(
            @WebParam(name = "schematronName") final String inSchematronName) throws SOAPException {
        String schpath = "";
        if (inSchematronName == null || inSchematronName.length() == 0) {
            throw new SOAPException("You must provide a schematron name");
        }

        final Schematron schematron = schematronDAO.getSchematronByName(inSchematronName);

        if (schematron == null) {
            throw new SOAPException("No schematron found with name: " + inSchematronName);
        }

        if ("".equals(schematron.getPath())) {
            return DatatypeConverter.printBase64Binary("No schematron -- Only XSD Validation".getBytes());
        } else {
            try {
                schpath = schFilePath.getSchPath(schematron);
                byte[] content = Files.readAllBytes(Paths.get(schpath));
                return DatatypeConverter.printBase64Binary(content);
            } catch (IOException e) {
                throw new SOAPException("Unexpected error while reading the file " + schpath + ": " + e.getMessage(), e);
            }
        }
    }

    public List<ObjectType> getAllAvailableObjectTypes() {
        return objectTypeDAO.getAllObjectTypes();
    }


}
