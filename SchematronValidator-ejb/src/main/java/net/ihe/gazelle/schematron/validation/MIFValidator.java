package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import org.eclipse.ohf.h3et.instanceeditor.core.InstanceValidatorApp;
import org.eclipse.ohf.h3et.instanceeditor.core.context.ContextManager;
import org.eclipse.ohf.h3et.instanceeditor.core.context.InstanceEditorContext;
import org.eclipse.ohf.h3et.instanceeditor.core.editor.extended.ExtendedEditorManager;
import org.eclipse.ohf.h3et.instanceeditor.core.problems.Problem;
import org.eclipse.ohf.h3et.instanceeditor.core.validator.ValidationOptions;
import org.eclipse.ohf.h3et.instanceeditor.core.validator.extended.StaticValidatorManager;
import org.eclipse.ohf.h3et.repository.BaseRepositoryFactory;
import org.eclipse.ohf.h3et.repository.H3ETConfiguration;
import org.eclipse.ohf.h3et.repository.definitions.RepositoryAccessor;
import org.eclipse.ohf.h3et.repository.definitions.RepositoryContext;
import org.eclipse.ohf.h3et.repository.pojo.SimpleFileRepositoryAccessor;
import org.eclipse.ohf.utilities.xml.DOMParserWithPosition;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MIFValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MIFValidator.class);

    private ApplicationConfigurationDAO applicationConfigurationDAO;
    private File fileToValidate;
    private List<Problem> problems;
    private boolean validationPassed = true;

    public MIFValidator(final File fileToValidate, ApplicationConfigurationDAO applicationConfigurationDAO) {
        this.fileToValidate = fileToValidate;
        this.applicationConfigurationDAO = applicationConfigurationDAO;
    }

    /**
     * @return the validationPassed
     */
    public boolean isValidationPassed() {
        return this.validationPassed;
    }

    /**
     * @param validationPassed the validationPassed to set
     */
    public void setValidationPassed(final boolean validationPassed) {
        this.validationPassed = validationPassed;
    }

    public Boolean validate() {

        try {
            // boot-trap the extended validator
            final Map<String, String[]> extendedValidators = new HashMap<>();
            extendedValidators
                    .put("ScenarioExtendedValidator", new String[]{"org.eclipse.ohf.h3et.instanceeditor.examples.core.validator.SchematronValidator"});
            ExtendedEditorManager.initManager(extendedValidators);

            final String root = applicationConfigurationDAO.getValue("mif_root_directory");
            final File rootDir = new File(root);

            // configure validator
            final File prefFile = new File(root, "ValidationConfig.xml");
            final Properties props = new Properties();

            final H3ETConfiguration mifConfiguration;
            final StaticValidatorManager extValidatorManager;
            final String modelPath;
            final String vocabFolder;
            final String fragmentFolder;
            final RepositoryAccessor accessor;
            final RepositoryContext reposContext;
            final DOMParserWithPosition parser;
            final Document domDoc;
            final InstanceEditorContext editContext;

            try (final FileInputStream prefFileInputStream = new FileInputStream(prefFile)) {
                props.load(prefFileInputStream);
            } catch (final Exception e) {
                LOGGER.error(e.getMessage(), e);
            }

            mifConfiguration = new H3ETConfiguration(props);
            extValidatorManager = new StaticValidatorManager(null);

            // create context
            modelPath = root + File.separatorChar + mifConfiguration.getModelPath();
            vocabFolder = root + File.separatorChar + mifConfiguration.getVocabPath();
            fragmentFolder = root + File.separatorChar + mifConfiguration.getFragmentPath();
            accessor = new SimpleFileRepositoryAccessor(modelPath, false, vocabFolder, fragmentFolder);
            reposContext = BaseRepositoryFactory.getRepositoryContext(rootDir, mifConfiguration, accessor);

            parser = InstanceValidatorApp.getDOMParser();
            parser.parse(new InputSource(new FileInputStream(this.fileToValidate)));
            domDoc = parser.getDocument();
            reposContext.getAccessor().setWorkingDocument(this.fileToValidate.getAbsolutePath());

            editContext = ContextManager.createEditorContext(this.fileToValidate.toURI(),
                    domDoc.getDocumentElement(),
                    reposContext,
                    extValidatorManager,
                    this.getClass().getResourceAsStream("ExtServiceConfig.xml"),
                    true);

            this.problems = editContext.getValidator().validate(domDoc.getDocumentElement(), new ValidationOptions());
            // if no problem is returned, the validation is successful
            return (this.problems == null || this.problems.isEmpty());
        }
        // if a problem occurred during the validation, returns null
        catch (final FileNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    public Element getMIFValidationResult() {
        final Element xmlResult = new Element("MIFValidation");
        int errorCount = 0;
        int warningCount = 0;
        int infoCount = 0;

        final Element result = new Element("Result");
        if (this.problems != null && !this.problems.isEmpty()) {
            // define severity
            for (final Problem problem : this.problems) {
                Element problemNode = null;
                switch (problem.getSeverity()) {
                case Problem.HIGH_SEVERITY:
                    errorCount++;
                    problemNode = new Element("Error");
                    break;
                case Problem.NORMAL_SEVERITY:
                    problemNode = new Element("Warning");
                    warningCount++;
                    break;
                case Problem.LOW_SEVERITY:
                    problemNode = new Element("Info");
                    infoCount++;
                    break;
                default:
                    problemNode = new Element("Problem");
                }
                problemNode.setAttribute("startLine", DOMParserWithPosition.getNodeStartLine(problem.getNode(), 0).toString());
                problemNode.setAttribute("startColumn", DOMParserWithPosition.getNodeStartColumn(problem.getNode(), 0).toString());
                problemNode.setAttribute("endLine", DOMParserWithPosition.getNodeEndLine(problem.getNode(), 0).toString());
                problemNode.setAttribute("endColumn", DOMParserWithPosition.getNodeEndColumn(problem.getNode(), 0).toString());
                problemNode.setAttribute("message", problem.getMessage().trim());
                problemNode.setAttribute("location", this.buildNodePath(problem.getNode()));

                xmlResult.addContent(problemNode);
            }
            if (errorCount > 0) {
                this.validationPassed = false;
                result.addContent("FAILED");
            } else {
                this.validationPassed = true;
                result.addContent("PASSED");
            }
        } else {
            this.validationPassed = true;
            result.addContent("PASSED");
        }

        xmlResult.addContent(result);
        final Element counters = new Element("ValidationCounters");
        counters.addContent(new Element("NrOfValidationErrors").addContent(Integer.toString(errorCount)));
        counters.addContent(new Element("NrOfValidationWarnings").addContent(Integer.toString(warningCount)));
        counters.addContent(new Element("NrOfValidationInfos").addContent(Integer.toString(infoCount)));
        xmlResult.addContent(counters);
        return xmlResult;
    }

    private String buildNodePath(final org.w3c.dom.Node node) {
        if (node == null) {
            return "/";
        } else if (node instanceof Document) {
            return "";
        } else if (node instanceof Element) {
            return this.buildNodePath(node.getParentNode())
                    + "/" + node.getNodeName() + "[" + this.getNodeIndex((org.w3c.dom.Element) node) + "]";
        } else if (node instanceof Attr) {
            return this.buildNodePath(((Attr) node).getOwnerElement()) + "/" + node.getNodeName();
        } else {
            return node.getNodeName();
        }
    }

    private int getNodeIndex(final org.w3c.dom.Element node) {
        int i = 1;
        for (Node prev = node.getPreviousSibling(); prev != null; prev = prev.getPreviousSibling()) {
            if (prev instanceof Element && node.getNodeName().equals(prev.getNodeName())) {
                i++;
            }
        }

        return i;
    }

}
