package net.ihe.gazelle.schematron.compiler;

import net.ihe.gazelle.schematron.dao.SchematronDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.SchFilePath;
import net.ihe.gazelle.schematron.util.SchematronXSLConverter;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Name("schematronCompiler")
@AutoCreate
public class SchematronCompiler {

    @In
    private SchematronDAO schematronDAO;

    @In
    private SchFilePath schFilePath;

    private static final Logger LOGGER = LoggerFactory.getLogger(SchematronCompiler.class);

    private static final String CDA_HL7_TAG = "xmlns:cda=\"urn:hl7-org:v3\" xmlns:hl7=\"urn:hl7-org:v3\"";
    private static final String HL7_TAG = "xmlns:hl7=\"urn:hl7-org:v3\"";
    private static final String CDA_TAG = "xmlns:cda=\"urn:hl7-org:v3\"";

    /**
     * @param schema : Schematron
     * @return : boolean true when creation successful else false
     */
    public boolean createArtDecorSchematronPreCompilationCopy(final Schematron schema) {
        boolean result = true;
        final String schpath = schFilePath.getSchPath(schema);

        final File schemaFileParent = new File(schpath).getParentFile();

        final File precompilationSchemaFileParent = new File(schFilePath.getPrecompilationDirPath(schemaFileParent));

        if (precompilationSchemaFileParent.exists() && !precompilationSchemaFileParent.delete()) {
            LOGGER.info("Fail to delete schematron precompilation folder : ", precompilationSchemaFileParent.getAbsolutePath());
        }

        precompilationSchemaFileParent.mkdir();
        try {
            FileUtils.copyDirectory(schemaFileParent, precompilationSchemaFileParent);
        } catch (final Exception e) {
            result = false;
            LOGGER.error("Fail to create ART-DECOR schematron precompilation file : ", e);
        }
        return result;
    }

    /**
     *
     * @param schemaPath : Schematron path
     * @return : boolean true when successful precompilation else false
     */
    public boolean precompileArtDecorSchematron(final String schemaPath) {
        boolean result = false;
        if (null != schemaPath && !schemaPath.isEmpty()) {
            final File schemaFile = new File(schemaPath);
            if (schemaFile.exists()) {
                final String cmd = getADPrecompileFile(schemaFile);
                try {
                    final Process process = Runtime.getRuntime().exec(cmd);
                    final int resultInt = process.waitFor();
                    //InputStream outcome = process.getInputStream();
                    if (resultInt == 0) {
                        result = true;
                    }
                } catch (final Exception e) {
                    LOGGER.error("Fail to compile ART-DECOR schematron : ", e);
                }
            }
        }
        return result;
    }

    private String getADPrecompileFile(File schemaFile) {
        return schFilePath.getBinPath() + "/ART-DECOR_precompilation_tools/precompile.sh" + " " + schFilePath.getBinPath() + "/ART-DECOR_precompilation_tools" + " " + schemaFile.getParent() + " " + schemaFile.getName();
    }

    protected String updateDocumentLinks(String content, String schematronAbsolutePath) {
        if (content != null) {
            String res = content;
            Pattern pat = Pattern.compile("document\\((.*?)\\)");
            Matcher mat = pat.matcher(content);
            while (mat.find()) {
                res = res.replace(mat.group(), "document(concat('" + schematronAbsolutePath + "/', " + mat.group(1) + "))");
            }
            return res;
        }
        return null;
    }

    /**
     * Allows to make the conversion
     */
    public void compileSchematron(final Schematron schematron) {

        final String schpath = schFilePath.getSchPath(schematron);
        String schpathForCompilation = schpath;

        boolean isCompilationOK = true;
        // Check the type of Schematron...
        if (Schematron.TYPE_ART_DECOR.equals(schematron.getType())) {
            LOGGER.info("Schematron.TYPE_ART_DECOR : Precompilation processing...");
            createArtDecorSchematronPreCompilationCopy(schematron);
            schpathForCompilation = schFilePath.calculateSchpathForCompilation(schpath);
            isCompilationOK = precompileArtDecorSchematron(schpathForCompilation);
        }

        if (isCompilationOK) {
            final String isoexpandpath = schFilePath.getIsoExpandPath();
            final String isopath = schFilePath.getIsoPath();
            final SAXBuilder sxb = new SAXBuilder();
            Document sch = null;

            try {
                sch = sxb.build(new File(schpathForCompilation));
            } catch (final JDOMException e) {
                LOGGER.error(e.getMessage(), e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "An error occurred when converting the schematron [" + schematron.getName() + "] to DOM : " + e.getMessage(), e);
            } catch (final IOException e) {
                LOGGER.error(e.getMessage(), e);
                FacesMessages.instance()
                        .add(StatusMessage.Severity.ERROR,
                                "An error occurred when retrieving the schematron file [" + schpathForCompilation + "] : " + e.getMessage(),
                                e);
            }

            final SchematronXSLConverter sxc = new SchematronXSLConverter();
            sxc.setSchematronDocument(sch);
            try {
                boolean needReportGeneration = false;
                if (schematron.getNeedReportGeneration() != null && schematron.getNeedReportGeneration()) {
                    needReportGeneration = true;
                }
                sxc.convert(isoexpandpath, isopath, needReportGeneration);
            } catch (final TransformerException e) {
                LOGGER.error(e.getMessage(), e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "An error occurred when converting the schematron [" + schematron.getName() + "] : " + e.getMessage(), e);
            }
            final Document doc = sxc.getResult();
            String content = new XMLOutputter().outputString(doc);

            if (Schematron.TYPE_ART_DECOR.equals(schematron.getType()) && !content.contains(CDA_TAG)) {
                content = content.replace(HL7_TAG, CDA_HL7_TAG);
            }
            if (schematron.getUseRelativeLinks() == null || schematron.getUseRelativeLinks()) {
                content = updateDocumentLinks(content, new File(schpath).getParent());
            }

            final String filecompil = schFilePath.getCompilationPath();
            final File dir = new File(filecompil);
            dir.mkdirs();
            final File res = new File(dir, schematron.getId() + ".xsl");
            try (PrintWriter pw = new PrintWriter(res)) {
                pw.println(content);
                pw.close();
                schematronDAO.addOrUpdateSchematron(schematron);
            } catch (final FileNotFoundException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "File not found [" + res.getAbsolutePath() + "] :" + e.getMessage(), e);
                LOGGER.error(e.getMessage(), e);

            }

            LOGGER.info("The Schematron [" + schematron.getName()
                    + "] compiled with success. The result of compilation available on bin/compilations/"
                    + schematron.getId() + ".xsl");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Errors occurred when precompiling the schematron [" + schematron.getName() + "] : try again...");
        }
    }
}
