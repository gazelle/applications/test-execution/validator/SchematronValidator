package net.ihe.gazelle.schematron.util;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * @author aberge
 * @class EmailNotification
 * <p/>
 * This class is used to send an email to the administrator and/or monitor of the application when the validation aborts
 */
@AutoCreate
@Name("emailNotificationManager")
public class EmailNotificationManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2013153390754620929L;
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailNotificationManager.class);

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    public void send(final Exception e, final String messageSubject, final String additionalMessage) {
        LOGGER.info("sending email...");

        // retrieving email addresses
        final String monitorEmail = applicationConfigurationDAO.getValue("monitor_email");
        final String administratorEmail = applicationConfigurationDAO.getValue("application_admin_email");

        String emailContent = "";

        if (additionalMessage != null) {
            emailContent = additionalMessage + "\n";
        }

        if (e != null) {
            final String message = e.getMessage();
            final StackTraceElement[] trace = e.getStackTrace();
            if (trace.length > 0) {

                final StringBuffer buf = new StringBuffer(emailContent);

                for (final StackTraceElement el : trace) {
                    buf.append("at ");
                    buf.append(el.getClassName());
                    buf.append(".");
                    buf.append(el.getMethodName());
                    buf.append("(line ");
                    buf.append(el.getLineNumber());
                    buf.append(")\n");
                }
                emailContent = message + "\n\n" + buf.toString();
            }
        }

        try {
            final Properties properties = new Properties();
            properties.put("mail.smtp.host", "localhost");

            final Session session = Session.getDefaultInstance(properties);
            session.setDebug(false);

            final Message mail = new MimeMessage(session);

            final InternetAddress from = new InternetAddress(administratorEmail,
                    applicationConfigurationDAO.getValue("application_name"));
            mail.setFrom(from);

            final InternetAddress[] to = new InternetAddress[2];
            to[0] = new InternetAddress(administratorEmail);
            if (monitorEmail != null) {
                to[1] = new InternetAddress(monitorEmail);
            }

            mail.setRecipients(Message.RecipientType.TO, to);

            mail.setSubject(messageSubject);
            mail.setContent(emailContent, "text/plain");
            Transport.send(mail);

        } catch (UnsupportedEncodingException | MessagingException ex) {
            LOGGER.error("Cannot send email", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot send email " + e.getMessage(), e);
        }

    }

}