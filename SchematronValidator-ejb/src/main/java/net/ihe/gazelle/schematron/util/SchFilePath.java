package net.ihe.gazelle.schematron.util;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import java.io.File;

@Name("schFilePath")
@AutoCreate
public class SchFilePath {

    @In
    ApplicationConfigurationDAO applicationConfigurationDAO;

    private static final String BIN_PATH_PREF = "bin_path";
    private static final String BIN_PATH_DEFAULT = "/opt/SchematronValidator_prod/bin/";
    private static final String HOME_PATH_PREF = "gazelle_home_path";
    private static final String HOME_PATH_DEFAULT = "/opt/SchematronValidator_prod/";

    public String getSchematronPathRoot() {
        return getBinPath() + "/schematron";
    }

    public String getXsdPathRoot() {
        return getGazelleHomePath() + "/xsd";
    }

    public String getSchPath(Schematron schematron) {
        return getBinPath() + "/schematron/" + schematron.getPath().trim();
    }

    public String getIsoExpandPath() {
        return getBinPath() + "/schematron/schematron/iso_abstract_expand.xsl";
    }

    public String getIsoPath() {
        return getBinPath() + "/schematron/schematron/iso_svrl_for_xslt2.xsl";
    }

    public String getSchCompiledPath(Schematron schematron) {
        return getBinPath() + "/compilations/" + schematron.getId() + ".xsl";
    }

    public String getXSDPath(Schematron schematron) {
        return getGazelleHomePath() + "/xsd/" + schematron.getXsdPath().trim();
    }

    public String getCompilationPath() {
        return getBinPath() + File.separator + "compilations";
    }

    public String calculateSchpathForCompilation(String schpath) {
        if (schpath != null) {
            File filesch = new File(schpath);
            return filesch.getParent() + File.separator + "_pre_compilation" + File.separator + filesch.getName();
        }
        return null;
    }

    public String getPrecompilationDirPath(File schemaFileParent) {
        return schemaFileParent.getAbsolutePath() + File.separator + "_pre_compilation";
    }

    public String getBinPath() {
        String binPath = applicationConfigurationDAO.getValue(BIN_PATH_PREF);
        return binPath != null && !binPath.isEmpty() ? binPath : BIN_PATH_DEFAULT;
    }

    private String getGazelleHomePath() {
        String binPath = applicationConfigurationDAO.getValue(HOME_PATH_PREF);
        return binPath != null && !binPath.isEmpty() ? binPath : HOME_PATH_DEFAULT;
    }
}
