package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.schematron.compiler.SchematronCompiler;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.SchFilePath;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.*;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJBException;
import javax.xml.bind.ValidationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Name("schematronValidator")
@AutoCreate
public class SchematronValidator {

    @In(value = "metadataServiceProvider")
    MetadataServiceProvider metadataServiceProvider;


    @In
    private SchematronCompiler schematronCompiler;

    @In
    private SchFilePath schFilePath;

    private static final Logger LOGGER = LoggerFactory.getLogger(SchematronValidator.class);

    private static final String PASSED = "PASSED";

    private static final String FAILED = "FAILED";

    private static Namespace svrlNamespace = Namespace.getNamespace("svrl", "http://purl.oclc.org/dsdl/svrl");


    public Document validateDocument(final Schematron schematron, final File cdafile)
            throws JDOMException, IOException, TransformerException, ValidationException {
        final String isoexpandpath = schFilePath.getIsoExpandPath();
        final String isopath = schFilePath.getIsoPath();
        return validateDocument(schematron, cdafile, isoexpandpath, isopath);
    }

    public Document validateDocument(final Schematron schematron, final File cdafile, final String isoexpandpath, final String isopath)
            throws JDOMException, IOException, TransformerException, ValidationException {
        final SAXBuilder sxb = new SAXBuilder();
        Document fileD = null;
        try {
            fileD = sxb.build(cdafile);
        } catch (final JDOMException | IOException | EJBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new ValidationException("an error occurred when converting the XML Document to DOM : " + e.getMessage(), e);
        }

        final File compiledSch = new File(schFilePath.getSchCompiledPath(schematron));

        //Initial set to true so that if file does not exist, compilation is forced
        boolean needToRecompile = true;
        if (compiledSch.exists()) {
            final long compiledDate = compiledSch.lastModified();
            final String schpath = schFilePath.getSchPath(schematron);
            final File schFile = new File(schpath);
            if (schFile.exists()) {
                final long schDate = schFile.lastModified();
                if (compiledDate >= schDate) {
                    needToRecompile = false;
                }
            }
        }

        if (needToRecompile) {
            schematronCompiler.compileSchematron(schematron);
        }

        return validateFileByCompiledSchematron(sxb, fileD, compiledSch);
    }

    public Document validateFileByCompiledSchematron(final SAXBuilder sxb, final Document fileD, final File compiledSch)
            throws JDOMException, IOException, TransformerException {
        final Document schDoc = sxb.build(compiledSch);

        final XMLXSLValidator xxv = new XMLXSLValidator(fileD, schDoc);
        try {
            xxv.validate();
        } catch (final TransformerException e) {
            LOGGER.error(e.getMessage(), e);
            throw new TransformerException("An error occurred during the validation : " + e.getMessage(), e);
        }
        return xxv.getResult().getDocument();
    }

    public List<net.ihe.gazelle.validation.Notification> validateFileBySchematron(final Schematron schematron, final File cdafile) throws IOException,
            ValidationException, JDOMException, TransformerException {

        List<Notification> ln = new ArrayList<Notification>();

        if (cdafile == null) {
            LOGGER.error("file is null");
            return ln;
        }
        if (schematron == null) {
            LOGGER.error("schematron is null");
            return ln;
        }
        final String schpath = schFilePath.getSchPath(schematron);
        final File file = new File(schpath);
        if (!file.exists() || !file.isFile()) {
            LOGGER.info("schematron " + schpath + " does not exists");
            return ln;
        }

        return getValidationResult(schematron, cdafile);

    }

    List<net.ihe.gazelle.validation.Notification> getValidationResult(final Schematron schematron, final File file) throws JDOMException, ValidationException, TransformerException, IOException {
        try {
            final Document result = validateDocument(schematron, file);
            if (result == null) {
                LOGGER.error("no result");
                throw new ValidationException("no result");
            }
            final List<net.ihe.gazelle.validation.Notification> ln = getListNotification(result, schematron);
            return ln;

        } catch (final TransformerException | JDOMException | IOException | ValidationException e) {
            LOGGER.error(e.getMessage() + System.lineSeparator() + "Schematron name : " + schematron.getKeyword());
            throw e;
        }
    }

    private List<net.ihe.gazelle.validation.Notification> getListNotification(Document result, Schematron schematron) {
        List<net.ihe.gazelle.validation.Notification> res = new ArrayList<net.ihe.gazelle.validation.Notification>();
        if (result == null) {
            LOGGER.info("result null");
            return null;
        }
        final List<Element> list_pb = result.getRootElement().getChildren("failed-assert", svrlNamespace);
        if (list_pb != null && !list_pb.isEmpty()) {
            for (final Element elem : list_pb) {
                final String test = elem.getAttributeValue("test");
                final String context = elem.getAttributeValue("location");
                String mess = null;
                final Element el = elem.getChild("text", svrlNamespace);
                if (el != null) {
                    mess = el.getText();
                }
                if (mess != null) {
                    if ((elem.getAttribute("role") != null && "error"
                            .equalsIgnoreCase(elem.getAttribute("role").getValue())) ||
                            mess.contains("error") || mess.contains("Error") || mess.contains("ERR")) {
                        Error not = new Error();
                        not.setDescription(mess);
                        not.setTest(test);
                        not.setLocation(context);
                        res.add(not);
                    } else if ((elem.getAttribute("role") != null && "warning"
                            .equalsIgnoreCase(elem.getAttribute("role").getValue())) || mess.contains("Warning") || mess.contains("warning")
                            || mess.contains("CTX:")) {
                        Warning not = new Warning();
                        not.setDescription(mess);
                        not.setTest(test);
                        not.setLocation(context);
                        res.add(not);
                    } else if ((elem.getAttribute("role") != null && "note"
                            .equalsIgnoreCase(elem.getAttribute("role").getValue())) ||
                            (elem.getAttribute("role") != null && "information"
                                    .equalsIgnoreCase(elem.getAttribute("role").getValue())) ||
                            mess.contains("Note:")) {
                        Note not = new Note();
                        not.setDescription(mess);
                        not.setTest(test);
                        not.setLocation(context);
                        res.add(not);
                    } else {
                        boolean transformUnknowns = true;
                        if (schematron != null) {
                            transformUnknowns = schematron.getTransformUnknowns();
                        }
                        if (transformUnknowns) {
                            Error not = new Error();
                            not.setDescription(mess);
                            not.setTest(test);
                            not.setLocation(context);
                            res.add(not);
                        } else {
                            Info info = new Info();
                            info.setDescription(mess);
                            info.setTest(test);
                            info.setLocation(context);
                            res.add(info);
                        }
                    }
                }
            }
        }

        // list all successful-report
        final List<Element> list_success = result.getRootElement().getChildren("successful-report", svrlNamespace);
        if (list_success != null && !list_success.isEmpty()) {
            for (final Element elem : list_success) {
                Note not = new Note();
                not.setTest(elem.getAttributeValue("test"));
                not.setLocation(elem.getAttributeValue("location"));
                if (elem.getChild("text", svrlNamespace) != null) {
                    not.setDescription(elem.getChild("text", svrlNamespace).getText());
                }
                res.add(not);
            }
        }
        return res;
    }

    public void validate(DetailedResult res, Schematron schematron, File file) throws ValidationException, TransformerException, JDOMException, IOException {
        List<net.ihe.gazelle.validation.Notification> ln = validateFileBySchematron(schematron, file);
        if (res.getMDAValidation() == null) {
            res.setMDAValidation(new MDAValidation());
        }
        for (net.ihe.gazelle.validation.Notification notification : ln) {
            res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
        }
        updateMDAValidation(res.getMDAValidation(), ln);
        summarizeDetailedResult(res, schematron);
    }

    private void updateMDAValidation(MDAValidation mda, List<net.ihe.gazelle.validation.Notification> ln) {
        mda.setResult("PASSED");
        if (ln != null && !ln.isEmpty()) {
            for (net.ihe.gazelle.validation.Notification notification : ln) {
                if (notification instanceof net.ihe.gazelle.validation.Error) {
                    mda.setResult("FAILED");
                }
            }
        } else {
            LOGGER.error("The schematron validation is not done!");
        }
    }

    private void summarizeDetailedResult(DetailedResult dr, Schematron schematron) {
        if (dr != null) {

            Date dd = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            DateFormat dF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

            dr.setValidationResultsOverview(new ValidationResultsOverview());
            dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
            dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
            dr.getValidationResultsOverview().setValidationServiceName(
                    "Gazelle Schematron Validator");
            dr.getValidationResultsOverview().setValidationEngine(schematron.getName());
            dr.getValidationResultsOverview().setValidationEngineVersion(schematron.getVersion());
            dr.getValidationResultsOverview().setValidationTestResult(PASSED);
            if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null)
                    && (dr.getDocumentValidXSD().getResult().equals(FAILED))) {
                dr.getValidationResultsOverview().setValidationTestResult(FAILED);
            }
            if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() != null)
                    && (dr.getDocumentWellFormed().getResult().equals(FAILED))) {
                dr.getValidationResultsOverview().setValidationTestResult(FAILED);
            }
            if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null)
                    && (dr.getMDAValidation().getResult().equals(FAILED))) {
                dr.getValidationResultsOverview().setValidationTestResult(FAILED);
            }
            dr.getValidationResultsOverview().setValidationServiceVersion(metadataServiceProvider.getMetadata().getVersion());
        }
    }
}


