package net.ihe.gazelle.schematron.dao;

import net.ihe.gazelle.schematron.model.Schematron;

import javax.ejb.Local;
import javax.faces.model.SelectItem;
import java.util.List;

@Local
public interface SchematronDAO extends FilteredDAO<Schematron> {

    String XSD_VERSION_10  = "1.0";
    String XSD_VERSION_11  = "1.1";
    String TYPE_STANDARD   = "Standard";
    String TYPE_ART_DECOR  = "ART-DECOR";

    List<Schematron> getAllSchematrons();

    List<Schematron> getAllAvailableSchematrons();

    List<String> getAllAvailableSchematronsKeywords();

    List<Schematron> getAvailableSchematronsForAGivenType(final String inTypeKeyword);

    Schematron getSchematronByName(final String inName);

    Schematron getSchematronByKeyword(final String inKeyword);

    Schematron addOrUpdateSchematron(Schematron inSchematron);

    void deleteSchematron(Schematron inSchematron);

    boolean isKeywordAlreadyUsed(final String keyword);

    SelectItem[] getAllXsdVersions();

    SelectItem[] getAllSchematronTypes();

}
