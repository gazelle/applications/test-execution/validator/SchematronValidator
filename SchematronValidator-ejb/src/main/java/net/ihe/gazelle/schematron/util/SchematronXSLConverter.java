package net.ihe.gazelle.schematron.util;

import net.sf.saxon.TransformerFactoryImpl;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;
import org.jdom.transform.JDOMResult;
import org.jdom.transform.JDOMSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

/**
 * @author abdallah miladi - abderrazek boufahja
 */
public class SchematronXSLConverter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchematronXSLConverter.class);

    /**
     * The schematron that will be transformed
     */
    private Document schematronDocument;

    /**
     * The xsl result document
     */
    private Document resultDocument;

    /**
     * Default Constructor
     */
    public SchematronXSLConverter() {

    }

    /**
     * Constructs a converter which will convert the given schematronDocument using the preprocessedDocument
     *
     * @param schematronDocument
     * @param preprocessorDocument
     */
    public SchematronXSLConverter(Document schematronDocument, Document preprocessorDocument) {
        this.schematronDocument = schematronDocument;
    }

    /**
     * Sets the schematron Document that will be transformed
     *
     * @param schematronDocument The document that will be transformed
     */
    public void setSchematronDocument(final Document schematronDocument) {
        this.schematronDocument = schematronDocument;
    }

    /**
     * Allows to make the conversion
     *
     * @throws TransformerException
     */
    public void convert(String isoexpandpath, String isopath, boolean needReportGeneration) throws TransformerException {
        TransformerFactoryImpl trfactory = new TransformerFactoryImpl();
        Transformer transformer;

        // expand abstract patterns
        LOGGER.info("preprocessing schematron");
        final Source expandxsl = new StreamSource(isoexpandpath);
        trfactory.setErrorListener(new Listener());
        transformer = trfactory.newTransformer(expandxsl);
        transformer.setErrorListener(new Listener());
        final JDOMResult expandedSchematron = new JDOMResult();
        transformer.transform(new JDOMSource(this.schematronDocument), expandedSchematron);
        final Document expandedDoc = expandedSchematron.getDocument();

        final String xmlString = new XMLOutputter().outputString(expandedDoc);
        LOGGER.info(xmlString);

        // convert expanded schematron to xsl
        LOGGER.info("processing schematron");
        final Source isoxsl = new StreamSource(isopath);
        trfactory.setErrorListener(new Listener());
        transformer = trfactory.newTransformer(isoxsl);
        transformer.setErrorListener(new Listener());
        final JDOMResult domValidator = new JDOMResult();
        transformer.setParameter("allow-foreign", true);
        transformer.setParameter("process-assert-as-report", needReportGeneration);
        transformer.transform(new JDOMSource(expandedDoc), domValidator);
        this.resultDocument = domValidator.getDocument();
    }

    /**
     * Permits to get the result xsl File
     *
     * @return The result xsl File
     */
    public Document getResult() {
        return this.resultDocument;
    }

    public Document getSchematronDocument() {
        return this.schematronDocument;
    }

    public Document getResultDocument() {
        return this.resultDocument;
    }

    /**
     * <b>Class Description : </b>
     * <li>A listener that permits to customize the behavior of the transformer when encountering errors or warnings.</li>
     *
     * @author Abdallah MILADI / INRIA Rennes IHE development Project
     * <pre>http://www.ihe-europe.org</pre>
     * <pre>amiladi@irisa.fr</pre>
     * @version 1.0
     */
    class Listener implements javax.xml.transform.ErrorListener {
        /**
         * Customize the behavior of the transformer when encountering a warning
         */
        public void warning(final TransformerException e) {
            LOGGER.info(e.getLocationAsString() + ":" + e.getMessageAndLocation());
        }

        /**
         * Customize the behavior of the transformer when encountering an error
         */
        public void error(final TransformerException e) {
            LOGGER.info(e.getLocationAsString() + ":" + e.getMessageAndLocation());
        }

        /**
         * Customize the behavior of the transformer when encountering a fatal error
         */
        public void fatalError(final TransformerException e) {
            LOGGER.info(e.getLocationAsString() + ":" + e.getMessageAndLocation());
        }
    }
}
