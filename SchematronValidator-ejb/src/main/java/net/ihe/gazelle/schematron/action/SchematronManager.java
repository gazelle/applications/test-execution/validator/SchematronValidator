package net.ihe.gazelle.schematron.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.schematron.compiler.SchematronCompiler;
import net.ihe.gazelle.schematron.dao.ObjectTypeDAO;
import net.ihe.gazelle.schematron.dao.SchematronDAO;
import net.ihe.gazelle.schematron.model.ObjectType;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.SchFilePath;
import net.ihe.gazelle.schematron.validation.XMLValidation;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Name("schematronManagerBean")
@Scope(ScopeType.PAGE)
public class SchematronManager implements Serializable {

    @In
    private ObjectTypeDAO objectTypeDAO;

    @In
    private SchematronDAO schematronDAO;

    @In
    private SchematronCompiler schematronCompiler;

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    @In
    XMLValidation xMLValidation;

    @In
    private SchFilePath schFilePath;

	/**
     *
     */
    private static final long serialVersionUID = -7942050440637130610L;
    private static final Logger LOGGER = LoggerFactory.getLogger(SchematronManager.class);


    private transient Filter<Schematron> filter;
    private Schematron selectedSchematron;

    /**Getter setter
     *
     *
     */
    public void setFilter(final Filter<Schematron> filter) {
        this.filter = filter;
    }

    public Filter<Schematron> getFilter() {
        if (this.filter == null) {
            this.filter = schematronDAO.createFilter();
        }
        return this.filter;
    }

    public Schematron getSelectedSchematron() {
        return this.selectedSchematron;
    }

    public void setSelectedSchematron(final Schematron selectedSchematron) {
        this.selectedSchematron = selectedSchematron;
    }


    public void initCreateSchematron() {
        this.selectedSchematron = new Schematron();
    }

    public FilterDataModel<Schematron> getAllSchematrons() {
        return schematronDAO.getFiltered(SchematronManager.this.getFilter());
    }



    public void getSchematronFromUrl() {
        // If there's an id in the URL, get it and load the selectedSchematron
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.containsKey("id")) {
            final HQLQueryBuilder<Schematron> queryBuilder = new HQLQueryBuilder<>(
                    Schematron.class);
            final Integer schematronId = Integer.parseInt(urlParams.get("id"));
            queryBuilder.addEq("id", schematronId);
            this.selectedSchematron = queryBuilder.getUniqueResult();
        }
    }

    public void deleteSelectedSchematron() {
        schematronDAO.deleteSchematron(this.selectedSchematron);
    }

    public String displaySchematron(final Schematron inSchematron) {
        return "/schematrons/viewSchematron.seam?id=" + inSchematron.getId();
    }

    public String editSchematron(final Schematron inSchematron) {
        return "/schematrons/editSchematron.seam?id=" + inSchematron.getId();
    }

    public List<ObjectType> getAllObjectTypes() {
        return objectTypeDAO.getAllObjectTypes();
    }

    public SelectItem[] getAllXsdVersions() {
        return schematronDAO.getAllXsdVersions();
    }

    public SelectItem[] getAllSchematronTypes() {
        return schematronDAO.getAllSchematronTypes();
    }

    public String saveSchematron(final boolean editMode) {

        final boolean existingKeyword = schematronDAO.isKeywordAlreadyUsed(this.selectedSchematron.getKeyword());
        if (!existingKeyword || editMode) {
            if (this.selectedSchematron.getPath() != null && this.selectedSchematron.getPath().length() > 0) {
                final String path = schFilePath.getSchPath(this.selectedSchematron);
                final File temp = new File(path);
                if (!temp.exists()) {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "Schematron is missing at the location " + path);
                }
            }
            if (this.selectedSchematron.getXsdPath() != null && this.selectedSchematron.getXsdPath().length() > 0) {
                final String xsdpath = schFilePath.getXSDPath(this.selectedSchematron);
                final File temp = new File(xsdpath);
                if (!temp.exists()) {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "XSD is missing at the location " + xsdpath);
                }
            }
            if (this.selectedSchematron.getKeyword() == null || this.selectedSchematron.getKeyword().length() == 0) {
                FacesMessages.instance().addFromResourceBundle("schematron keyword is missing");
            }
            if (this.selectedSchematron.getVersion() == null || this.selectedSchematron.getVersion().length() == 0) {
                FacesMessages.instance().addFromResourceBundle("gazelle.sch.validator.schematron.version.missing");
            }
            schematronDAO.addOrUpdateSchematron(this.selectedSchematron);
            FacesMessages.instance().add("Schematron " + this.selectedSchematron.getName() + " saved");

        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Schematron has not been added in database : " + this.selectedSchematron
                    .getKeyword() + " keyword must be unique and is already in database");
        }

        return "/schematrons/manageSchematrons.seam";
    }

    public void cancelDeletion() {
        this.selectedSchematron = null;
    }

    public void copySchematron(final Schematron inSchematron) {
        if (inSchematron == null) {
            FacesMessages.instance().addFromResourceBundle("gazelle.sch.validator.no.schematron.selected");
            return;
        }
        final boolean existingKeyword = schematronDAO.isKeywordAlreadyUsed(inSchematron.getKeyword() + "_COPY");
        if (!existingKeyword) {
            this.selectedSchematron = schematronDAO.addOrUpdateSchematron(new Schematron(inSchematron));
        }
        if (this.selectedSchematron == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Schematron has not been duplicated in database : " + inSchematron.getKeyword() + "_COPY"
                            + " keyword must be unique and is already in database");
        } else {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.INFO, "Schematron " + this.selectedSchematron.getKeyword() + " has been duplicated in database");
        }
    }

    public String getSchematronPathRoot() {
        return schFilePath.getSchematronPathRoot();
    }

    /**
     * @param inSchematron
     * @return creationTime of inSchematron's compilation file if it exists, else empty String
     */
    public String getCompilationTimestamp(Schematron inSchematron) {
        String pathRoot = schFilePath.getCompilationPath();
        pathRoot += "/" + inSchematron.getId() + ".xsl";
        try {
            BasicFileAttributes attributes = Files.readAttributes(Paths.get(pathRoot), BasicFileAttributes.class);
            return new Date(attributes.creationTime().toMillis()).toString();
        } catch (IOException e) {
            return "";
        }
    }

    public String getXsdPathRoot() {
        return schFilePath.getXsdPathRoot();
    }

    public void updateSchematronsVersion() {
        final List<Schematron> schematronsToUpdate = schematronDAO.getAllSchematrons();
        if (schematronsToUpdate != null && !schematronsToUpdate.isEmpty()) {
            final String rootPath = getSchematronPathRoot();
            for (Schematron schematron : schematronsToUpdate) {
                final File schematronFile = new File(rootPath.concat(schematron.getPath()));
                if (schematronFile.exists()) {
                    final Date fileDate = new Date(schematronFile.lastModified());
                    if (null != schematron.getlastChanged()) {
                        if (fileDate.after(schematron.getlastChanged())) {
                            try (InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(schematronFile), StandardCharsets.UTF_8)){
                                final StringBuffer content = new StringBuffer();
                                final BufferedReader buff = new BufferedReader(inputStreamReader);
                                String line;
                                while ((line = buff.readLine()) != null) {
                                    content.append(line);
                                    content.append("\n");
                                }
                                buff.close();
                                final String revisionTag = "Revision";
                                Integer begin = content.indexOf(revisionTag);
                                if (begin < 0) {
                                    FacesMessages.instance().add("No Revision specified for schematron " + schematron.getName());
                                    continue;
                                } else {
                                    begin = begin + revisionTag.length();
                                    final Integer end = content.indexOf("-->", begin);
                                    final String newVersion = content.substring(begin, end).replace("=", "").replace(" ", "");
                                    if (newVersion != null && !newVersion.isEmpty()) {
                                        schematron.setVersion(newVersion);
                                        schematron = schematronDAO.addOrUpdateSchematron(schematron);
                                        if (schematron != null) {
                                            FacesMessages.instance()
                                                    .add("Version of schematron " + schematron.getName() + " has been updated to " + newVersion);
                                        }
                                    }
                                }
                            } catch (final IOException e) {
                                LOGGER.error(e.getMessage());
                                FacesMessages.instance().add("Version update for " + schematron.getName() + " failed");
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                } else {
                    FacesMessages.instance().add("No file exists at location " + rootPath.concat(schematron.getPath()));
                    continue;
                }
            }
        } else {
            FacesMessages.instance().addFromResourceBundle("gazelle.sch.validator.no.schematron");
        }
    }

    /**
     * Allows to make the conversion
     */
    public void compileSchematronFromGUI(final Schematron schematron) {
        try {
            schematronCompiler.compileSchematron(schematron);
            FacesMessages.instance()
                    .add("The Schematron [" + schematron.getName()
                            + "] compiled with success. The result of compilation available on bin/compilations/"
                            + schematron.getId() + ".xsl");
        } catch (final Exception e) {
            LOGGER.error("not able to compile schematron", e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occure when compiling the schematron.");
        }
    }

    /**
     * Compile all schematrons
     */
    public void compileModifiedSchematrons() {
        final List<Schematron> schematronsToUpdate = schematronDAO.getAllSchematrons();
        if (schematronsToUpdate != null && !schematronsToUpdate.isEmpty()) {
            final String rootPath = schFilePath.getSchematronPathRoot();
            int schemaToValidate = 0;
            int schemaValidated = 0;
            for (final Schematron schematron : schematronsToUpdate) {
                final String schemaPath = schematron.getPath();
                if (schemaPath.endsWith(".sch")) {
                    final File schematronFile = new File(rootPath.concat(schemaPath));
                    if (schematronFile.exists()) {
                        final Date fileDate = new Date(schematronFile.lastModified());
                        if (null != schematron.getlastChanged()) {
                            if (fileDate.after(schematron.getlastChanged())) {
                                schemaToValidate++;
                                try {
                                    schematronCompiler.compileSchematron(schematron);
                                    FacesMessages.instance().add("The Schematron [" + schematron.getName()
                                            + "] compiled with success. The result of compilation available on bin/compilations/" + schematron.getId()
                                            + ".xml");
                                    schemaValidated++;
                                } catch (final Exception e) {
                                    //FacesMessage is displayed in function compileSchematron...
                                    LOGGER.error("Not able to compile schematron", e);
                                }
                            }
                        }
                    }
                }
            }

            if (0 == schemaToValidate) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No schematron compilation available.");
            } else if (schemaToValidate == schemaValidated) {
                FacesMessages.instance().add("All modified schematrons are compiled with success : " + schemaValidated + "/" + schemaToValidate);

            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Some errors occured when compiling modified schematrons : " + schemaValidated + "/" + schemaToValidate);
            }
        }
    }

    /**
     * Compile all schematrons
     *
     */
    public void compileAllSchematrons() {
        final List<Schematron> schematronsToUpdate = schematronDAO.getAllSchematrons();
        if (schematronsToUpdate != null && !schematronsToUpdate.isEmpty()) {
            final String rootPath = schFilePath.getSchematronPathRoot();
            int schemaToValidate= 0;
            int schemaValidated = 0;
            for (final Schematron schematron : schematronsToUpdate) {
                final String schemaPath = schematron.getPath();
                if (schemaPath.endsWith(".sch")){
                    final File schematronFile = new File(rootPath.concat(schemaPath));
                    if (schematronFile.exists()) {
                        schemaToValidate++;
                        try {
                            schematronCompiler.compileSchematron(schematron);
                            schemaValidated++;
                        } catch (final Exception e) {
                            //FacesMessage is displayed in function compileSchematron...
                            LOGGER.error("Not able to compile schematron", e);
                        }
                    }
                }
            }

            if(0 == schemaToValidate) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No schematron compilation available.");
            }
            else if (schemaToValidate == schemaValidated) {
                FacesMessages.instance().add("All modified schematrons are compiled with success : "+ schemaValidated + "/" + schemaToValidate);

            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Some errors occured when compiling modified schematrons : " + schemaValidated + "/" + schemaToValidate);
            }
        }
    }

        /**
         * @param currentSchematron : schematron with the XSD to reload
         * @return boolean true when successful reload else false
         */
    public void reloadXSD(final Schematron currentSchematron) {
        boolean result = false;
        if (!currentSchematron.getXsdPath().isEmpty()) {
            final String xsdLocation = schFilePath.getXSDPath(currentSchematron);
            if (new File(xsdLocation).exists()) {
                result = xMLValidation.reloadXSD(xsdLocation);
            }
        }
        if (result) {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.INFO, "The schematron XSD [" + currentSchematron.getXsdPath() + "] successful reloaded.");
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Errors occurred when reloading the schematron XSD [" + currentSchematron.getXsdPath()
                            + "] : Check XSD file and path then try again...");
        }
    }
}
