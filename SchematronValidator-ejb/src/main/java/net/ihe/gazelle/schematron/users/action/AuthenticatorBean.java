package net.ihe.gazelle.schematron.users.action;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;

/**
 * @author abderrazek boufahja
 */
@Stateless
@Name("authenticator")
public class AuthenticatorBean implements Authenticator {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticatorBean.class);

    @In
    Identity identity;
    @In
    Credentials credentials;
    @In
    ApplicationConfigurationDAO applicationConfigurationDAO;

    public boolean authenticate() {
        LOGGER.info("authenticating {0}", this.credentials.getUsername());
        //write your authentication logic here,
        //return true if the authentication was
        //successful, false otherwise
        if ("admin".equals(this.credentials.getUsername())) {
            this.identity.addRole("admin");
            return true;
        }
        return false;
    }

    public boolean isCas() {
        return !Boolean.parseBoolean(applicationConfigurationDAO.getValue("ip_login"));
    }

    public String loginByIP() {
        Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

}
