package net.ihe.gazelle.schematron.users.action;

import javax.ejb.Local;

/**
 * @author abderrazek boufahja
 */
@Local
public interface Authenticator {

    boolean authenticate();

    public boolean isCas();

    public String loginByIP();

}
