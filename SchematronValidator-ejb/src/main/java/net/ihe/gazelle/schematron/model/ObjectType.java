package net.ihe.gazelle.schematron.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("objectType")
@Table(name = "sch_validator_object_type", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword", name = "uk_sch_validator_object_type_keyword"))
@SequenceGenerator(name = "sch_validator_object_type_sequence", sequenceName = "sch_validator_object_type_id_seq", allocationSize = 1)
public class ObjectType extends AuditModule implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2013153390759620929L;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "sch_validator_object_type_sequence", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "keyword", nullable = false, unique = true)
    private String keyword;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    public ObjectType() {

    }


    /**
     * Getters and Setters
     */

    public String getKeyword() {
        return this.keyword;
    }

    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((this.description == null) ? 0 : this.description.hashCode());
        result = prime * result + ((this.keyword == null) ? 0 : this.keyword.hashCode());
        result = prime * result
                + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final ObjectType other = (ObjectType) obj;
        if (this.description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!this.description.equals(other.description)) {
            return false;
        }
        if (this.keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!this.keyword.equals(other.keyword)) {
            return false;
        }
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
