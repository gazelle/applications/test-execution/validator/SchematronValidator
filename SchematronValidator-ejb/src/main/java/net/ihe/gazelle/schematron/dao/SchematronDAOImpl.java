package net.ihe.gazelle.schematron.dao;

import net.ihe.gazelle.common.filter.BooleanLabelProvider;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.AbstractCriterion;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.model.SchematronQuery;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;

import javax.ejb.Stateless;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Stateless
@AutoCreate
@Name("schematronDAO")
public class SchematronDAOImpl implements SchematronDAO {

    public static final String XSD_VERSION_10  = "1.0";
    public static final String XSD_VERSION_11  = "1.1";
    public static final String TYPE_STANDARD   = "Standard";
    public static final String TYPE_ART_DECOR  = "ART-DECOR";
    private static List<SelectItem> xsdVersions;
    private static List<SelectItem> schematronTypes;

    @Override
    public List<Schematron> getAllSchematrons() {
        final SchematronQuery hq = new SchematronQuery();
        return hq.getListNullIfEmpty();
    }

    @Override
    public List<Schematron> getAllAvailableSchematrons() {
        final SchematronQuery hq = new SchematronQuery();
        hq.available().eq(true);
        return hq.getListNullIfEmpty();
    }

    @Override
    public List<String> getAllAvailableSchematronsKeywords() {
        final List<Schematron> allAvailableSchematrons = getAllAvailableSchematrons();
        final List<String> allAvailableSchematronsKeywords = new ArrayList<>();
        for (final Schematron currentSchematron : allAvailableSchematrons) {
            allAvailableSchematronsKeywords.add(currentSchematron.getKeyword());
        }

        return allAvailableSchematronsKeywords;
    }

    @Override
    public List<Schematron> getAvailableSchematronsForAGivenType(String inTypeKeyword) {
        if (inTypeKeyword == null || inTypeKeyword.length() == 0) {
            return null;
        }
        final SchematronQuery hq = new SchematronQuery();
        hq.available().eq(true);
        hq.objectType().keyword().eq(inTypeKeyword);
        return hq.getListNullIfEmpty();
    }

    @Override
    public Schematron getSchematronByName(String inName) {
        if (inName == null || inName.length() == 0) {
            return null;
        }
        final SchematronQuery hq = new SchematronQuery();
        hq.name().eq(inName);
        return hq.getUniqueResult();
    }

    @Override
    public Schematron getSchematronByKeyword(String inKeyword) {
        if (inKeyword == null || inKeyword.length() == 0) {
            return null;
        }
        final SchematronQuery hq = new SchematronQuery();
        hq.keyword().eq(inKeyword);
        return hq.getUniqueResult();
    }

    @Override
    public Schematron addOrUpdateSchematron(Schematron inSchematron) {
        if (inSchematron == null) {
            return null;
        }
        else {
            final EntityManager em = (EntityManager) Component.getInstance("entityManager");
            if (inSchematron.getName() == null || inSchematron.getName().length() == 0) {
                inSchematron.setName(inSchematron.getKeyword() + " (" + inSchematron.getVersion() + ")");
            }
            inSchematron = (Schematron) em.merge(inSchematron);
            em.flush();
            return inSchematron;
        }
    }

    @Override
    public void deleteSchematron(Schematron inSchematron) {
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        inSchematron = em.find(Schematron.class, inSchematron.getId());
        em.remove(inSchematron);
        em.flush();
        FacesMessages.instance().add("Schematron " + inSchematron.getName() + " deleted");
    }

    @Override
    public boolean isKeywordAlreadyUsed(String keyword) {
        final SchematronQuery schematronQuery = new SchematronQuery();
        schematronQuery.keyword().eq(keyword);
        final int existingKeyword = schematronQuery.getCount();
        return existingKeyword != 0;
    }

    @Override
    public Filter<Schematron> createFilter() {
        return new Filter<Schematron>(getHQLCriteriaForFilter());
    }

    @Override
    public FilterDataModel<Schematron> getFiltered(Filter<Schematron> filter) {
        return new FilterDataModel<Schematron>(filter) {
            @Override
            protected Object getId(Schematron schematron) {
                return schematron.getId();
            }
        };
    }

    /**
     * @return All XSD Versions List
     */
    public SelectItem[] getAllXsdVersions() {
        if(null == xsdVersions) {
            xsdVersions = new ArrayList<>();
        }
        if(xsdVersions.isEmpty()){
            xsdVersions.add(new SelectItem(XSD_VERSION_10, XSD_VERSION_10));
            xsdVersions.add(new SelectItem(XSD_VERSION_11, XSD_VERSION_11));
        }
        return  xsdVersions.toArray(new SelectItem[xsdVersions.size()]);
    }

    /**
     * @return Schematron types List
     */
    public SelectItem[] getAllSchematronTypes() {
        if(null == schematronTypes) {
            schematronTypes = new ArrayList<>();
        }
        if(schematronTypes.isEmpty()){
            schematronTypes.add(new SelectItem(TYPE_STANDARD, TYPE_STANDARD));
            schematronTypes.add(new SelectItem(TYPE_ART_DECOR, TYPE_ART_DECOR));
        }
        return  schematronTypes.toArray(new SelectItem[schematronTypes.size()]);
    }

    protected HQLCriterionsForFilter<Schematron> getHQLCriteriaForFilter() {
        final SchematronQuery query = new SchematronQuery();
        final HQLCriterionsForFilter<Schematron> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("label", query.name());
        criteria.addPath("version", query.version());
        criteria.addPath("path", query.path());
        criteria.addPath("lastChanged", query.lastChanged());

        AbstractCriterion<Schematron, Boolean> availableCriterion =
                (AbstractCriterion<Schematron, Boolean>) criteria.addPath("available", query.available());
        availableCriterion.setLabelProvider(BooleanLabelProvider.INSTANCE);

        criteria.addPath("objectType", query.objectType().keyword());

        criteria.addQueryModifier(new QueryModifier<Schematron>() {
            @Override
            public void modifyQuery(final HQLQueryBuilder<Schematron> hqlQueryBuilder, final Map<String, Object> map) {
            }
        });

        return criteria;
    }
}
