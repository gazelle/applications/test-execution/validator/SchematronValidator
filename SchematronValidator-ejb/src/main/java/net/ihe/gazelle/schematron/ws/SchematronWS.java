package net.ihe.gazelle.schematron.ws;

import net.ihe.gazelle.schematron.model.ObjectType;
import net.ihe.gazelle.schematron.model.Schematron;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.soap.SOAPException;
import java.util.List;

@Remote
public interface SchematronWS {

    /**
     * Returns the list of all available methods to validate objects (CDA, Dicom ...)
     *
     * @return
     * @throws SOAPException
     */
    @WebMethod
    @WebResult(name = "schematrons")
    public List<Schematron> getAllSchematrons() throws SOAPException;

    //FIXME add findSchematronByKeyword method

    /**
     * Returns the list methods available to validate a kind of object (CDA or Dicom or ...)
     *
     * @param inType
     * @return
     * @throws SOAPException
     */
    @WebMethod
    @WebResult(name = "schematrons")
    public List<Schematron> getSchematronsForAGivenType(String inType) throws SOAPException;

    /**
     * Locates the schematron used by the given function and returns a Base64 encoded String representing this schematron
     *
     * @param inMethodName : name of the ObjectMethodValidator
     * @return
     * @throws SOAPException
     */
    @WebMethod
    @WebResult(name = "schematron")
    public String getSchematronByName(
            @WebParam(name = "methodName")
                    String inMethodName) throws SOAPException;

    @WebMethod
    @WebResult(name = "objectType")
    public List<ObjectType> getAllAvailableObjectTypes();
}
