package net.ihe.gazelle.schematron.validation;

import net.sf.saxon.TransformerFactoryImpl;
import org.jdom.Document;
import org.jdom.transform.JDOMResult;
import org.jdom.transform.JDOMSource;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

/**
 * <b>Class Description : </b>
 * <li>This class permits to make the validation of the generated XML file against an xsl file describing
 * an HL7 message profile.</li>
 *
 * @author Abdallah MILADI - Abderrazek Boufahja / INRIA Rennes IHE development Project
 * <pre>http://www.ihe-europe.org</pre>
 * <pre>amiladi@irisa.fr</pre>
 * @version 1.0
 */

public class XMLXSLValidator {

    /**
     * The document that will contain the XML file to be validated
     */
    private Document fileDocument;

    /**
     * The xsl file against it the xml file will be validated
     */

    private Document XSLDocument;

    /**
     * The result of the validation
     */
    private JDOMResult result;

    /**
     * Default constructor:
     *
     * @param fileDocument       The xml file to be validated
     * @param XSLDocument The file against it the file xml file will be validated
     */
    public XMLXSLValidator(Document fileDocument, Document XSLDocument) {
        this.fileDocument = fileDocument;
        this.XSLDocument = XSLDocument;
    }

    /**
     * Sets the document that will be validated
     *
     * @param fileDocument The xml file that will be validated
     */
    public void setXMLDocument(Document fileDocument) {
        this.fileDocument = fileDocument;
    }

    /**
     * Sets the document that will be used to validate the xml file
     *
     * @param XSLDocument The document that will be used to make the validation
     */
    public void setXSLDocument(Document XSLDocument) {
        this.XSLDocument = XSLDocument;
    }

    /**
     * Permits to make the validation
     *
     * @throws TransformerException
     * @throws TransformerException
     */
    public void validate() throws TransformerException {
        TransformerFactory trsfactory = new TransformerFactoryImpl();
        Transformer validator = trsfactory.newTransformer(new JDOMSource(this.XSLDocument));
        this.result = new JDOMResult();
        validator.setErrorListener(new Listener());
        validator.transform(new JDOMSource(this.fileDocument), this.result);
    }

    /**
     * Permits to get the result of the validation
     *
     * @return The result of the validation
     */
    public JDOMResult getResult() {
        return this.result;
    }

    /**
     * <b>Class Description : </b>
     * <li>A listener that permits to customize the behavior of the transformer when encountering errors or warnings.</li>
     *
     * @author Abdallah MILADI / INRIA Rennes IHE development Project
     * <pre>http://www.ihe-europe.org</pre>
     * <pre>amiladi@irisa.fr</pre>
     * @version 1.0
     */
    private class Listener implements javax.xml.transform.ErrorListener {
        /**
         * Customize the behavior of the transformer when encountering a warning
         */
        public void warning(TransformerException e) {
            System.out.println("The warning is " + e.getMessage());
        }

        /**
         * Customize the behavior of the transformer when encountering an error
         */
        public void error(TransformerException e) {
            System.out.println("The error is " + e.getMessage());
        }

        /**
         * Customize the behavior of the transformer when encountering a fatal error
         */
        public void fatalError(TransformerException e) {
            System.out.println(e.getMessage());
        }
    }

}
