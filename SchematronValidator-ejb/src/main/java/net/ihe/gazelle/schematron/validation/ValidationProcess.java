package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.transformation.DfdlTransformation;
import net.ihe.gazelle.schematron.util.DetailedResultMarshaller;
import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.TransformationException;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.ValidationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@Name("validationProcess")
@AutoCreate
public class ValidationProcess {

    @In
    private XMLValidation xMLValidation;

    @In
    private SchematronValidator schematronValidator;

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    @In(value = "detailedResultMarshaller")
    private DetailedResultMarshaller marshaller;

    @In(value = "dldlTransformation")
    private DfdlTransformation dfdlTransformation;

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationProcess.class);
    private static final String PASSED = "PASSED";
    private static final String FAILED = "FAILED";

    public File getFileToValidate(String xmlFile) throws IOException {

        File fileToValidate = File.createTempFile("FileToValidate", ".xml");

        LOGGER.info(fileToValidate.getPath());
        try (FileWriter fw = new FileWriter(fileToValidate)) {
            fw.write(xmlFile);
            fw.close();
        }

        return fileToValidate;
    }

    public void validateXML(DetailedResult res, File file, Schematron schematron) {
        DocumentWellFormed dd = new DocumentWellFormed();
        dd = xMLValidation.isXMLWellFormed(file);
        res.setDocumentWellFormed(dd);

        if (res.getDocumentWellFormed().getResult().equals(PASSED)) {
            // if the document is a CDA for epSOS, check it is valid regarding the epSOS schema
            if ("CDA".equals(schematron.getObjectType().getKeyword()) && schematron.getName().contains("epSOS")) {
                res.setDocumentValidXSD(xMLValidation.isCDAValidForEpsos(file));
            }

            // if the document is a CDA, check it is valid regarding HL7 format
            else if ("CDA".equals(schematron.getObjectType().getKeyword())) {
                res.setDocumentValidXSD(xMLValidation.isFileValid(file, schematron));
            } else if (schematron.getXsdPath() != null && schematron.getXsdPath().length() > 0) {
                res.setDocumentValidXSD(xMLValidation.isFileValid(file, schematron));
            } else {
                final String message = "There is no XSD file defined.";
                res.setDocumentValidXSD(xMLValidation.handleException(message));
            }
        }
    }

    public String validateDocument(String b64ObjToValidate, Schematron schematron, String schematronName) throws IOException, JDOMException, TransformerException, ValidationException {

        String validationResult = null;
        byte[] objToValidate = DatatypeConverter.parseBase64Binary(b64ObjToValidate);
        String xmlToValidate = null;
        DetailedResult result = new DetailedResult();
        if (schematron.getDfdlTransformationNeeded() != null && schematron.getDfdlTransformationNeeded()) {
            try {
                xmlToValidate = dfdlTransformation.doDfdlTransformation(schematron, objToValidate);
            } catch (MalformedURLException | ObjectNotFoundException | CompilationException | TransformationException e) {
                String message = "";
                if (e instanceof MalformedURLException) {
                    message = "Internal error, gazelle_transformation_url is not properly defined";
                } else if (e instanceof ObjectNotFoundException) {
                    message = "DFDL Schema Not Found: " + ((ObjectNotFoundException) e).getMessage();
                } else if (e instanceof CompilationException) {
                    message = "Compilation error: " + ((CompilationException) e).getMessage();
                } else if (e instanceof TransformationException) {
                    message = "Transformation error: " + ((TransformationException) e).getMessage();
                }
                DocumentWellFormed doc = new DocumentWellFormed();
                XSDMessage xsdMessage = new XSDMessage();

                xsdMessage.setSeverity("error");
                xsdMessage.setLineNumber(0);
                xsdMessage.setColumnNumber(0);
                xsdMessage.setMessage(message);

                doc.getXSDMessage().add(xsdMessage);
                doc.setResult(FAILED);

                result.setDocumentWellFormed(doc);
                return marshaller.getDetailedResultAsString(result);
            }
        } else {
            xmlToValidate = new String(objToValidate, StandardCharsets.UTF_8);
        }

        File fileToValidate = null;

        try {
            fileToValidate = getFileToValidate(xmlToValidate);
            validateXML(result, fileToValidate, schematron);

            if (result.getDocumentWellFormed().getResult().equals(PASSED)) {
                schematronValidator.validate(result, schematron, fileToValidate);
            }
        } catch (Exception e) {
            DetailedResult exception_result = new DetailedResult();
            DocumentWellFormed doc = new DocumentWellFormed();
            XSDMessage xsdMessage = new XSDMessage();

            xsdMessage.setSeverity("error");
            xsdMessage.setLineNumber(0);
            xsdMessage.setColumnNumber(0);
            xsdMessage.setMessage(e.getMessage());

            doc.getXSDMessage().add(xsdMessage);
            doc.setResult(FAILED);

            exception_result.setDocumentWellFormed(doc);
            return marshaller.getDetailedResultAsString(exception_result);
        } finally {
            LOGGER.info("deleting temp file");
            Files.delete(fileToValidate.toPath());
        }

        return marshaller.getDetailedResultAsString(result);
    }

}
