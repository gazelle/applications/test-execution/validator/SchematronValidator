package net.ihe.gazelle.schematron.dao;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.schematron.model.ObjectType;
import net.ihe.gazelle.schematron.model.ObjectTypeQuery;
import net.ihe.gazelle.schematron.model.SchematronQuery;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@Stateless
@AutoCreate
@Name("objectTypeDAO")
public class ObjectTypeDAOImpl implements ObjectTypeDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectTypeDAOImpl.class);

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public ObjectType getById(Integer id) {
        if (id == null || id <= 0) {
            return null;
        }
        return entityManager.find(ObjectType.class, id);
    }

    @Override
    public ObjectType getByKeyword(String inKeyword) {
        if (inKeyword == null || inKeyword.length() == 0) {
            return null;
        }
        ObjectTypeQuery otq = new ObjectTypeQuery(entityManager);
        otq.keyword().eq(inKeyword);
        return otq.getUniqueResult();
    }

    @Override
    public List<ObjectType> getAllObjectTypes() {
        Query query = entityManager.createQuery("FROM ObjectType ot  order by ot.id");
        return (List<ObjectType>) query.getResultList();
    }

    @Override
    public ObjectType saveObjectType(ObjectType objectType) {
        if (objectType != null) {
            try {
                objectType = entityManager.merge(objectType);
                entityManager.flush();
            } catch (PersistenceException e) {
                LOGGER.error(e.getMessage());
                throw e;
            }
        }
        return objectType;
    }

    @Override
    public void deleteObjectType(ObjectType objectType) {
        if (objectType != null) {
            objectType = entityManager.find(ObjectType.class, objectType.getId());
            entityManager.remove(objectType);
            entityManager.flush();
            FacesMessages.instance().add("ObjectType " + objectType.getName() + " deleted");
        }
    }

    @Override
    public boolean isObjectTypeDeletable(ObjectType objectType) {
        final SchematronQuery schematronQuery = new SchematronQuery();
        schematronQuery.objectType().id().eq(objectType.getId());
        final int count = schematronQuery.getCount();
        return count == 0;
    }

    @Override
    public Filter<ObjectType> createFilter() {
        return new Filter<ObjectType>(getHQLCriteriaForFilter());
    }

    @Override
    public FilterDataModel<ObjectType> getFiltered(Filter<ObjectType> filter) {
        return new FilterDataModel<ObjectType>(filter) {
            @Override
            protected Object getId(ObjectType objectType) {
                return objectType.getId();
            }
        };
    }

    protected HQLCriterionsForFilter<ObjectType> getHQLCriteriaForFilter() {
        final ObjectTypeQuery query = new ObjectTypeQuery();
        final HQLCriterionsForFilter<ObjectType> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("name", query.name());
        criteria.addPath("keyword", query.keyword());

        criteria.addQueryModifier(new QueryModifier<ObjectType>() {
            @Override
            public void modifyQuery(final HQLQueryBuilder<ObjectType> hqlQueryBuilder, final Map<String, Object> map) {

            }

        });

        return criteria;
    }
}
