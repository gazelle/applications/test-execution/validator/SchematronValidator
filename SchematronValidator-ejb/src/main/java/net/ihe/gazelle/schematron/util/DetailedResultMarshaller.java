package net.ihe.gazelle.schematron.util;

import net.ihe.gazelle.validation.DetailedResult;

public interface DetailedResultMarshaller {

    public String getDetailedResultAsString(DetailedResult dr);

}
