package net.ihe.gazelle.schematron.ws;

import net.ihe.gazelle.schematron.dao.SchematronDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.EmailNotificationManager;
import net.ihe.gazelle.schematron.validation.SchematronValidator;
import net.ihe.gazelle.schematron.validation.ValidationProcess;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jdom.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.ValidationException;
import javax.xml.soap.SOAPException;
import javax.xml.transform.TransformerException;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Name("SchematronValidatorWS")
@WebService(name = "SchematronValidator", serviceName = "SchematronValidatorService", targetNamespace = "http://ws.validator.sch.gazelle.ihe.net/")
public class SchematronValidatorWSImpl implements SchematronValidatorWS {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchematronValidator.class);
    private static final String ERROR = "Error";

    @Resource
    WebServiceContext wsCtx;
    @In
    private SchematronDAO schematronDAO;
    @In
    private ValidationProcess validationProcess;
    @In
    private EmailNotificationManager emailNotificationManager;

    @Override
    public String about() {
        String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate XML file using Schematron validation" +
                " based validation.\n";
        res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
        return res;
    }

    /**
     * Validates an object and returns the result
     *
     * @param base64Document : a string representing the object to validate (content of the file)
     * @param validator      : an XML string containing the parameters to pass to the validator (if needed)
     * @return a result string formatted using a specific XML architecture
     * @throws SOAPException
     */
    @Override
    public String validateBase64Document(
            @WebParam(name = "base64Document") String base64Document,
            @WebParam(name = "validator") String validator)
            throws SOAPException {

        if (base64Document == null || base64Document.length() == 0) {
            throw new SOAPException("You must provide an non-empty object to validate !");
        }

        if (validator == null || validator.length() == 0) {
            throw new SOAPException("You must at least provide the name of the schematron to use for validating the given object");
        }

        //FIXME #1 Must look for schematron by keyword
        final String schematronName = validator;

        final Schematron schematron = schematronDAO.getSchematronByName(schematronName);
        if (schematron == null) {
            throw new SOAPException("No schematron found with name " + schematronName);
        }

        try {
            return validationProcess.validateDocument(base64Document, schematron, schematronName);
        } catch (final IOException | JDOMException | ValidationException | TransformerException | EJBException e) {

            final MessageContext jaxwsContext = this.wsCtx.getMessageContext();
            final HttpServletRequest hRequest = (HttpServletRequest) jaxwsContext.get(MessageContext.SERVLET_REQUEST);
            emailNotificationManager.send(e, "validation has ended in an unexpected way for schematron " + schematronName,
                    "Web service has been called by " + hRequest.getRemoteAddr() + " ( " + hRequest.getRemoteHost() + " )");
            LOGGER.error(e.getMessage(), e);
            throw new SOAPException("validation has ended in an unexpected way for schematron " + schematronName, e);
        }
    }

    @Override
    public String validateDocument(String document, String validator) throws SOAPException {
        String message = "The method validateDocument(String document, String validator) is not implemented";
        LOGGER.warn(message);
        throw new SOAPException(message);
    }


    @Override
    public List<String> getListOfValidators(String descriminator) throws SOAPException {
        if (descriminator == null) {
            descriminator = "";
        }
        List<String> res = new ArrayList<>();
        List<Schematron> schematrons = schematronDAO.getAvailableSchematronsForAGivenType(descriminator);
        if (schematrons != null) {
            for (Schematron validators : schematrons) {
                //FIXME return keyword list instead of name list. (To fix at the same time that fixme #1)
                res.add(validators.getName());
            }

        }
        return res;
    }

}
