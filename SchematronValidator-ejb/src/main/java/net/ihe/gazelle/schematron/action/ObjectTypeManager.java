package net.ihe.gazelle.schematron.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.schematron.dao.ObjectTypeDAO;
import net.ihe.gazelle.schematron.model.ObjectType;
import net.ihe.gazelle.schematron.model.SchematronQuery;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.ejb.Remove;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

@Name("objectTypeManagerBean")
@Scope(ScopeType.PAGE)
public class ObjectTypeManager implements Serializable {

    /**
     *
     */

    private static final long serialVersionUID = 1L;

    @In
    private ObjectTypeDAO objectTypeDAO;

    private transient Filter<ObjectType> filter;
    private ObjectType selectedObjectType;

    public void initCreateObjectType() {
        this.selectedObjectType = new ObjectType();
    }

    public Filter<ObjectType> getFilter() {
        if (this.filter == null) {
            this.filter = objectTypeDAO.createFilter();
        }
        return this.filter;
    }

    public void setFilter(final Filter<ObjectType> filter) {
        this.filter = filter;
    }

    public ObjectType getSelectedObjectType() {
        return this.selectedObjectType;
    }

    public void setSelectedObjectType(final ObjectType selectedObjectType) {
        this.selectedObjectType = selectedObjectType;
    }

    @Destroy
    @Remove
    public void destroy() {
        // TODO Auto-generated method stub

    }

    public String editObjectType(final ObjectType inObjectType) {
        return "/schematrons/editObjectType.seam?id=" + inObjectType.getId();
    }

    public String saveChanges() {
        if (this.selectedObjectType == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occured during the recording of the ObjectType");
            return null;
        } else {
            try {
                selectedObjectType = objectTypeDAO.saveObjectType(selectedObjectType);
                FacesMessages.instance().add("The object type " + this.selectedObjectType.getName() + " has been successfully saved");
                return "/schematrons/manageObjectTypes.seam";
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot save the object type: {0}", e.getMessage());
                return null;
            }

        }
    }

    public FilterDataModel<ObjectType> getAllObjectTypes() {
        return objectTypeDAO.getFiltered(getFilter());
    }

    public void getObjectTypeFromUrl() {
        // If there's an id in the URL, get it and load the selectedObjectType
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && urlParams.containsKey("id")) {
            final HQLQueryBuilder<ObjectType> queryBuilder = new HQLQueryBuilder<>(
                    ObjectType.class);
            final Integer objectTypeId = Integer.parseInt(urlParams.get("id"));
            queryBuilder.addEq("id", objectTypeId);
            this.selectedObjectType = queryBuilder.getUniqueResult();
        }
    }

    public void deleteSelectedObjectType() {
        this.deleteObjectType(this.selectedObjectType);
    }

    private void deleteObjectType(ObjectType objectType) {
        final EntityManager em = (EntityManager) Component.getInstance("entityManager");
        objectType = em.find(ObjectType.class, objectType.getId());
        em.remove(objectType);
        em.flush();
        FacesMessages.instance().add("ObjectType " + objectType.getName() + " deleted");
    }

    public boolean isObjectTypeDeletable(final ObjectType objectType) {

        final SchematronQuery schematronQuery = new SchematronQuery();
        schematronQuery.objectType().id().eq(objectType.getId());
        final int count = schematronQuery.getCount();
        return count == 0;
    }

}
