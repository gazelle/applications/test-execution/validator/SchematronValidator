package net.ihe.gazelle.schematron.ws;

import net.ihe.gazelle.validation.ws.ModelBasedValidationRemote;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.soap.SOAPException;
import java.util.List;

public interface SchematronValidatorWS extends ModelBasedValidationRemote {
    @WebMethod
    @WebResult(name = "about")
    String about();

    @Override
    @WebMethod
    @WebResult(name = "DetailedResult")
    String validateBase64Document(
            @WebParam(name = "base64Document") String base64Document,
            @WebParam(name = "validator") String validator)
            throws SOAPException;

    @Override
    String validateDocument(String document, String validator) throws SOAPException;

    @Override
    List<String> getListOfValidators(String descriminator) throws SOAPException;
}
