package net.ihe.gazelle.schematron.transformation;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.TransformationException;
import net.ihe.gazelle.transformation.ws.TransformationWSClient;
import net.ihe.gazelle.transformation.ws.TransformedXMLStringWithDFDLReference;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import java.net.MalformedURLException;

@AutoCreate
@Name("dldlTransformation")
public class DfdlTransformationImpl implements DfdlTransformation {

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    @Override
    public String doDfdlTransformation(Schematron schematron, byte[] objToValidate) throws MalformedURLException, CompilationException, TransformationException, ObjectNotFoundException {
        TransformationWSClient transformationWSClient = new TransformationWSClient(
                applicationConfigurationDAO.getValue("gazelle_transformation_url"));
        TransformedXMLStringWithDFDLReference transformedXml = transformationWSClient
                .transformDataGivenReferenceToDfdlSchema(schematron.getDfdlSchemaKeyword(), objToValidate);
        return transformedXml.getTransformedXmlAsString();
    }
}
