package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.EmailNotificationManager;
import net.ihe.gazelle.schematron.util.SchFilePath;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.XSDMessage;
import net.ihe.gazelle.xmltools.xsd.ValidationException;
import net.ihe.gazelle.xmltools.xsd.XMLValidator;
import net.ihe.gazelle.xmltools.xsd.XSDValidator;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Name("xMLValidation")
@AutoCreate
public class XMLValidation {

    @In
    private SchFilePath schFilePath;

    @In
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    @In
    EmailNotificationManager emailNotificationManager;

    private static final String PASSED = "PASSED";

    private static final String FAILED = "FAILED";

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLValidation.class);
    private static final String XSD_VERSION_USED_11 = "1.1";

    private static Map<String, SAXParserFactory> listFactory = new HashMap<>();


    /**
     * Checks the CDA document is valid for epSOS (uses CDA_extended.xsd file)
     *
     * @param file : xml file
     */
    public DocumentValidXSD isCDAValidForEpsos(File file) {
        final String ap = applicationConfigurationDAO.getValue("epsos_cda_xsd_path");
        if (ap != null) {
            // Validate using XSD1.0
            return validXMLUsingXSD10(file, ap);
        } else {
            String res = "There is no XSD file defined.";
            return handleException(res);
        }
    }

    public DocumentValidXSD isFileValid(File file, Schematron schematron) {
        DocumentValidXSD doc = new DocumentValidXSD();
        if (file == null || !file.exists() || schematron.getXsdPath() == null || schematron.getXsdPath().isEmpty()) {
            final String message = "XSD file missing";
            return handleException(message);
        } else {
            final String xsdLocation = schFilePath.getXSDPath(schematron);

            final File temp = new File(xsdLocation);
            if (!temp.exists()) {
                final String message = "XSD file missing: the file " + xsdLocation + " is missing";
                emailNotificationManager.send(null, "XSD file missing", message);
                return handleException(message);
            }

            if (XSD_VERSION_USED_11.equals(schematron.getXsdVersion())) {
                LOGGER.info("XML validation using XSD 1.1");
                return validXMLUsingXSD11(file, xsdLocation);
            } else {
                LOGGER.info("XML validation using XSD 1.0");
                return validXMLUsingXSD10(file, xsdLocation);
            }
        }
    }

    public boolean reloadXSD(final String xsdLocation) {
        if (listFactory.get(xsdLocation) != null) {
            listFactory.remove(xsdLocation);
        }
        return getSAXParserFactoryFromSchema(xsdLocation) != null;
    }

    DocumentValidXSD validXMLUsingXSD10(File file, String xsdLocation) {
        DocumentValidXSD doc = new DocumentValidXSD();
        List<ValidationException> exceptions = new ArrayList<ValidationException>();
        try {
            final SAXParserFactory fact = getSAXParserFactoryFromSchema(xsdLocation);
            if (fact == null) {
                String message = "Error occured on the server, the schema cannot be loaded";
                return handleException(message);
            } else {
                XSDValidator xsdValidator = new XSDValidator();
                exceptions = xsdValidator.validateUsingFactoryAndSchema(new FileInputStream(file), xsdLocation, fact);
            }
        } catch (Exception e) {
            exceptions.add(handleException(e));
        }
        return extractValidationResult(exceptions, xsdLocation, doc);
    }

    DocumentValidXSD validXMLUsingXSD11(File file, String xsdLocation) {
        DocumentValidXSD doc = new DocumentValidXSD();
        List<ValidationException> exceptions = new ArrayList<ValidationException>();
        try {
            XSDValidator xsdValidator = new XSDValidator();
            xsdValidator.setValidatorJarPath(applicationConfigurationDAO.getValue("xsd_1_1_validator_path"));
            exceptions = xsdValidator.validate(new FileInputStream(file), xsdLocation);
        } catch (Exception e) {
            exceptions.add(handleException(e));
        }
        return extractValidationResult(exceptions, xsdLocation, doc);
    }


    private SAXParserFactory getSAXParserFactoryFromSchema(final String xsdLocation) {
        if (listFactory.get(xsdLocation) != null) {
            return listFactory.get(xsdLocation);
        }
        try {
            final SAXParserFactory fact = XSDValidator.initFactoryFromSchema(xsdLocation);
            if (fact != null) {
                listFactory.put(xsdLocation, fact);
                return fact;
            } else {
                throw new SAXException("problem to create factory from schema");
            }
        } catch (final SAXException e) {
            LOGGER.warn("Unexpected error parsing the schema : " + xsdLocation, e);
        }
        return null;
    }


    /**
     * Checks that it is a well-formed XML file
     *
     * @param file
     * @return
     */
    public DocumentWellFormed isXMLWellFormed(File file) {
        DocumentWellFormed dwf = new DocumentWellFormed();
        if (file == null) {
            String res = "Document passed is empty.";
            DocumentWellFormed doc = new DocumentWellFormed();
            XSDMessage xsdMessage = new XSDMessage();

            xsdMessage.setSeverity("error");
            xsdMessage.setLineNumber(0);
            xsdMessage.setColumnNumber(0);
            xsdMessage.setMessage(res);

            doc.getXSDMessage().add(xsdMessage);
            doc.setResult(FAILED);

            return doc;
        } else {
            try {
                dwf = wellFormed(file, null, dwf);
                return dwf;
            } catch (Exception e) {
                String res = "Error: Cannot initialize SAX parser";

                DocumentWellFormed doc = new DocumentWellFormed();
                XSDMessage xsdMessage = new XSDMessage();

                xsdMessage.setSeverity("error");
                xsdMessage.setLineNumber(0);
                xsdMessage.setColumnNumber(0);
                xsdMessage.setMessage(res);

                doc.getXSDMessage().add(xsdMessage);
                doc.setResult(FAILED);

                return doc;
            }
        }
    }

    private DocumentWellFormed wellFormed(File file, String xsdpath, DocumentWellFormed dv) {
        List<net.ihe.gazelle.xmltools.xsd.ValidationException> exceptions = new ArrayList<ValidationException>();
        try {
            exceptions = XMLValidator.validate(new FileInputStream(file));
        } catch (Exception e) {
            exceptions.add(handleException(e));
        }
        return extractValidationResult(exceptions, xsdpath, dv);
    }

    private static <T extends DocumentValidXSD> T extractValidationResult(List<ValidationException> exceptions, String xsdpath, T dv) {

        dv.setResult(PASSED);

        if (exceptions == null || exceptions.size() == 0) {
            dv.setResult(PASSED);
            return dv;
        } else {
            Integer nbOfErrors = 0;
            Integer nbOfWarnings = 0;
            Integer exceptionCounter = 0;
            for (ValidationException ve : exceptions) {
                if (ve.getSeverity() == null) {
                    ve.setSeverity("error");
                }
                exceptionCounter++;
                if ((ve.getSeverity() != null) && (ve.getSeverity().equals("warning"))) {
                    nbOfWarnings++;
                } else {
                    nbOfErrors ++;
                }
                XSDMessage xsd = new XSDMessage();
                xsd.setSeverity(ve.getSeverity());

                if (StringUtils.isNumeric(ve.getLineNumber()) && StringUtils.isNumeric(ve.getColumnNumber())) {
                    xsd.setMessage("Line:Col[" + ve.getLineNumber() + ":" + ve.getColumnNumber() + "]:" + ve.getMessage());
                } else {
                    xsd.setMessage(ve.getMessage());
                }
                dv.getXSDMessage().add(xsd);
            }
            dv.setNbOfErrors(nbOfErrors.toString());
            dv.setNbOfWarnings(nbOfWarnings.toString());
            if (nbOfErrors > 0) {
                dv.setResult(FAILED);
            }
            return dv;
        }

    }

    private static ValidationException handleException(Exception e) {
        ValidationException ve = new ValidationException();
        ve.setLineNumber("0");
        ve.setColumnNumber("0");
        if (e != null && e.getMessage() != null) {
            ve.setMessage("error on validating : " + e.getMessage());
        } else if (e != null && e.getCause() != null && e.getCause().getMessage() != null) {
            ve.setMessage("error on validating : " + e.getCause().getMessage());
        } else {
            ve.setMessage("error on validating. The exception generated is of kind : " + e.getClass().getSimpleName());
        }
        ve.setSeverity("error");
        return ve;
    }

    public DocumentValidXSD handleException(String message) {
        LOGGER.error(message);

        DocumentValidXSD doc = new DocumentValidXSD();
        XSDMessage xsdMessage = new XSDMessage();

        xsdMessage.setSeverity("error");
        xsdMessage.setLineNumber(0);
        xsdMessage.setColumnNumber(0);
        xsdMessage.setMessage(message);

        doc.getXSDMessage().add(xsdMessage);
        doc.setResult(FAILED);

        return doc;
    }
}
