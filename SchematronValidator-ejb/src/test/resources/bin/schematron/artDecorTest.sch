<?xml version="1.0" encoding="UTF-8"?>

<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process">

    <sch:ns uri="urn:ihe:iti:xdw:2011" prefix="xdw"/>
    <sch:ns uri="urn:hl7-org:v3" prefix="hl7"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" prefix="ws-ht"/>
    <sch:ns uri="urn:rve:2018:xdw:extension" prefix="ext2018"/>
    <sch:ns uri="urn:rve:2017:xdw:extension" prefix="ext2017"/>

    <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="soap"/>
    <sch:ns uri="urn:oasis:names:tc:SAML:2.0:assertion" prefix="saml"/>
    <sch:ns uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
        prefix="wsse"/>
    <sch:ns uri="urn:ihe:iti:xds-b:2007" prefix="ihe"/>

    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0" prefix="lcm"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0" prefix="query"/>

    <!-- controlli WD -->
    <sch:extends href="artDecorSub1.sch"/>

    <!-- confronti -->
    <sch:extends href="artDecorSub2.sch"/>

    <sch:pattern>


    </sch:pattern>
</sch:schema>