<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process">

    <sch:ns uri="urn:ihe:iti:xdw:2011" prefix="xdw"/>
    <sch:ns uri="urn:hl7-org:v3" prefix="hl7"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://docs.oasis-open.org/ns/bpel4people/ws-humantask/types/200803" prefix="ws-ht"/>
    <sch:ns uri="urn:rve:2018:xdw:extension" prefix="ext2018"/>
    <sch:ns uri="urn:rve:2017:xdw:extension" prefix="ext2017"/>
    <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="env"/>
    <sch:ns uri="urn:hl7-org:v2xml" prefix="adt"/>


    <sch:pattern>

        <sch:let name="MPI" value="2857784"/>
        <sch:let name="SAR_request" value="document('SAR_request.xml')"/>

        <sch:rule context="xdw:XDW.WorkflowDocument">
            <sch:assert test="count(xdw:id)=1">
                Error1
            </sch:assert>
        </sch:rule>


        <sch:rule context="xdw:XDW.WorkflowDocument/xdw:id">
            <sch:assert test="count(@root)=1">
                Error2
            </sch:assert>
        </sch:rule>
    </sch:pattern>
</sch:schema>