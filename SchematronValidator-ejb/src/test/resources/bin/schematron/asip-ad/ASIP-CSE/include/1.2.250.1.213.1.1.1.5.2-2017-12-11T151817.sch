<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.250.1.213.1.1.1.5.2
Name: Cerficat du 9e mois
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.2.250.1.213.1.1.1.5.2-2017-12-11T151817">
    <title>Cerficat du 9e mois</title>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]" id="d506897e2303-false-d561630e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2'])&gt;=1">(CerficatDu9eMois): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2'])&lt;=1">(CerficatDu9eMois): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:id)&gt;=1">(CerficatDu9eMois): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:id)&lt;=1">(CerficatDu9eMois): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:code)&gt;=1">(CerficatDu9eMois): element hl7:code is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:code)&lt;=1">(CerficatDu9eMois): element hl7:code appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:title)&lt;=1">(CerficatDu9eMois): element hl7:title appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:effectiveTime)&gt;=1">(CerficatDu9eMois): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:effectiveTime)&lt;=1">(CerficatDu9eMois): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component)&gt;=1">(CerficatDu9eMois): element hl7:component is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component)&lt;=1">(CerficatDu9eMois): element hl7:component appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:componentOf[not(@nullFlavor)])&gt;=1">(CerficatDu9eMois): element hl7:componentOf[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:componentOf[not(@nullFlavor)])&lt;=1">(CerficatDu9eMois): element hl7:componentOf[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:relatedDocument[@typeCode='RPLC'])&gt;=1">(CerficatDu9eMois): element hl7:relatedDocument[@typeCode='RPLC'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:relatedDocument[@typeCode='RPLC'])&lt;=1">(CerficatDu9eMois): element hl7:relatedDocument[@typeCode='RPLC'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:documentationOf[not(@nullFlavor)])&gt;=1">(CerficatDu9eMois): element hl7:documentationOf[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:documentationOf[not(@nullFlavor)])&lt;=1">(CerficatDu9eMois): element hl7:documentationOf[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:custodian[not(@nullFlavor)])&gt;=1">(CerficatDu9eMois): element hl7:custodian[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:custodian[not(@nullFlavor)])&lt;=1">(CerficatDu9eMois): element hl7:custodian[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]])&gt;=1">(CerficatDu9eMois): element hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]])&lt;=1">(CerficatDu9eMois): element hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:author[not(@nullFlavor)])&gt;=1">(CerficatDu9eMois): element hl7:author[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:author[not(@nullFlavor)])&lt;=1">(CerficatDu9eMois): element hl7:author[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:informant)&gt;=1">(CerficatDu9eMois): element hl7:informant is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:informant)&lt;=1">(CerficatDu9eMois): element hl7:informant appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:participant)&gt;=1">(CerficatDu9eMois): element hl7:participant is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:participant)&lt;=1">(CerficatDu9eMois): element hl7:participant appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:recordTarget)&gt;=1">(CerficatDu9eMois): element hl7:recordTarget is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:recordTarget)&lt;=1">(CerficatDu9eMois): element hl7:recordTarget appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']" id="d506897e2304-false-d562099e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CerficatDu9eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="string(@root)=('1.2.250.1.213.1.1.1.5.2')">(CerficatDu9eMois): The value for @root SHALL be '1.2.250.1.213.1.1.1.5.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:id
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:id" id="d506897e2306-false-d562113e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CerficatDu9eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:code
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:code" id="d506897e2307-false-d562123e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CerficatDu9eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:title
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:title" id="d506897e2308-false-d562133e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CerficatDu9eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:effectiveTime
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:effectiveTime" id="d506897e2309-false-d562143e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CerficatDu9eMois): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="not(*)">(CerficatDu9eMois): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component" id="d506897e2310-false-d562307e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:structuredBody)&gt;=1">(CerficatDu9eMois): element hl7:structuredBody is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:structuredBody)&lt;=1">(CerficatDu9eMois): element hl7:structuredBody appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody
Item: (CerficatDu9eMois)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody" id="d506897e2311-false-d562624e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]])&gt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.5.2" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]])&lt;=1">(CerficatDu9eMois): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]
Item: (CerficatDu9eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.8']]]
Item: (CerficatDu9eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.15']]]
Item: (CerficatDu9eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.3']]]
Item: (CerficatDu9eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.13.2.5']]]
Item: (CerficatDu9eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.23'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.6']]]
Item: (CerficatDu9eMois)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.5.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:component/hl7:structuredBody/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]
Item: (CerficatDu9eMois)
-->
<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]" id="d563114e12-false-d563146e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:encompassingEncounter[not(@nullFlavor)])&gt;=1">(CI-SISComponentOf): element hl7:encompassingEncounter[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:encompassingEncounter[not(@nullFlavor)])&lt;=1">(CI-SISComponentOf): element hl7:encompassingEncounter[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]" id="d563114e13-false-d563208e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')])&lt;=1">(CI-SISComponentOf): element hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:effectiveTime)&gt;=1">(CI-SISComponentOf): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:effectiveTime)&lt;=1">(CI-SISComponentOf): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:dischargeDispositionCode)&lt;=1">(CI-SISComponentOf): element hl7:dischargeDispositionCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:responsibleParty)&lt;=1">(CI-SISComponentOf): element hl7:responsibleParty appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]])&gt;=1">(CI-SISComponentOf): element hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]])&lt;=1">(CI-SISComponentOf): element hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:id
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:id" id="d563114e19-false-d563290e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@root">(CI-SISComponentOf): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISComponentOf): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:code[(@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')]" id="d563114e27-false-d563312e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@nullFlavor or (@code='IMP' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EMER' and @codeSystem='2.16.840.1.113883.5.4') or (@code='AMB' and @codeSystem='2.16.840.1.113883.5.4') or (@code='FLD' and @codeSystem='2.16.840.1.113883.5.4') or (@code='HH' and @codeSystem='2.16.840.1.113883.5.4') or (@code='VR' and @codeSystem='2.16.840.1.113883.5.4') or (@code='EXTERNE' and @codeSystem='1.2.250.1.213.1.1.4.2.291') or (@code='SEANCE' and @codeSystem='1.2.250.1.213.1.1.4.2.291')">(CI-SISComponentOf): The element value SHALL be one of 'code 'IMP' codeSystem '2.16.840.1.113883.5.4' or code 'EMER' codeSystem '2.16.840.1.113883.5.4' or code 'AMB' codeSystem '2.16.840.1.113883.5.4' or code 'FLD' codeSystem '2.16.840.1.113883.5.4' or code 'HH' codeSystem '2.16.840.1.113883.5.4' or code 'VR' codeSystem '2.16.840.1.113883.5.4' or code 'EXTERNE' codeSystem '1.2.250.1.213.1.1.4.2.291' or code 'SEANCE' codeSystem '1.2.250.1.213.1.1.4.2.291''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime" id="d563114e70-false-d563356e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:low)&lt;=1">(CI-SISComponentOf): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:high)&lt;=1">(CI-SISComponentOf): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:low
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:low" id="d563114e76-false-d563380e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@value">(CI-SISComponentOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISComponentOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:high
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:effectiveTime/hl7:high" id="d563114e84-false-d563397e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@value">(CI-SISComponentOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISComponentOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:dischargeDispositionCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:dischargeDispositionCode" id="d563114e92-false-d563414e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@codeSystem)=('1.2.250.1.213.2.14') or not(@codeSystem)">(CI-SISComponentOf): The value for @codeSystem SHALL be '1.2.250.1.213.2.14'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty" id="d563114e107-false-d563453e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d563114e113-false-d563492e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedPerson)&lt;=1">(CI-SISComponentOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:representedOrganization)&lt;=1">(CI-SISComponentOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d563114e121-false-d563558e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d563559e76-false-d563581e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d563559e94-false-d563679e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d563559e98-false-d563689e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d563559e102-false-d563699e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d563559e110-false-d563709e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d563559e118-false-d563719e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d563559e122-false-d563729e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d563559e126-false-d563739e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d563559e130-false-d563749e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d563559e175-false-d563759e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d563559e190-false-d563769e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d563559e200-false-d563779e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d563559e204-false-d563789e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d563559e208-false-d563799e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d563559e226-false-d563855e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d563559e232-false-d563865e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d563559e236-false-d563875e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d563559e240-false-d563885e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d563559e244-false-d563895e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d563559e248-false-d563905e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d563906e27-false-d563916e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson" id="d563114e126-false-d563937e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&gt;=1">(CI-SISComponentOf): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name" id="d563114e127-false-d563953e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&gt;=1">(CI-SISComponentOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&lt;=1">(CI-SISComponentOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:given)&lt;=1">(CI-SISComponentOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:prefix)&lt;=1">(CI-SISComponentOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization" id="d563114e140-false-d564014e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISComponentOf): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d563114e141-false-d564071e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name" id="d563114e149-false-d564092e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d564093e70-false-d564104e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country" id="d564093e88-false-d564202e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state" id="d564093e92-false-d564212e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city" id="d564093e96-false-d564222e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d564093e104-false-d564232e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d564093e112-false-d564242e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d564093e116-false-d564252e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d564093e120-false-d564262e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d564093e124-false-d564272e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d564093e169-false-d564282e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d564093e184-false-d564292e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d564093e194-false-d564302e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d564093e198-false-d564312e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d564093e202-false-d564322e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d564093e220-false-d564378e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d564093e226-false-d564388e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d564093e230-false-d564398e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d564093e234-false-d564408e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d564093e238-false-d564418e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d564093e242-false-d564428e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom" id="d564429e27-false-d564439e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:responsibleParty/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode" id="d563114e152-false-d564460e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@codeSystem">(CI-SISComponentOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISComponentOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant" id="d563114e165-false-d564502e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISComponentOf): element hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d563114e171-false-d564541e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:assignedPerson)&lt;=1">(CI-SISComponentOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:representedOrganization)&lt;=1">(CI-SISComponentOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d563114e179-false-d564597e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d564598e76-false-d564620e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d564598e94-false-d564718e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d564598e98-false-d564728e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d564598e102-false-d564738e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d564598e110-false-d564748e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d564598e118-false-d564758e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d564598e122-false-d564768e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d564598e126-false-d564778e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d564598e130-false-d564788e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d564598e175-false-d564798e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d564598e190-false-d564808e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d564598e200-false-d564818e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d564598e204-false-d564828e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d564598e208-false-d564838e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d564598e226-false-d564894e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d564598e232-false-d564904e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d564598e236-false-d564914e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d564598e240-false-d564924e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d564598e244-false-d564934e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d564598e248-false-d564944e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d564945e27-false-d564955e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson" id="d563114e184-false-d564976e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&gt;=1">(CI-SISComponentOf): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name" id="d563114e185-false-d564992e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&gt;=1">(CI-SISComponentOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:family)&lt;=1">(CI-SISComponentOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:given)&lt;=1">(CI-SISComponentOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:prefix)&lt;=1">(CI-SISComponentOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISComponentOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization" id="d563114e189-false-d565053e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISComponentOf): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISComponentOf): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d563114e190-false-d565100e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@extension">(CI-SISComponentOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISComponentOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISComponentOf): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:name" id="d563114e198-false-d565121e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d565122e70-false-d565133e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:country" id="d565122e88-false-d565231e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:state" id="d565122e92-false-d565241e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:city" id="d565122e96-false-d565251e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d565122e104-false-d565261e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d565122e112-false-d565271e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d565122e116-false-d565281e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d565122e120-false-d565291e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d565122e124-false-d565301e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d565122e169-false-d565311e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d565122e184-false-d565321e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d565122e194-false-d565331e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d565122e198-false-d565341e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr" id="d565122e202-false-d565351e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d565122e220-false-d565407e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d565122e226-false-d565417e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d565122e230-false-d565427e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d565122e234-false-d565437e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d565122e238-false-d565447e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d565122e242-false-d565457e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:telecom" id="d565458e27-false-d565468e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:encounterParticipant/hl7:assignedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:representedOrganization/hl7:standardIndustryClassCode" id="d563114e201-false-d565489e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@displayName">(CI-SISComponentOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISComponentOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@codeSystem">(CI-SISComponentOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISComponentOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@code">(CI-SISComponentOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISComponentOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]" id="d563114e214-false-d565526e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]])&gt;=1">(CI-SISComponentOf): element hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]])&lt;=1">(CI-SISComponentOf): element hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]" id="d563114e220-false-d565554e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)])&gt;=1">(CI-SISComponentOf): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)])&lt;=1">(CI-SISComponentOf): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location)&gt;=1">(CI-SISComponentOf): element hl7:location is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:location)&lt;=1">(CI-SISComponentOf): element hl7:location appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]" id="d563114e226-false-d565589e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISComponentOf): The element value SHALL be one of '1.2.250.1.213.1.1.5.3 J02-HealthcareFacilityTypeCode (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location" id="d563114e236-false-d565610e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="count(hl7:name)&lt;=1">(CI-SISComponentOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="$elmcount&lt;=1">(CI-SISComponentOf): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.2
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:name
Item: (CI-SISComponentOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:name" id="d563114e242-false-d565644e0">
        <extends rule="EN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.2" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='EN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISComponentOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:EN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr" id="d565645e70-false-d565656e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:country" id="d565645e88-false-d565754e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:state" id="d565645e92-false-d565764e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:city" id="d565645e96-false-d565774e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postalCode" id="d565645e104-false-d565784e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumber" id="d565645e112-false-d565794e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:houseNumberNumeric" id="d565645e116-false-d565804e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetName" id="d565645e120-false-d565814e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetNameType" id="d565645e124-false-d565824e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:additionalLocator" id="d565645e169-false-d565834e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:unitID" id="d565645e184-false-d565844e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:postBox" id="d565645e194-false-d565854e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:precinct" id="d565645e198-false-d565864e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr" id="d565645e202-false-d565874e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d565645e220-false-d565930e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d565645e226-false-d565940e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d565645e230-false-d565950e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d565645e234-false-d565960e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d565645e238-false-d565970e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:componentOf[not(@nullFlavor)]/hl7:encompassingEncounter[not(@nullFlavor)]/hl7:location[hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]]/hl7:healthCareFacility[hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.3-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem)]]/hl7:location/hl7:addr/hl7:streetAddressLine" id="d565645e242-false-d565980e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:relatedDocument[@typeCode='RPLC']
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:relatedDocument[@typeCode='RPLC']" id="d565981e48-false-d565991e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="string(@typeCode)=('RPLC')">(CI-SISRelatedDocument): The value for @typeCode SHALL be 'RPLC'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:parentDocument[not(@nullFlavor)])&gt;=1">(CI-SISRelatedDocument): element hl7:parentDocument[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:parentDocument[not(@nullFlavor)])&lt;=1">(CI-SISRelatedDocument): element hl7:parentDocument[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]" id="d565981e50-false-d566011e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISRelatedDocument): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISRelatedDocument): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.3
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (CI-SISRelatedDocument)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:relatedDocument[@typeCode='RPLC']/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]" id="d565981e51-false-d566027e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRelatedDocument): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRelatedDocument): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="@root">(CI-SISRelatedDocument): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.3" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISRelatedDocument): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]" id="d566028e25-false-d566059e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:serviceEvent[not(@nullFlavor)])&gt;=1">(CI-SISDocumentationOf): element hl7:serviceEvent[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:serviceEvent[not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:serviceEvent[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]" id="d566028e26-false-d566095e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:code)&lt;=1">(CI-SISDocumentationOf): element hl7:code appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:effectiveTime)&lt;=1">(CI-SISDocumentationOf): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:performer[@typeCode='PRF'][not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:performer[@typeCode='PRF'][not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:code
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:code" id="d566028e34-false-d566132e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISDocumentationOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@codeSystem">(CI-SISDocumentationOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISDocumentationOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@code">(CI-SISDocumentationOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISDocumentationOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime" id="d566028e58-false-d566160e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:low)&gt;=1">(CI-SISDocumentationOf): element hl7:low is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:low)&lt;=1">(CI-SISDocumentationOf): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:high)&lt;=1">(CI-SISDocumentationOf): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:low
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:low" id="d566028e62-false-d566187e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@value">(CI-SISDocumentationOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISDocumentationOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:high
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:effectiveTime/hl7:high" id="d566028e70-false-d566204e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@value">(CI-SISDocumentationOf): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISDocumentationOf): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]" id="d566028e78-false-d566231e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="string(@typeCode)=('PRF')">(CI-SISDocumentationOf): The value for @typeCode SHALL be 'PRF'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedEntity)&gt;=1">(CI-SISDocumentationOf): element hl7:assignedEntity is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedEntity)&lt;=1">(CI-SISDocumentationOf): element hl7:assignedEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity" id="d566028e117-false-d566271e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&gt;=1">(CI-SISDocumentationOf): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&lt;=1">(CI-SISDocumentationOf): element hl7:id appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:assignedPerson)&lt;=1">(CI-SISDocumentationOf): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:representedOrganization)&gt;=1">(CI-SISDocumentationOf): element hl7:representedOrganization is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:representedOrganization)&lt;=1">(CI-SISDocumentationOf): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:id
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:id" id="d566028e130-false-d566338e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@extension">(CI-SISDocumentationOf): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISDocumentationOf): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@root">(CI-SISDocumentationOf): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISDocumentationOf): Attribute @root SHALL be of data type 'uid'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:telecom" id="d566339e35-false-d566363e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr" id="d566364e85-false-d566386e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:country" id="d566364e103-false-d566484e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:state" id="d566364e107-false-d566494e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:city" id="d566364e111-false-d566504e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postalCode" id="d566364e119-false-d566514e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumber" id="d566364e127-false-d566524e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric" id="d566364e131-false-d566534e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetName" id="d566364e135-false-d566544e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetNameType" id="d566364e139-false-d566554e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator" id="d566364e184-false-d566564e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:unitID" id="d566364e199-false-d566574e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:postBox" id="d566364e209-false-d566584e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:precinct" id="d566364e213-false-d566594e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr" id="d566364e217-false-d566604e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d566364e235-false-d566660e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d566364e241-false-d566670e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d566364e245-false-d566680e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d566364e249-false-d566690e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d566364e253-false-d566700e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d566364e257-false-d566710e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name" id="d566028e136-false-d566730e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:family)&gt;=1">(CI-SISDocumentationOf): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:family)&lt;=1">(CI-SISDocumentationOf): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:given)&lt;=1">(CI-SISDocumentationOf): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:prefix)&lt;=1">(CI-SISDocumentationOf): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISDocumentationOf)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization" id="d566028e143-false-d566790e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:id)&lt;=1">(CI-SISDocumentationOf): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:name)&lt;=1">(CI-SISDocumentationOf): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:standardIndustryClassCode[not(@nullFlavor)])&gt;=1">(CI-SISDocumentationOf): element hl7:standardIndustryClassCode[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="count(hl7:standardIndustryClassCode[not(@nullFlavor)])&lt;=1">(CI-SISDocumentationOf): element hl7:standardIndustryClassCode[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:id
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:id" id="d566028e147-false-d566849e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:name
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:name" id="d566028e148-false-d566859e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom" id="d566860e27-false-d566870e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr" id="d566871e85-false-d566893e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country" id="d566871e103-false-d566991e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state" id="d566871e107-false-d567001e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city" id="d566871e111-false-d567011e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode" id="d566871e119-false-d567021e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber" id="d566871e127-false-d567031e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric" id="d566871e131-false-d567041e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName" id="d566871e135-false-d567051e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType" id="d566871e139-false-d567061e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator" id="d566871e184-false-d567071e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID" id="d566871e199-false-d567081e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox" id="d566871e209-false-d567091e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct" id="d566871e213-false-d567101e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr" id="d566871e217-false-d567111e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d566871e235-false-d567167e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d566871e241-false-d567177e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d566871e245-false-d567187e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d566871e249-false-d567197e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d566871e253-false-d567207e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine" id="d566871e257-false-d567217e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.4
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[not(@nullFlavor)]
Item: (CI-SISDocumentationOf)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:documentationOf[not(@nullFlavor)]/hl7:serviceEvent[not(@nullFlavor)]/hl7:performer[@typeCode='PRF'][not(@nullFlavor)]/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[not(@nullFlavor)]" id="d566028e151-false-d567227e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISDocumentationOf): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@displayName">(CI-SISDocumentationOf): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISDocumentationOf): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@codeSystem">(CI-SISDocumentationOf): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISDocumentationOf): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="@code">(CI-SISDocumentationOf): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.4" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISDocumentationOf): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]" id="d567228e29-false-d567264e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:assignedCustodian)&gt;=1">(CI-SISCustodian): element hl7:assignedCustodian is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:assignedCustodian)&lt;=1">(CI-SISCustodian): element hl7:assignedCustodian appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian" id="d567228e35-false-d567290e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:representedCustodianOrganization)&gt;=1">(CI-SISCustodian): element hl7:representedCustodianOrganization is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:representedCustodianOrganization)&lt;=1">(CI-SISCustodian): element hl7:representedCustodianOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization" id="d567228e41-false-d567316e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISCustodian): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISCustodian): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:name)&lt;=1">(CI-SISCustodian): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="count(hl7:telecom)&lt;=1">(CI-SISCustodian): element hl7:telecom appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="$elmcount&lt;=1">(CI-SISCustodian): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id[not(@nullFlavor)]
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:id[not(@nullFlavor)]" id="d567228e92-false-d567374e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISCustodian): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISCustodian): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="@root">(CI-SISCustodian): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISCustodian): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="hl7:custodian/hl7:representedCustodianOrganization/hl7:id/@root='1.2.250.1.71.4.2.2' or hl7:custodian/hl7:representedCustodianOrganization/hl7:id/@root='1.2.250.1.213.4.1'">(CI-SISCustodian): L'attribut @root doit avoir la valeur :
- soit "1.2.250.1.71.4.2.2" pour l'OID des structures de santé
- soit "1.2.250.1.213.4.1" pour l'OID de l'organisation hébergeant les documents produits par le patient</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.5
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name
Item: (CI-SISCustodian)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:name" id="d567228e110-false-d567395e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.5" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISCustodian): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:telecom" id="d567396e27-false-d567406e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr" id="d567407e85-false-d567429e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:country" id="d567407e103-false-d567527e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:state" id="d567407e107-false-d567537e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:city" id="d567407e111-false-d567547e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postalCode" id="d567407e119-false-d567557e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumber" id="d567407e127-false-d567567e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:houseNumberNumeric" id="d567407e131-false-d567577e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetName" id="d567407e135-false-d567587e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetNameType" id="d567407e139-false-d567597e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:additionalLocator" id="d567407e184-false-d567607e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:unitID" id="d567407e199-false-d567617e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:postBox" id="d567407e209-false-d567627e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:precinct" id="d567407e213-false-d567637e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr" id="d567407e217-false-d567647e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d567407e235-false-d567703e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d567407e241-false-d567713e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d567407e245-false-d567723e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d567407e249-false-d567733e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d567407e253-false-d567743e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:custodian[not(@nullFlavor)]/hl7:assignedCustodian/hl7:representedCustodianOrganization/hl7:addr/hl7:streetAddressLine" id="d567407e257-false-d567753e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]" id="d567754e25-false-d567775e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:time)&gt;=1">(CI-SISLegalAuthenticator): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:time)&lt;=1">(CI-SISLegalAuthenticator): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:signatureCode[@code='S' or @nullFlavor])&gt;=1">(CI-SISLegalAuthenticator): element hl7:signatureCode[@code='S' or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:signatureCode[@code='S' or @nullFlavor])&lt;=1">(CI-SISLegalAuthenticator): element hl7:signatureCode[@code='S' or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedEntity)&gt;=1">(CI-SISLegalAuthenticator): element hl7:assignedEntity is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedEntity)&lt;=1">(CI-SISLegalAuthenticator): element hl7:assignedEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:time
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:time" id="d567754e26-false-d567822e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(*)">(CI-SISLegalAuthenticator): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@value">(CI-SISLegalAuthenticator): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISLegalAuthenticator): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:signatureCode[@code='S' or @nullFlavor]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:signatureCode[@code='S' or @nullFlavor]" id="d567754e34-false-d567843e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@nullFlavor or (@code='S')">(CI-SISLegalAuthenticator): The element value SHALL be one of 'code 'S''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity" id="d567754e41-false-d567869e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedPerson[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:assignedPerson[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:assignedPerson[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:assignedPerson[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']])&lt;=1">(CI-SISLegalAuthenticator): element hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:id[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:id[not(@nullFlavor)]" id="d567754e51-false-d567937e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@root">(CI-SISLegalAuthenticator): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISLegalAuthenticator): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@extension">(CI-SISLegalAuthenticator): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISLegalAuthenticator): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr" id="d567938e78-false-d567963e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:country" id="d567938e96-false-d568061e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:state" id="d567938e100-false-d568071e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:city" id="d567938e104-false-d568081e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postalCode" id="d567938e112-false-d568091e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumber" id="d567938e120-false-d568101e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric" id="d567938e124-false-d568111e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetName" id="d567938e128-false-d568121e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetNameType" id="d567938e132-false-d568131e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:additionalLocator" id="d567938e177-false-d568141e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:unitID" id="d567938e192-false-d568151e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:postBox" id="d567938e202-false-d568161e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:precinct" id="d567938e206-false-d568171e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr" id="d567938e210-false-d568181e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d567938e228-false-d568237e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d567938e234-false-d568247e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d567938e238-false-d568257e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d567938e242-false-d568267e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d567938e246-false-d568277e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine" id="d567938e250-false-d568287e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:telecom" id="d568288e27-false-d568298e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]" id="d567754e73-false-d568319e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name[not(@nullFlavor)])&gt;=1">(CI-SISLegalAuthenticator): element hl7:name[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name[not(@nullFlavor)])&lt;=1">(CI-SISLegalAuthenticator): element hl7:name[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]" id="d567754e80-false-d568335e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:family)&gt;=1">(CI-SISLegalAuthenticator): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:family)&lt;=1">(CI-SISLegalAuthenticator): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:given)&lt;=1">(CI-SISLegalAuthenticator): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:prefix)&lt;=1">(CI-SISLegalAuthenticator): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (CI-SISLegalAuthenticator)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]" id="d567754e90-false-d568396e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&gt;=1">(CI-SISLegalAuthenticator): element hl7:id[@root='1.2.250.1.71.4.2.2'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISLegalAuthenticator): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:name)&lt;=1">(CI-SISLegalAuthenticator): element hl7:name appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="count(hl7:standardIndustryClassCode)&lt;=1">(CI-SISLegalAuthenticator): element hl7:standardIndustryClassCode appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d567754e91-false-d568457e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@extension">(CI-SISLegalAuthenticator): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISLegalAuthenticator): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISLegalAuthenticator): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:name
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:name" id="d567754e94-false-d568478e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.6
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:standardIndustryClassCode
Item: (CI-SISLegalAuthenticator)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:standardIndustryClassCode" id="d567754e95-false-d568488e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISLegalAuthenticator): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@displayName">(CI-SISLegalAuthenticator): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISLegalAuthenticator): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@codeSystem">(CI-SISLegalAuthenticator): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISLegalAuthenticator): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="@code">(CI-SISLegalAuthenticator): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.6" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISLegalAuthenticator): Attribute @code SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:telecom" id="d568489e39-false-d568520e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr" id="d568521e85-false-d568543e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:country" id="d568521e103-false-d568641e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:state" id="d568521e107-false-d568651e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:city" id="d568521e111-false-d568661e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postalCode" id="d568521e119-false-d568671e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumber" id="d568521e127-false-d568681e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:houseNumberNumeric" id="d568521e131-false-d568691e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetName" id="d568521e135-false-d568701e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetNameType" id="d568521e139-false-d568711e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:additionalLocator" id="d568521e184-false-d568721e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:unitID" id="d568521e199-false-d568731e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:postBox" id="d568521e209-false-d568741e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:precinct" id="d568521e213-false-d568751e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr" id="d568521e217-false-d568761e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d568521e235-false-d568817e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d568521e241-false-d568827e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d568521e245-false-d568837e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d568521e249-false-d568847e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d568521e253-false-d568857e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code='S' or @nullFlavor]]/hl7:assignedEntity/hl7:representedOrganization[hl7:id[@root='1.2.250.1.71.4.2.2']]/hl7:addr/hl7:streetAddressLine" id="d568521e257-false-d568867e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]" id="d568868e99-false-d568884e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')])&lt;=1">(CI-SISAuthor): element hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:time)&gt;=1">(CI-SISAuthor): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:time)&lt;=1">(CI-SISAuthor): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthor[not(@nullFlavor)])&gt;=1">(CI-SISAuthor): element hl7:assignedAuthor[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthor[not(@nullFlavor)])&lt;=1">(CI-SISAuthor): element hl7:assignedAuthor[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]" id="d568868e100-false-d568924e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or (@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant admis le patient dans la structure de soins') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin responsable du patient dans la structure de soins') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant décidé la sortie du patient de la structure de soins') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin intervenant principal') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='premier assistant lors de l’intervention') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='second assistant lors de l’intervention') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e)') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin anesthésiste') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e) anesthésiste') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='sage-femme') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin traitant') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin correspondant') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin remplaçant du médecin traitant') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='gynécologue') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='ophtalmologue') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='psychiatre ou neuropsychiatre') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='cardiologue traitant') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre spécialiste') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre PS informateur')">(CI-SISAuthor): The element value SHALL be one of 'code 'ADMPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant admis le patient dans la structure de soins' or code 'ATTPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin responsable du patient dans la structure de soins' or code 'DISPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant décidé la sortie du patient de la structure de soins' or code 'PRISURG' codeSystem '2.16.840.1.113883.5.88' displayName='médecin intervenant principal' or code 'FASST' codeSystem '2.16.840.1.113883.5.88' displayName='premier assistant lors de l’intervention' or code 'SASST' codeSystem '2.16.840.1.113883.5.88' displayName='second assistant lors de l’intervention' or code 'NASST' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e)' or code 'ANEST' codeSystem '2.16.840.1.113883.5.88' displayName='médecin anesthésiste' or code 'ANRS' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e) anesthésiste' or code 'MDWF' codeSystem '2.16.840.1.113883.5.88' displayName='sage-femme' or code 'PCP' codeSystem '2.16.840.1.113883.5.88' displayName='médecin traitant' or code 'CORRE' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin correspondant' or code 'REMPL' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin remplaçant du médecin traitant' or code 'GYNEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='gynécologue' or code 'OPHTA' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='ophtalmologue' or code 'PSYCH' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='psychiatre ou neuropsychiatre' or code 'CARDT' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='cardiologue traitant' or code 'PRELV' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques' or code 'ASPEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre spécialiste' or code 'APSIN' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre PS informateur''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISAuthor): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@codeSystem">(CI-SISAuthor): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISAuthor): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@code">(CI-SISAuthor): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISAuthor): Attribute @code SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:originalText)&lt;=1">(CI-SISAuthor): element hl7:originalText appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]/hl7:originalText
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280')]/hl7:originalText" id="d568868e180-false-d569022e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:time
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:time" id="d568868e186-false-d569032e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(*)">(CI-SISAuthor): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@value">(CI-SISAuthor): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISAuthor): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]" id="d568868e194-false-d569058e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id)&gt;=1">(CI-SISAuthor): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id)&lt;=1">(CI-SISAuthor): element hl7:id appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAuthor): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedPerson)&lt;=1">(CI-SISAuthor): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:assignedAuthoringDevice)&lt;=1">(CI-SISAuthor): element hl7:assignedAuthoringDevice appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:representedOrganization)&lt;=1">(CI-SISAuthor): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:id
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:id" id="d568868e205-false-d569132e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@root">(CI-SISAuthor): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISAuthor): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@extension">(CI-SISAuthor): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAuthor): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d568868e223-false-d569159e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.1-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAuthor): The element value SHALL be one of '1.2.250.1.213.1.1.5.1 JDV_J01-XdsAuthorSpecialty-CISIS (DYNAMIC)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr" id="d569160e75-false-d569182e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:country" id="d569160e93-false-d569280e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:state" id="d569160e97-false-d569290e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:city" id="d569160e101-false-d569300e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postalCode" id="d569160e109-false-d569310e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumber" id="d569160e117-false-d569320e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:houseNumberNumeric" id="d569160e121-false-d569330e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetName" id="d569160e125-false-d569340e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetNameType" id="d569160e129-false-d569350e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:additionalLocator" id="d569160e174-false-d569360e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:unitID" id="d569160e189-false-d569370e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:postBox" id="d569160e199-false-d569380e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:precinct" id="d569160e203-false-d569390e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr" id="d569160e207-false-d569400e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d569160e225-false-d569456e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d569160e231-false-d569466e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d569160e235-false-d569476e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d569160e239-false-d569486e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d569160e243-false-d569496e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:addr/hl7:streetAddressLine" id="d569160e247-false-d569506e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:telecom" id="d569507e27-false-d569517e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson" id="d568868e259-false-d569538e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&gt;=1">(CI-SISAuthor): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&lt;=1">(CI-SISAuthor): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name" id="d568868e265-false-d569554e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:family)&gt;=1">(CI-SISAuthor): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:family)&lt;=1">(CI-SISAuthor): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:given)&lt;=1">(CI-SISAuthor): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAuthor): element hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISAuthor)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISAuthor)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedPerson/hl7:name/hl7:prefix[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d568868e295-false-d569608e0">
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.24-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAuthor): The element value SHALL be one of '1.2.250.1.213.1.1.5.24 JDV_J12-CiviliteTitre (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice" id="d568868e302-false-d569622e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:manufacturerModelName)&lt;=1">(CI-SISAuthor): element hl7:manufacturerModelName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:softwareName)&lt;=1">(CI-SISAuthor): element hl7:softwareName appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName" id="d568868e308-false-d569642e0">
        <extends rule="SC"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SC' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SC", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:softwareName
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:assignedAuthoringDevice/hl7:softwareName" id="d568868e314-false-d569652e0">
        <extends rule="SC"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SC' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SC", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization" id="d568868e320-false-d569662e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISAuthor): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="count(hl7:name)&lt;=1">(CI-SISAuthor): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d568868e326-false-d569684e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="@extension">(CI-SISAuthor): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAuthor): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISAuthor): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.7
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:name
Item: (CI-SISAuthor)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:author[not(@nullFlavor)]/hl7:assignedAuthor[not(@nullFlavor)]/hl7:representedOrganization/hl7:name" id="d568868e339-false-d569705e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.7" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAuthor): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant
Item: (CI-SISInformant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant" id="d569706e29-false-d569746e0">
        <let name="elmcount" value="count(hl7:assignedEntity|hl7:relatedEntity)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="$elmcount&gt;=1">(CI-SISInformant): choice (hl7:assignedEntity or hl7:relatedEntity) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="$elmcount&lt;=1">(CI-SISInformant): choice (hl7:assignedEntity or hl7:relatedEntity) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="count(hl7:assignedEntity)&lt;=1">(CI-SISInformant): element hl7:assignedEntity appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.12" test="count(hl7:relatedEntity)&lt;=1">(CI-SISInformant): element hl7:relatedEntity appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISInformant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id)&gt;=1">(CI-SISAssignedEntity): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id)&lt;=1">(CI-SISAssignedEntity): element hl7:id appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:assignedPerson)&lt;=1">(CI-SISAssignedEntity): element hl7:assignedPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:representedOrganization)&lt;=1">(CI-SISAssignedEntity): element hl7:representedOrganization appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:id
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:id">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@root">(CI-SISAssignedEntity): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISAssignedEntity): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@extension">(CI-SISAssignedEntity): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAssignedEntity): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&gt;=1">(CI-SISAssignedEntity): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&lt;=1">(CI-SISAssignedEntity): element hl7:name appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:prefix)&lt;=1">(CI-SISAssignedEntity): element hl7:prefix appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:given)&lt;=1">(CI-SISAssignedEntity): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:family)&gt;=1">(CI-SISAssignedEntity): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:family)&lt;=1">(CI-SISAssignedEntity): element hl7:family appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:prefix
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:given
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name/hl7:family
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISAssignedEntity): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:name)&lt;=1">(CI-SISAssignedEntity): element hl7:name appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="count(hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISAssignedEntity): element hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@extension">(CI-SISAssignedEntity): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISAssignedEntity): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISAssignedEntity): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:name
Item: (CI-SISAssignedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.14
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISAssignedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:standardIndustryClassCode[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]">
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.14" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.4-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISAssignedEntity): The element value SHALL be one of '1.2.250.1.213.1.1.5.4 J04-XdsPracticeSettingCode (2018-01-05T00:00:00)'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.12
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISInformant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@classCode">(CI-SISRelatedEntity): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(CI-SISRelatedEntity): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@classCode='ECON' or @classCode='GUARD' or @classCode='QUAL' or @classCode='POLHOLD' or @classCode='CON'">(CI-SISRelatedEntity): @classCode doit être renseigné avec une de ces valeurs :
"ECON" pour personne à prévenir en cas d’urgence
"GUARD" pour rôle de tuteur légal
"QUAL" pour personne de confiance
"POLHOLD" pour assuré ouvrant droit
"CON" pour informateur</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:code)&lt;=1">(CI-SISRelatedEntity): element hl7:code appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="$elmcount&gt;=1">(CI-SISRelatedEntity): choice (hl7:addr or hl7:addr) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:telecom)&gt;=1">(CI-SISRelatedEntity): element hl7:telecom is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:relatedPerson)&gt;=1">(CI-SISRelatedEntity): element hl7:relatedPerson is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:relatedPerson)&lt;=1">(CI-SISRelatedEntity): element hl7:relatedPerson appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:code
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:code">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@displayName">(CI-SISRelatedEntity): attribute @displayName SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@displayName) or string-length(@displayName)&gt;0">(CI-SISRelatedEntity): Attribute @displayName SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@codeSystem">(CI-SISRelatedEntity): attribute @codeSystem SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(CI-SISRelatedEntity): Attribute @codeSystem SHALL be of data type 'oid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="@code">(CI-SISRelatedEntity): attribute @code SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="not(@code) or (string-length(@code)&gt;0 and not(matches(@code,'\s')))">(CI-SISRelatedEntity): Attribute @code SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:originalText)&lt;=1">(CI-SISRelatedEntity): element hl7:originalText appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:code/hl7:originalText
Item: (CI-SISRelatedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:country">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:state">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:city">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postalCode">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumber">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:houseNumberNumeric">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetName">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetNameType">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:additionalLocator">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:unitID">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:postBox">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:precinct">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:addr/hl7:streetAddressLine">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:telecom">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:name)&gt;=1">(CI-SISRelatedEntity): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:name)&lt;=1">(CI-SISRelatedEntity): element hl7:name appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name
Item: (CI-SISRelatedEntity)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:family)&gt;=1">(CI-SISRelatedEntity): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:family)&lt;=1">(CI-SISRelatedEntity): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.13" test="count(hl7:given)&lt;=1">(CI-SISRelatedEntity): element hl7:given appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name/hl7:family
Item: (CI-SISRelatedEntity)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.13
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:informant/hl7:relatedEntity/hl7:relatedPerson/hl7:name/hl7:given
Item: (CI-SISRelatedEntity)
-->
<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant" id="d571401e22-false-d571414e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@typeCode">(CI-SISParticipant): attribute @typeCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@typeCode) or (string-length(@typeCode)&gt;0 and not(matches(@typeCode,'\s')))">(CI-SISParticipant): Attribute @typeCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor])&lt;=1">(CI-SISParticipant): element hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:time)&gt;=1">(CI-SISParticipant): element hl7:time is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:time)&lt;=1">(CI-SISParticipant): element hl7:time appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&gt;=1">(CI-SISParticipant): element hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']])&lt;=1">(CI-SISParticipant): element hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]" id="d571401e43-false-d571462e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@nullFlavor or (@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant admis le patient dans la structure de soins') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin responsable du patient dans la structure de soins') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin ayant décidé la sortie du patient de la structure de soins') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin intervenant principal') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='premier assistant lors de l’intervention') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='second assistant lors de l’intervention') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e)') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin anesthésiste') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='infirmier(e) anesthésiste') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='sage-femme') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88' and @displayName='médecin traitant') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin correspondant') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='médecin remplaçant du médecin traitant') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='gynécologue') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='ophtalmologue') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='psychiatre ou neuropsychiatre') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='cardiologue traitant') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre spécialiste') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280' and @displayName='autre PS informateur')">(CI-SISParticipant): The element value SHALL be one of 'code 'ADMPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant admis le patient dans la structure de soins' or code 'ATTPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin responsable du patient dans la structure de soins' or code 'DISPHYS' codeSystem '2.16.840.1.113883.5.88' displayName='médecin ayant décidé la sortie du patient de la structure de soins' or code 'PRISURG' codeSystem '2.16.840.1.113883.5.88' displayName='médecin intervenant principal' or code 'FASST' codeSystem '2.16.840.1.113883.5.88' displayName='premier assistant lors de l’intervention' or code 'SASST' codeSystem '2.16.840.1.113883.5.88' displayName='second assistant lors de l’intervention' or code 'NASST' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e)' or code 'ANEST' codeSystem '2.16.840.1.113883.5.88' displayName='médecin anesthésiste' or code 'ANRS' codeSystem '2.16.840.1.113883.5.88' displayName='infirmier(e) anesthésiste' or code 'MDWF' codeSystem '2.16.840.1.113883.5.88' displayName='sage-femme' or code 'PCP' codeSystem '2.16.840.1.113883.5.88' displayName='médecin traitant' or code 'CORRE' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin correspondant' or code 'REMPL' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='médecin remplaçant du médecin traitant' or code 'GYNEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='gynécologue' or code 'OPHTA' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='ophtalmologue' or code 'PSYCH' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='psychiatre ou neuropsychiatre' or code 'CARDT' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='cardiologue traitant' or code 'PRELV' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='préleveur d’échantillons biologiques pour l’exécution d’une prescription d’examens biologiques' or code 'ASPEC' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre spécialiste' or code 'APSIN' codeSystem '1.2.250.1.213.1.1.4.2.280' displayName='autre PS informateur''.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:originalText)&lt;=1">(CI-SISParticipant): element hl7:originalText appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]/hl7:originalText
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:functionCode[(@code='ADMPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ATTPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='DISPHYS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PRISURG' and @codeSystem='2.16.840.1.113883.5.88') or (@code='FASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='SASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='NASST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANEST' and @codeSystem='2.16.840.1.113883.5.88') or (@code='ANRS' and @codeSystem='2.16.840.1.113883.5.88') or (@code='MDWF' and @codeSystem='2.16.840.1.113883.5.88') or (@code='PCP' and @codeSystem='2.16.840.1.113883.5.88') or (@code='CORRE' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='REMPL' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='GYNEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='OPHTA' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PSYCH' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='CARDT' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='PRELV' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='ASPEC' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or (@code='APSIN' and @codeSystem='1.2.250.1.213.1.1.4.2.280') or @nullFlavor]/hl7:originalText" id="d571401e119-false-d571542e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:time
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:time" id="d571401e125-false-d571552e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:low)&lt;=1">(CI-SISParticipant): element hl7:low appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:high)&lt;=1">(CI-SISParticipant): element hl7:high appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:time/hl7:low
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:time/hl7:low" id="d571401e129-false-d571576e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@value">(CI-SISParticipant): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISParticipant): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:time/hl7:high
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:time/hl7:high" id="d571401e137-false-d571593e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@value">(CI-SISParticipant): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISParticipant): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]" id="d571401e147-false-d571617e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@classCode">(CI-SISParticipant): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(CI-SISParticipant): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&gt;=1">(CI-SISParticipant): element hl7:id[@root='1.2.250.1.71.4.2.1'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:id[@root='1.2.250.1.71.4.2.1'])&lt;=1">(CI-SISParticipant): element hl7:id[@root='1.2.250.1.71.4.2.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor])&lt;=1">(CI-SISParticipant): element hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor] appears too often [max 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedPerson)&gt;=1">(CI-SISParticipant): element hl7:associatedPerson is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:associatedPerson)&lt;=1">(CI-SISParticipant): element hl7:associatedPerson appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:id[@root='1.2.250.1.71.4.2.1']" id="d571401e157-false-d571690e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@extension">(CI-SISParticipant): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISParticipant): Attribute @extension SHALL be of data type 'st'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="string(@root)=('1.2.250.1.71.4.2.1')">(CI-SISParticipant): The value for @root SHALL be '1.2.250.1.71.4.2.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:code[concat(@code,@codeSystem)=doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1]/conceptList/concept/concat(@code,@codeSystem) or @nullFlavor]" id="d571401e192-false-d571714e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISParticipant): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <let name="theCode" value="@code"/>
        <let name="theCodeSystem" value="@codeSystem"/>
        <let name="theCodeSystemVersion" value="@codeSystemVersion"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="@nullFlavor or exists(doc('include/voc-1.2.250.1.213.1.1.5.1-2018-01-05T000000.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion] or completeCodeSystem[@codeSystem=$theCodeSystem][not(@codeSystemVersion) or @codeSystemVersion=$theCodeSystemVersion]])">(CI-SISParticipant): The element value SHALL be one of '1.2.250.1.213.1.1.5.1 JDV_J01-XdsAuthorSpecialty-CISIS (2018-01-05T00:00:00)'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d571715e75-false-d571734e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:country" id="d571715e93-false-d571832e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:state" id="d571715e97-false-d571842e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:city" id="d571715e101-false-d571852e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postalCode" id="d571715e109-false-d571862e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumber" id="d571715e117-false-d571872e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:houseNumberNumeric" id="d571715e121-false-d571882e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetName" id="d571715e125-false-d571892e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetNameType" id="d571715e129-false-d571902e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:additionalLocator" id="d571715e174-false-d571912e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:unitID" id="d571715e189-false-d571922e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:postBox" id="d571715e199-false-d571932e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:precinct" id="d571715e203-false-d571942e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr" id="d571715e207-false-d571952e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d571715e225-false-d572008e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d571715e231-false-d572018e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d571715e235-false-d572028e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d571715e239-false-d572038e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d571715e243-false-d572048e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:addr/hl7:streetAddressLine" id="d571715e247-false-d572058e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:telecom" id="d572059e27-false-d572069e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson" id="d571401e218-false-d572090e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:name)&gt;=1">(CI-SISParticipant): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:name)&lt;=1">(CI-SISParticipant): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name
Item: (CI-SISParticipant)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name" id="d571401e323-false-d572106e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:family)&gt;=1">(CI-SISParticipant): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:family)&lt;=1">(CI-SISParticipant): element hl7:family appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:given)&lt;=1">(CI-SISParticipant): element hl7:given appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.9" test="count(hl7:prefix)&lt;=1">(CI-SISParticipant): element hl7:prefix appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:family
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:given
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.9
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:participant/hl7:associatedEntity[hl7:id[@root='1.2.250.1.71.4.2.1']]/hl7:associatedPerson/hl7:name/hl7:prefix
Item: (CI-SISParticipant)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget" id="d572152e71-false-d572171e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patientRole)&gt;=1">(CI-SISRecordTarget): element hl7:patientRole is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patientRole)&lt;=1">(CI-SISRecordTarget): element hl7:patientRole appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole" id="d572152e72-false-d572213e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(CI-SISRecordTarget): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&gt;=1">(CI-SISRecordTarget): choice (hl7:addr or hl7:addr) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]])&gt;=1">(CI-SISRecordTarget): element hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]])&lt;=1">(CI-SISRecordTarget): element hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:id[not(@nullFlavor)]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:id[not(@nullFlavor)]" id="d572152e78-false-d572277e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@root">(CI-SISRecordTarget): attribute @root SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(CI-SISRecordTarget): Attribute @root SHALL be of data type 'uid'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@extension">(CI-SISRecordTarget): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRecordTarget): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr" id="d572278e78-false-d572303e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:country" id="d572278e96-false-d572401e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:state" id="d572278e100-false-d572411e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:city" id="d572278e104-false-d572421e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postalCode" id="d572278e112-false-d572431e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumber" id="d572278e120-false-d572441e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:houseNumberNumeric" id="d572278e124-false-d572451e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetName" id="d572278e128-false-d572461e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetNameType" id="d572278e132-false-d572471e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:additionalLocator" id="d572278e177-false-d572481e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:unitID" id="d572278e192-false-d572491e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:postBox" id="d572278e202-false-d572501e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:precinct" id="d572278e206-false-d572511e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr" id="d572278e210-false-d572521e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d572278e228-false-d572577e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d572278e234-false-d572587e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d572278e238-false-d572597e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d572278e242-false-d572607e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d572278e246-false-d572617e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:addr/hl7:streetAddressLine" id="d572278e250-false-d572627e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:telecom" id="d572628e27-false-d572638e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]" id="d572152e91-false-d572668e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name[not(@nullFlavor)])&gt;=1">(CI-SISRecordTarget): element hl7:name[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name[not(@nullFlavor)])&lt;=1">(CI-SISRecordTarget): element hl7:name[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor])&gt;=1">(CI-SISRecordTarget): element hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor])&lt;=1">(CI-SISRecordTarget): element hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthTime)&gt;=1">(CI-SISRecordTarget): element hl7:birthTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthTime)&lt;=1">(CI-SISRecordTarget): element hl7:birthTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:birthplace)&lt;=1">(CI-SISRecordTarget): element hl7:birthplace appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]" id="d572152e100-false-d572724e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:given)&gt;=1">(CI-SISRecordTarget): element hl7:given is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&gt;=1">(CI-SISRecordTarget): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&lt;=3">(CI-SISRecordTarget): element hl7:family appears too often [max 3x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:given
Item: (CI-SISRecordTarget)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:family
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:name[not(@nullFlavor)]/hl7:family" id="d572152e112-false-d572758e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@qualifier">(CI-SISRecordTarget): attribute @qualifier SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@qualifier) or (string-length(@qualifier)&gt;0 and not(matches(@qualifier,'\s')))">(CI-SISRecordTarget): Attribute @qualifier SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="//hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='BR' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='SP' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='CL'">(CI-SISRecordTarget): Valeur du nom typé par l’attribut qualifier : 
                "BR" pour le nom de famille
                "SP" pour le nom de d’usage
                "CL" pour le pseudonyme</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]" id="d572152e121-false-d572772e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@nullFlavor or (@code='M' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Masculin') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Féminin') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1' and @displayName='Inconnu')">(CI-SISRecordTarget): The element value SHALL be one of 'code 'M' codeSystem '2.16.840.1.113883.5.1' displayName='Masculin' or code 'F' codeSystem '2.16.840.1.113883.5.1' displayName='Féminin' or code 'U' codeSystem '2.16.840.1.113883.5.1' displayName='Inconnu''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthTime
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthTime" id="d572152e125-false-d572794e0">
        <extends rule="TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(*)">(CI-SISRecordTarget): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@value">(CI-SISRecordTarget): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@value) or matches(string(@value), '^[0-9]{4,14}')">(CI-SISRecordTarget): Attribute @value SHALL be of data type 'ts'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian" id="d572152e141-false-d572820e0">
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <let name="elmcount" value="count(hl7:guardianPerson|hl7:guardianOrganization)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&gt;=1">(CI-SISRecordTarget): choice (hl7:guardianPerson or hl7:guardianOrganization) does not contain enough elements [min 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&lt;=1">(CI-SISRecordTarget): choice (hl7:guardianPerson or hl7:guardianOrganization) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:guardianPerson)&lt;=1">(CI-SISRecordTarget): element hl7:guardianPerson appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:guardianOrganization)&lt;=1">(CI-SISRecordTarget): element hl7:guardianOrganization appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr" id="d572817e126-false-d572885e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:country" id="d572817e144-false-d572983e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:state" id="d572817e148-false-d572993e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:city" id="d572817e152-false-d573003e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postalCode" id="d572817e160-false-d573013e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumber" id="d572817e168-false-d573023e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:houseNumberNumeric" id="d572817e172-false-d573033e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetName" id="d572817e176-false-d573043e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetNameType" id="d572817e180-false-d573053e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:additionalLocator" id="d572817e225-false-d573063e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:unitID" id="d572817e240-false-d573073e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:postBox" id="d572817e250-false-d573083e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:precinct" id="d572817e254-false-d573093e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr" id="d572817e258-false-d573103e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d572817e276-false-d573159e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d572817e282-false-d573169e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d572817e286-false-d573179e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d572817e290-false-d573189e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d572817e294-false-d573199e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:addr/hl7:streetAddressLine" id="d572817e298-false-d573209e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.19
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:telecom
Item: (CI-SISTelecom)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:telecom" id="d573210e27-false-d573220e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISTelecom): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISTelecom): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@value">(CI-SISTelecom): attribute @value SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="not(@value) or string(@value castable as xs:anyURI)">(CI-SISTelecom): Attribute @value SHALL be of data type 'url'<value-of select="@value"/>
        </assert>
        <let name="prefix" value="substring-before(@value, ':')"/>
        <let name="suffix" value="substring-after(@value, ':')"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="(             (count(@*) = 1 and name(@*) = 'nullFlavor' and             (@* = 'UNK' or @* = 'NASK' or @* = 'ASKU' or @* = 'NAV' or @* = 'MSK')) or             ($suffix and (             $prefix = 'tel' or              $prefix = 'fax' or              $prefix = 'mailto' or              $prefix = 'http' or              $prefix = 'ftp' or              $prefix = 'mllp'))             )">(CI-SISTelecom): Erreur de conformité CI-SIS : <name/> n'est pas conforme à une adresse de télécommunication préfixe:chaîne 
            (avec préfixe = tel, fax, mailto, http, ftp ou mllp) 
            ou est vide et sans nullFlavor, ou contient un nullFlavor non admis.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.19" test="@use = 'H'                      or @use = 'HP'                      or @use = 'HV'                      or @use = 'WP'                     or @use = 'DIR'                      or @use = 'PUB'                      or @use = 'EC'                      or @use = 'MC'                      or @use = 'PG'                      or not(@use)">(CI-SISTelecom): Erreur de conformité CI-SIS : L'attribut use de l'élément telecom n'est pas conforme. 
            Il est facultatif et les valeurs permises sont 'H','HP', 'HV','WP','DIR','PUB','EC','MC','PG'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson" id="d572152e151-false-d573241e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&gt;=1">(CI-SISRecordTarget): element hl7:name is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name" id="d572152e155-false-d573257e0">
        <extends rule="PN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='PN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:PN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&gt;=1">(CI-SISRecordTarget): element hl7:family is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:family)&lt;=3">(CI-SISRecordTarget): element hl7:family appears too often [max 3x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:family
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:family" id="d572152e159-false-d573281e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@qualifier">(CI-SISRecordTarget): attribute @qualifier SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@qualifier) or (string-length(@qualifier)&gt;0 and not(matches(@qualifier,'\s')))">(CI-SISRecordTarget): Attribute @qualifier SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="//hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='BR' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='SP' or //hl7:recordTarget/hl7:patientRole/hl7:patient/hl7:name/hl7:family/@qualifier='CL'">(CI-SISRecordTarget): Valeur du nom typé par l’attribut qualifier : 
                      "BR" pour le nom de famille
                      "SP" pour le nom de d’usage
                      "CL" pour le pseudonyme</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianPerson/hl7:name/hl7:given
Item: (CI-SISRecordTarget)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization" id="d572152e177-false-d573301e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:id[@root='1.2.250.1.71.4.2.2'])&lt;=1">(CI-SISRecordTarget): element hl7:id[@root='1.2.250.1.71.4.2.2'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:id[@root='1.2.250.1.71.4.2.2']" id="d572152e181-false-d573323e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="string(@root)=('1.2.250.1.71.4.2.2')">(CI-SISRecordTarget): The value for @root SHALL be '1.2.250.1.71.4.2.2'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="@extension">(CI-SISRecordTarget): attribute @extension SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="not(@extension) or string-length(@extension)&gt;0">(CI-SISRecordTarget): Attribute @extension SHALL be of data type 'st'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:guardian/hl7:guardianOrganization/hl7:name" id="d572152e194-false-d573344e0">
        <extends rule="ON"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ON' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ON", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace" id="d572152e202-false-d573357e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:place)&gt;=1">(CI-SISRecordTarget): element hl7:place is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:place)&lt;=1">(CI-SISRecordTarget): element hl7:place appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place" id="d572152e203-false-d573379e0">
        <let name="elmcount" value="count(hl7:addr|hl7:addr)"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="$elmcount&lt;=1">(CI-SISRecordTarget): choice (hl7:addr or hl7:addr) contains too many elements [max 1x]</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="count(hl7:name)&lt;=1">(CI-SISRecordTarget): element hl7:name appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place
Item: (CI-SISAddr)
-->

<!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr" id="d573399e68-false-d573415e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@hl7:use) or (string-length(@hl7:use)&gt;0 and not(matches(@hl7:use,'\s')))">(CI-SISAddr): Attribute @hl7:use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:country)&lt;=1">(CI-SISAddr): element hl7:country appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:state)&lt;=1">(CI-SISAddr): element hl7:state appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:city)&lt;=1">(CI-SISAddr): element hl7:city appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postalCode)&lt;=1">(CI-SISAddr): element hl7:postalCode appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumber)&lt;=1">(CI-SISAddr): element hl7:houseNumber appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:houseNumberNumeric)&lt;=1">(CI-SISAddr): element hl7:houseNumberNumeric appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetName)&lt;=1">(CI-SISAddr): element hl7:streetName appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetNameType)&lt;=1">(CI-SISAddr): element hl7:streetNameType appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:additionalLocator)&lt;=1">(CI-SISAddr): element hl7:additionalLocator appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:unitID)&lt;=1">(CI-SISAddr): element hl7:unitID appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:postBox)&lt;=1">(CI-SISAddr): element hl7:postBox appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:precinct)&lt;=1">(CI-SISAddr): element hl7:precinct appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:country
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:country" id="d573399e86-false-d573513e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:state
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:state" id="d573399e90-false-d573523e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:city
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:city" id="d573399e94-false-d573533e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postalCode
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postalCode" id="d573399e102-false-d573543e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumber
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumber" id="d573399e110-false-d573553e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumberNumeric
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:houseNumberNumeric" id="d573399e114-false-d573563e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetName
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetName" id="d573399e118-false-d573573e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetNameType
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetNameType" id="d573399e122-false-d573583e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:additionalLocator
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:additionalLocator" id="d573399e167-false-d573593e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:unitID
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:unitID" id="d573399e182-false-d573603e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postBox
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:postBox" id="d573399e192-false-d573613e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:precinct
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:precinct" id="d573399e196-false-d573623e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr" id="d573399e200-false-d573633e0">
        <extends rule="AD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='AD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:AD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="not(@use) or (string-length(@use)&gt;0 and not(matches(@use,'\s')))">(CI-SISAddr): Attribute @use SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="count(hl7:streetAddressLine)&lt;=1">(CI-SISAddr): element hl7:streetAddressLine appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d573399e218-false-d573689e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d573399e224-false-d573699e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d573399e228-false-d573709e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d573399e232-false-d573719e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d573399e236-false-d573729e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.18
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (CI-SISAddr)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:addr/hl7:streetAddressLine" id="d573399e240-false-d573739e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.18" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISAddr): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.1.1.10.10
Context: //hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:name
Item: (CI-SISRecordTarget)
-->
    <rule context="//hl7:ClinicalDocument[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.2']]/hl7:recordTarget/hl7:patientRole/hl7:patient[hl7:administrativeGenderCode[(@code='M' and @codeSystem='2.16.840.1.113883.5.1') or (@code='F' and @codeSystem='2.16.840.1.113883.5.1') or (@code='U' and @codeSystem='2.16.840.1.113883.5.1') or @nullFlavor]]/hl7:birthplace/hl7:place/hl7:name" id="d572152e205-false-d573749e0">
        <extends rule="EN"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.1.1.10.10" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='EN' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CI-SISRecordTarget): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:EN", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
</pattern>