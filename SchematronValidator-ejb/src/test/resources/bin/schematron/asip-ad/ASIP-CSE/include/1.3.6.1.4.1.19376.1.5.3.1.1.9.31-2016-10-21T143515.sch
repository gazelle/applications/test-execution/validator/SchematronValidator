<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Name: Abdomen Section
Description: Template CDA Section (prototype, directly derived from POCD_RM000040 MIF)
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.1.9.31-2016-10-21T143515">
    <title>Abdomen Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]
Item: (AbdomenSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]
Item: (AbdomenSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]" id="d506897e3297-false-d662789e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31'])&gt;=1">(AbdomenSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31'])&lt;=1">(AbdomenSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(AbdomenSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(AbdomenSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:code[(@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(AbdomenSection): element hl7:code[(@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:code[(@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(AbdomenSection): element hl7:code[(@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:text[not(@nullFlavor)])&gt;=1">(AbdomenSection): element hl7:text[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="count(hl7:text[not(@nullFlavor)])&lt;=1">(AbdomenSection): element hl7:text[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']
Item: (AbdomenSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']" id="d506897e3298-false-d662852e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(AbdomenSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.1.9.31')">(AbdomenSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.1.9.31'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:id[not(@nullFlavor)]
Item: (AbdomenSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:id[not(@nullFlavor)]" id="d506897e3300-false-d662866e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(AbdomenSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:code[(@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (AbdomenSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:code[(@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e3301-false-d662877e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(AbdomenSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="@nullFlavor or (@code='10191- 5' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='ABDOMEN' and @codeSystemName='LOINC')">(AbdomenSection): The element value SHALL be one of 'code '10191- 5' codeSystem '2.16.840.1.113883.6.1' displayName='ABDOMEN' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:text[not(@nullFlavor)]
Item: (AbdomenSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:text[not(@nullFlavor)]" id="d506897e3303-false-d662893e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(AbdomenSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.31']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.28']]]
Item: (AbdomenSection)
-->
</pattern>