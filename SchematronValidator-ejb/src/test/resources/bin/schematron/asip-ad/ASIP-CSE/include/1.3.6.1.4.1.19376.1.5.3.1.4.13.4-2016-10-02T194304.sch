<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Name: IHE Social History Observation
Description: A social history observation is a simple observation that uses a specific vocabulary, and inherits  constraints from CCD.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.4.13.4-2016-10-02T194304">
    <title>IHE Social History Observation</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]
Item: (IHESocialHistoryObservation)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]
Item: (IHESocialHistoryObservation)
-->
    <rule context="*[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]" id="d506897e4001-false-d668139e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="@classCode">(IHESocialHistoryObservation): attribute @classCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="not(@classCode) or (string-length(@classCode)&gt;0 and not(matches(@classCode,'\s')))">(IHESocialHistoryObservation): Attribute @classCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="string(@moodCode)=('EVN')">(IHESocialHistoryObservation): The value for @moodCode SHALL be 'EVN'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'])&gt;=1">(IHESocialHistoryObservation): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'])&lt;=1">(IHESocialHistoryObservation): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:templateId[@root='2.16.840.1.113883.10.20.1.33'])&gt;=1">(IHESocialHistoryObservation): element hl7:templateId[@root='2.16.840.1.113883.10.20.1.33'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:templateId[@root='2.16.840.1.113883.10.20.1.33'])&lt;=1">(IHESocialHistoryObservation): element hl7:templateId[@root='2.16.840.1.113883.10.20.1.33'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:code[not(@nullFlavor)])&gt;=1">(IHESocialHistoryObservation): element hl7:code[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:code[not(@nullFlavor)])&lt;=1">(IHESocialHistoryObservation): element hl7:code[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="count(hl7:repeatNumber)=0">(IHESocialHistoryObservation): element hl7:repeatNumber MAY NOT be present.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']
Item: (IHESocialHistoryObservation)
-->
    <rule context="*[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4']" id="d506897e4004-false-d668200e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHESocialHistoryObservation): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.4.13.4')">(IHESocialHistoryObservation): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.4.13.4'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']
Item: (IHESocialHistoryObservation)
-->
    <rule context="*[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']" id="d506897e4006-false-d668215e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHESocialHistoryObservation): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="string(@root)=('2.16.840.1.113883.10.20.1.33')">(IHESocialHistoryObservation): The value for @root SHALL be '2.16.840.1.113883.10.20.1.33'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:code[not(@nullFlavor)]
Item: (IHESocialHistoryObservation)
-->
    <rule context="*[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:code[not(@nullFlavor)]" id="d506897e4008-false-d668229e0">
        <extends rule="CD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHESocialHistoryObservation): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:repeatNumber
Item: (IHESocialHistoryObservation)
-->
    <rule context="*[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:repeatNumber" id="d506897e4009-false-d668239e0">
        <extends rule="IVL_INT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_INT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHESocialHistoryObservation): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_INT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.13.4
Context: *[hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]/hl7:observation[@moodCode='EVN'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]/hl7:value
Item: (IHESocialHistoryObservation)
-->
</pattern>