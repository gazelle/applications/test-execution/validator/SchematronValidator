<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Name: IHE Care Plan Section
Description: The care plan section shall contain a narrative description of the expectations for care
including proposals, goals, and order requests for monitoring, tracking, or improving the
condition of the patient. 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.3.31-2015-10-05T164639">
    <title>IHE Care Plan Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]
Item: (IHECarePlanSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]
Item: (IHECarePlanSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]" id="d506897e3610-false-d665239e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'])&gt;=1">(IHECarePlanSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'])&lt;=1">(IHECarePlanSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:templateId[@root='2.16.840.1.113883.10.20.1.10'])&gt;=1">(IHECarePlanSection): element hl7:templateId[@root='2.16.840.1.113883.10.20.1.10'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:templateId[@root='2.16.840.1.113883.10.20.1.10'])&lt;=1">(IHECarePlanSection): element hl7:templateId[@root='2.16.840.1.113883.10.20.1.10'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHECarePlanSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHECarePlanSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:code[(@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHECarePlanSection): element hl7:code[(@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:code[(@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHECarePlanSection): element hl7:code[(@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:text[not(@nullFlavor)])&gt;=1">(IHECarePlanSection): element hl7:text[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="count(hl7:text[not(@nullFlavor)])&lt;=1">(IHECarePlanSection): element hl7:text[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31']
Item: (IHECarePlanSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31']" id="d506897e3611-false-d665299e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECarePlanSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.3.31')">(IHECarePlanSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.3.31'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']
Item: (IHECarePlanSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']" id="d506897e3613-false-d665314e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECarePlanSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="string(@root)=('2.16.840.1.113883.10.20.1.10')">(IHECarePlanSection): The value for @root SHALL be '2.16.840.1.113883.10.20.1.10'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:id[not(@nullFlavor)]
Item: (IHECarePlanSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:id[not(@nullFlavor)]" id="d506897e3615-false-d665328e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECarePlanSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:code[(@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHECarePlanSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:code[(@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e3616-false-d665339e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECarePlanSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="@nullFlavor or (@code='61145-9' and @codeSystem='2.16.840.1.113883.6.1')">(IHECarePlanSection): The element value SHALL be one of 'code '61145-9' codeSystem '2.16.840.1.113883.6.1''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.31
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:text[not(@nullFlavor)]
Item: (IHECarePlanSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.31'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.10']]/hl7:text[not(@nullFlavor)]" id="d506897e3618-false-d665355e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.31" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECarePlanSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
</pattern>