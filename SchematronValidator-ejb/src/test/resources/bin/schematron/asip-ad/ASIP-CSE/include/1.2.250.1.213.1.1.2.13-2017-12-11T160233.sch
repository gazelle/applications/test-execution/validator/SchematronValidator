<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.250.1.213.1.1.2.13
Name: CSE Labor and Delivery
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.2.250.1.213.1.1.2.13-2017-12-11T160233">
    <title>CSE Labor and Delivery</title>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]" id="d506897e2365-false-d654730e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']])&gt;=1">(CSELaborAndDelivery): element hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']])&lt;=1">(CSELaborAndDelivery): element hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]" id="d506897e2372-false-d654780e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'])&gt;=1">(CSELaborAndDelivery): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'])&lt;=1">(CSELaborAndDelivery): element hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.2.13'])&gt;=1">(CSELaborAndDelivery): element hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:templateId[@root='1.2.250.1.213.1.1.2.13'])&lt;=1">(CSELaborAndDelivery): element hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'])&gt;=1">(CSELaborAndDelivery): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'])&lt;=1">(CSELaborAndDelivery): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1') or @nullFlavor])&gt;=1">(CSELaborAndDelivery): element hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1') or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1') or @nullFlavor])&lt;=1">(CSELaborAndDelivery): element hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1') or @nullFlavor] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:title)&lt;=1">(CSELaborAndDelivery): element hl7:title appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:text)&lt;=1">(CSELaborAndDelivery): element hl7:text appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]])&gt;=1">(CSELaborAndDelivery): element hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]])&lt;=1">(CSELaborAndDelivery): element hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10']
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10']" id="d506897e2373-false-d654876e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="string(@root)=('1.2.250.1.213.1.1.1.5.10.10')">(CSELaborAndDelivery): The value for @root SHALL be '1.2.250.1.213.1.1.1.5.10.10'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.2.250.1.213.1.1.2.13']
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.2.250.1.213.1.1.2.13']" id="d506897e2375-false-d654891e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="string(@root)=('1.2.250.1.213.1.1.2.13')">(CSELaborAndDelivery): The value for @root SHALL be '1.2.250.1.213.1.1.2.13'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']" id="d506897e2377-false-d654906e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3')">(CSELaborAndDelivery): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1') or @nullFlavor]
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:code[(@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1') or @nullFlavor]" id="d506897e2379-false-d654921e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="@nullFlavor or (@code='57074-7' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Labor and Delivery process' and @codeSystemName='LOINC')">(CSELaborAndDelivery): The element value SHALL be one of 'code '57074-7' codeSystem '2.16.840.1.113883.6.1' displayName='Labor and Delivery process' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:title
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:title" id="d506897e2381-false-d654937e0">
        <extends rule="ST"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ST' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ST", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:text
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:text" id="d506897e2382-false-d654947e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]]
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]]" id="d506897e2383-false-d654958e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="string(@typeCode)=('SBJ') or not(@typeCode)">(CSELaborAndDelivery): The value for @typeCode SHALL be 'SBJ'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]])&gt;=1">(CSELaborAndDelivery): element hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]])&lt;=1">(CSELaborAndDelivery): element hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]]/hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]]/hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]" id="d506897e2385-false-d654980e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor])&gt;=1">(CSELaborAndDelivery): element hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="count(hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor])&lt;=1">(CSELaborAndDelivery): element hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]]/hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]/hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]
Item: (CSELaborAndDelivery)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:subject[hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]]/hl7:relatedSubject[hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]]/hl7:code[(@code='MTH' and @codeSystem='2.16.840.1.113883.5.111') or @nullFlavor]" id="d506897e2386-false-d654998e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(CSELaborAndDelivery): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.2.250.1.213.1.1.2.13" test="@nullFlavor or (@code='MTH' and @codeSystem='2.16.840.1.113883.5.111' and @displayName='Mother' and @codeSystemName='roleCode')">(CSELaborAndDelivery): The element value SHALL be one of 'code 'MTH' codeSystem '2.16.840.1.113883.5.111' displayName='Mother' codeSystemName='roleCode''.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.2.250.1.213.1.1.2.13
Context: *[hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]]/hl7:section[hl7:templateId[@root='1.2.250.1.213.1.1.1.5.10.10'] and hl7:templateId[@root='1.2.250.1.213.1.1.2.13'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.3']]/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.7'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.21.2.9']]]
Item: (CSELaborAndDelivery)
-->
</pattern>