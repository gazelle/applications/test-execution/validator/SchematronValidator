<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Name: IHE Psychomotor Development Section
Description: This section describes a test battery in order to evaluate the psychomotricity of the newborn.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.7.3.1.1.13.4-2017-03-24T142547">
    <title>IHE Psychomotor Development Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]
Item: (IHEPsychomotorDevelopmentSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]
Item: (IHEPsychomotorDevelopmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]" id="d506897e4976-false-d674451e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4'])&gt;=1">(IHEPsychomotorDevelopmentSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4'])&lt;=1">(IHEPsychomotorDevelopmentSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHEPsychomotorDevelopmentSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHEPsychomotorDevelopmentSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:code[(@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEPsychomotorDevelopmentSection): element hl7:code[(@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:code[(@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEPsychomotorDevelopmentSection): element hl7:code[(@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:text)&gt;=1">(IHEPsychomotorDevelopmentSection): element hl7:text is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:text)&lt;=1">(IHEPsychomotorDevelopmentSection): element hl7:text appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="count(hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]])&gt;=1">(IHEPsychomotorDevelopmentSection): element hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]] is mandatory [min 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']
Item: (IHEPsychomotorDevelopmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']" id="d506897e4977-false-d674510e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPsychomotorDevelopmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="string(@root)=('1.3.6.1.4.1.19376.1.7.3.1.1.13.4')">(IHEPsychomotorDevelopmentSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.7.3.1.1.13.4'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:id[not(@nullFlavor)]
Item: (IHEPsychomotorDevelopmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:id[not(@nullFlavor)]" id="d506897e4979-false-d674524e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPsychomotorDevelopmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:code[(@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEPsychomotorDevelopmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:code[(@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e4980-false-d674535e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPsychomotorDevelopmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="@nullFlavor or (@code='xx-MCH-PsychoMDev' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Psychomotor development' and @codeSystemName='LOINC')">(IHEPsychomotorDevelopmentSection): The element value SHALL be one of 'code 'xx-MCH-PsychoMDev' codeSystem '2.16.840.1.113883.6.1' displayName='Psychomotor development' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:text
Item: (IHEPsychomotorDevelopmentSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:text" id="d506897e4982-false-d674551e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.7.3.1.1.13.4" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPsychomotorDevelopmentSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.7.3.1.1.13.4
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.7.3.1.1.13.4']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13']]]
Item: (IHEPsychomotorDevelopmentSection)
-->
</pattern>