<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Name: IHE Coded Social History Section
Description: The social history section shall contain a narrative description of the person’s beliefs, home  life, community life, work life, hobbies, and risky habits. It shall include Social History  Observations.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.3.16.1-2015-10-05T154920">
    <title>IHE Coded Social History Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]
Item: (IHECodedSocialHistorySection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]" id="d506897e3509-false-d664250e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'])&gt;=1">(IHECodedSocialHistorySection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'])&lt;=1">(IHECodedSocialHistorySection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'])&gt;=1">(IHECodedSocialHistorySection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'])&lt;=1">(IHECodedSocialHistorySection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:templateId[@root='2.16.840.1.113883.10.20.1.15'])&gt;=1">(IHECodedSocialHistorySection): element hl7:templateId[@root='2.16.840.1.113883.10.20.1.15'] is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:templateId[@root='2.16.840.1.113883.10.20.1.15'])&lt;=1">(IHECodedSocialHistorySection): element hl7:templateId[@root='2.16.840.1.113883.10.20.1.15'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:code[(@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHECodedSocialHistorySection): element hl7:code[(@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:code[(@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHECodedSocialHistorySection): element hl7:code[(@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:text[not(@nullFlavor)])&gt;=1">(IHECodedSocialHistorySection): element hl7:text[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:text[not(@nullFlavor)])&lt;=1">(IHECodedSocialHistorySection): element hl7:text[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]])&gt;=1">(IHECodedSocialHistorySection): element hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="count(hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.37']]])&lt;=1">(IHECodedSocialHistorySection): element hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.37']]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1']" id="d506897e3510-false-d664340e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedSocialHistorySection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.3.16.1')">(IHECodedSocialHistorySection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.3.16.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16']
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16']" id="d506897e3512-false-d664355e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedSocialHistorySection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.3.16')">(IHECodedSocialHistorySection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.3.16'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']" id="d506897e3514-false-d664370e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedSocialHistorySection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="string(@root)=('2.16.840.1.113883.10.20.1.15')">(IHECodedSocialHistorySection): The value for @root SHALL be '2.16.840.1.113883.10.20.1.15'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:code[(@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:code[(@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e3516-false-d664385e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedSocialHistorySection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="@nullFlavor or (@code='29762-2' and @codeSystem='2.16.840.1.113883.6.1')">(IHECodedSocialHistorySection): The element value SHALL be one of 'code '29762-2' codeSystem '2.16.840.1.113883.6.1''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:text[not(@nullFlavor)]
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:text[not(@nullFlavor)]" id="d506897e3518-false-d664401e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHECodedSocialHistorySection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]
Item: (IHECodedSocialHistorySection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.13.4'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.33']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="@typeCode">(IHECodedSocialHistorySection): attribute @typeCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.3.16.1" test="not(@typeCode) or (string-length(@typeCode)&gt;0 and not(matches(@typeCode,'\s')))">(IHECodedSocialHistorySection): Attribute @typeCode SHALL be of data type 'cs'</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.3.16.1
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16.1'] and hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.16'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.15']]/hl7:component[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.3.37']]]
Item: (IHECodedSocialHistorySection)
-->
</pattern>