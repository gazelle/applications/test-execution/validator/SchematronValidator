<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Name: IHE Patient Transfer
Description: The Patient Transfer entry shall record the transfer of the patient to an internal department or external entity such as a different hospital or skilled nursing facility.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1-2017-03-21T124349">
    <title>IHE Patient Transfer</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]
Item: (IHEPatientTransfer)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]" id="d506897e2660-false-d657293e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="string(@classCode)=('ACT')">(IHEPatientTransfer): The value for @classCode SHALL be 'ACT'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="@moodCode">(IHEPatientTransfer): attribute @moodCode SHALL be present.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="not(@moodCode) or (string-length(@moodCode)&gt;0 and not(matches(@moodCode,'\s')))">(IHEPatientTransfer): Attribute @moodCode SHALL be of data type 'cs'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="@moodCode='INT' or @moodCode='EVN'">(IHEPatientTransfer): moodCode attribute SHALL have the value INT or EVN </assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="hl7:statusCode[@code='normal' or @code='completed']">(IHEPatientTransfer): statusCode SHALL have the value normal or completed</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(@moodCode='INT' and @code='normal')  or (@moodCode='EVN' and @code='completed')">(IHEPatientTransfer): When the transfer act has occurred (moodCode='EVN'), the statusCode element shall be present, and shall contain the value 'completed'. When the transfer act is intended (moodCode='INT') the statusCode element shall contain the value 'normal'</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="@moodCode='INT' or hl7:effectiveTime">(IHEPatientTransfer): When the transfer has occurred, effectiveTime element shall be present</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="@moodCode='INT' or not(//hl7:participantRole) or //hl7:participantRole/hl7:id">(IHEPatientTransfer): The &lt;id&gt; element shall be sent when the transfer has occurred</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1'])&gt;=1">(IHEPatientTransfer): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1'])&lt;=1">(IHEPatientTransfer): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:id)&gt;=1">(IHEPatientTransfer): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:code[(@code='107724000' and @codeSystem='2.16.840.1.113883.6.96')])&gt;=1">(IHEPatientTransfer): element hl7:code[(@code='107724000' and @codeSystem='2.16.840.1.113883.6.96')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:code[(@code='107724000' and @codeSystem='2.16.840.1.113883.6.96')])&lt;=1">(IHEPatientTransfer): element hl7:code[(@code='107724000' and @codeSystem='2.16.840.1.113883.6.96')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:text)&lt;=1">(IHEPatientTransfer): element hl7:text appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:statusCode[not(@nullFlavor)])&gt;=1">(IHEPatientTransfer): element hl7:statusCode[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:statusCode[not(@nullFlavor)])&lt;=1">(IHEPatientTransfer): element hl7:statusCode[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:effectiveTime)&lt;=1">(IHEPatientTransfer): element hl7:effectiveTime appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']" id="d506897e2663-false-d657368e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1')">(IHEPatientTransfer): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:id
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:id" id="d506897e2665-false-d657382e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:code[(@code='107724000' and @codeSystem='2.16.840.1.113883.6.96')]
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:code[(@code='107724000' and @codeSystem='2.16.840.1.113883.6.96')]" id="d506897e2666-false-d657393e0">
        <extends rule="CD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="@nullFlavor or (@code='107724000' and @codeSystem='2.16.840.1.113883.6.96' and @displayName='patient transfer')">(IHEPatientTransfer): The element value SHALL be one of 'code '107724000' codeSystem '2.16.840.1.113883.6.96' displayName='patient transfer''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:text
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:text" id="d506897e2668-false-d657409e0">
        <extends rule="ED"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='ED' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:ED", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:reference[not(@nullFlavor)])&gt;=1">(IHEPatientTransfer): element hl7:reference[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:reference[not(@nullFlavor)])&lt;=1">(IHEPatientTransfer): element hl7:reference[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:text/hl7:reference[not(@nullFlavor)]" id="d506897e2669-false-d657429e0">
        <extends rule="TEL"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='TEL' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:TEL", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:statusCode[not(@nullFlavor)]
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:statusCode[not(@nullFlavor)]" id="d506897e2670-false-d657439e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:effectiveTime
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:effectiveTime" id="d506897e2671-false-d657449e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:high)&lt;=1">(IHEPatientTransfer): element hl7:high appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:low)&lt;=1">(IHEPatientTransfer): element hl7:low appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:effectiveTime/hl7:high
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:effectiveTime/hl7:high" id="d506897e2672-false-d657473e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:effectiveTime/hl7:low
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:effectiveTime/hl7:low" id="d506897e2673-false-d657483e0">
        <extends rule="IVXB_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVXB_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEPatientTransfer): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVXB_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']" id="d506897e2674-false-d657493e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="string(@typeCode)=('DST')">(IHEPatientTransfer): The value for @typeCode SHALL be 'DST'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:participantRole)&lt;=1">(IHEPatientTransfer): element hl7:participantRole appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:templateId
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:templateId" id="d506897e2676-false-d657514e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="not(@root) or (string-length(@root)&gt;0 and not(matches(@root,'\s')))">(IHEPatientTransfer): Attribute @root SHALL be of data type 'cs'</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole" id="d506897e2678-false-d657524e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="string(@classCode)=('SDLOC') or not(@classCode)">(IHEPatientTransfer): The value for @classCode SHALL be 'SDLOC'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:code)&gt;=1">(IHEPatientTransfer): element hl7:code is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:code)&lt;=1">(IHEPatientTransfer): element hl7:code appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:playingEntity)&lt;=1">(IHEPatientTransfer): element hl7:playingEntity appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:id
Item: (IHEPatientTransfer)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:code
Item: (IHEPatientTransfer)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:addr
Item: (IHEPatientTransfer)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:telecom
Item: (IHEPatientTransfer)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:playingEntity
Item: (IHEPatientTransfer)
-->
    <rule context="*[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:playingEntity" id="d506897e2684-false-d657591e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="string(@classCode)=('ENT') or not(@classCode)">(IHEPatientTransfer): The value for @classCode SHALL be 'ENT'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1" test="count(hl7:name)&gt;=1">(IHEPatientTransfer): element hl7:name is required [min 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1
Context: *[hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]]/hl7:act[@classCode='ACT'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.25.1.4.1']]/hl7:participant[@typeCode='DST']/hl7:participantRole/hl7:playingEntity/hl7:name
Item: (IHEPatientTransfer)
-->
</pattern>