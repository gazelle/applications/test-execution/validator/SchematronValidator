<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Name: IHE Chest Wall Section
Description: The chest wall section shall contain a description of any type of chest wall exam.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.1.9.27-2017-03-16T141817">
    <title>IHE Chest Wall Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]
Item: (IHEChestWallSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]
Item: (IHEChestWallSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]" id="d506897e3200-false-d662062e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27'])&gt;=1">(IHEChestWallSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27'])&lt;=1">(IHEChestWallSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHEChestWallSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHEChestWallSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:code[(@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEChestWallSection): element hl7:code[(@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:code[(@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEChestWallSection): element hl7:code[(@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:text[not(@nullFlavor)])&gt;=1">(IHEChestWallSection): element hl7:text[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="count(hl7:text[not(@nullFlavor)])&lt;=1">(IHEChestWallSection): element hl7:text[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']
Item: (IHEChestWallSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']" id="d506897e3201-false-d662125e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChestWallSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.1.9.27')">(IHEChestWallSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.1.9.27'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:id[not(@nullFlavor)]
Item: (IHEChestWallSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:id[not(@nullFlavor)]" id="d506897e3203-false-d662139e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChestWallSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:code[(@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEChestWallSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:code[(@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e3204-false-d662150e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChestWallSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="@nullFlavor or (@code='11392-8' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='CHEST WALL' and @codeSystemName='LOINC')">(IHEChestWallSection): The element value SHALL be one of 'code '11392-8' codeSystem '2.16.840.1.113883.6.1' displayName='CHEST WALL' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:text[not(@nullFlavor)]
Item: (IHEChestWallSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:text[not(@nullFlavor)]" id="d506897e3206-false-d662166e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.27" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEChestWallSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.27
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.27']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.28']]]
Item: (IHEChestWallSection)
-->
</pattern>