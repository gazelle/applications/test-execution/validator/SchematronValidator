<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Name: IHE Occupational Data For Health Organizer Entry
Description: This organizer holds information about a person’s occupation. It organizes the employment  status, usual occupation and usual industry along with durations, and history of occupation  information (which includes occupation and employer with industry, and work hours and  workshift) into a standard structure.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.4.20-2016-10-02T180312">
    <title>IHE Occupational Data For Health Organizer Entry</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]" id="d506897e4214-false-d669563e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="string(@classCode)=('CLUSTER')">(IHEOccupationalDataForHealthOrganizerEntry): The value for @classCode SHALL be 'CLUSTER'.</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20'])&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20'])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:id)&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:id is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:code[(@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:code[(@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:code[(@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:code[(@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:statusCode[(@code='completed' and @codeSystem='2.16.840.1.113883.5.14')])&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:statusCode[(@code='completed' and @codeSystem='2.16.840.1.113883.5.14')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:statusCode[(@code='completed' and @codeSystem='2.16.840.1.113883.5.14')])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:statusCode[(@code='completed' and @codeSystem='2.16.840.1.113883.5.14')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:effectiveTime)&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:effectiveTime is required [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:effectiveTime)&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:effectiveTime appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.1']]])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.1']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.3']]])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.3']]] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']" id="d506897e4216-false-d669670e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationalDataForHealthOrganizerEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.4.20')">(IHEOccupationalDataForHealthOrganizerEntry): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.4.20'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:id
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:id" id="d506897e4218-false-d669684e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationalDataForHealthOrganizerEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:code[(@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:code[(@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e4219-false-d669695e0">
        <extends rule="CD"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CD' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationalDataForHealthOrganizerEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CD", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="@nullFlavor or (@code='LOINC-1' and @codeSystem='2.16.840.1.113883.6.1')">(IHEOccupationalDataForHealthOrganizerEntry): The element value SHALL be one of 'code 'LOINC-1' codeSystem '2.16.840.1.113883.6.1''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:statusCode[(@code='completed' and @codeSystem='2.16.840.1.113883.5.14')]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:statusCode[(@code='completed' and @codeSystem='2.16.840.1.113883.5.14')]" id="d506897e4221-false-d669712e0">
        <extends rule="CS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationalDataForHealthOrganizerEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="@nullFlavor or (@code='completed' and @codeSystem='2.16.840.1.113883.5.14')">(IHEOccupationalDataForHealthOrganizerEntry): The element value SHALL be one of 'code 'completed' codeSystem '2.16.840.1.113883.5.14''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:effectiveTime
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:effectiveTime" id="d506897e4223-false-d669728e0">
        <extends rule="IVL_TS"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='IVL_TS' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEOccupationalDataForHealthOrganizerEntry): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:IVL_TS", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.1']]]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.1']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:sequenceNumber[not(@nullFlavor)])&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:sequenceNumber[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:sequenceNumber[not(@nullFlavor)])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:sequenceNumber[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.1']]]/hl7:sequenceNumber[not(@nullFlavor)]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.1']]]/hl7:sequenceNumber[not(@nullFlavor)]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="string(@value)=('1')">(IHEOccupationalDataForHealthOrganizerEntry): The value for @value SHALL be '1'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:sequenceNumber[not(@nullFlavor)])&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:sequenceNumber[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:sequenceNumber[not(@nullFlavor)])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:sequenceNumber[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:sequenceNumber[not(@nullFlavor)]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.2']]]/hl7:sequenceNumber[not(@nullFlavor)]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="string(@value)=('2') or not(@value)">(IHEOccupationalDataForHealthOrganizerEntry): The value for @value SHALL be '2'.</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.3']]]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.3']]]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:sequenceNumber[not(@nullFlavor)])&gt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:sequenceNumber[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="count(hl7:sequenceNumber[not(@nullFlavor)])&lt;=1">(IHEOccupationalDataForHealthOrganizerEntry): element hl7:sequenceNumber[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.4.20
Context: *[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.3']]]/hl7:sequenceNumber[not(@nullFlavor)]
Item: (IHEOccupationalDataForHealthOrganizerEntry)
-->
    <rule context="*[hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]]/hl7:organizer[@classCode='CLUSTER'][hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20']]/hl7:component[hl7:organizer[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.20.3']]]/hl7:sequenceNumber[not(@nullFlavor)]">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.4.20" test="string(@value)=('3') or not(@value)">(IHEOccupationalDataForHealthOrganizerEntry): The value for @value SHALL be '3'.</assert>
    </rule>
</pattern>