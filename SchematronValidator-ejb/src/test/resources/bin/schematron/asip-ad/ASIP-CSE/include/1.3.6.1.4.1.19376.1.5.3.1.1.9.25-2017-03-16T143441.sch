<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Name: IHE Endocrine System Section
Description: The endocrine system section shall contain a description of any type of endocrine system exam.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron" id="template-1.3.6.1.4.1.19376.1.5.3.1.1.9.25-2017-03-16T143441">
    <title>IHE Endocrine System Section</title>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]
Item: (IHEEndocrineSystemSection)
-->

<!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]
Item: (IHEEndocrineSystemSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]" id="d506897e3146-false-d661702e0">
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25'])&gt;=1">(IHEEndocrineSystemSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25'] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25'])&lt;=1">(IHEEndocrineSystemSection): element hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25'] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:id[not(@nullFlavor)])&gt;=1">(IHEEndocrineSystemSection): element hl7:id[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:id[not(@nullFlavor)])&lt;=1">(IHEEndocrineSystemSection): element hl7:id[not(@nullFlavor)] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:code[(@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1')])&gt;=1">(IHEEndocrineSystemSection): element hl7:code[(@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1')] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:code[(@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1')])&lt;=1">(IHEEndocrineSystemSection): element hl7:code[(@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1')] appears too often [max 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:text[not(@nullFlavor)])&gt;=1">(IHEEndocrineSystemSection): element hl7:text[not(@nullFlavor)] is mandatory [min 1x].</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="count(hl7:text[not(@nullFlavor)])&lt;=1">(IHEEndocrineSystemSection): element hl7:text[not(@nullFlavor)] appears too often [max 1x].</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']
Item: (IHEEndocrineSystemSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']" id="d506897e3147-false-d661765e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEndocrineSystemSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="string(@root)=('1.3.6.1.4.1.19376.1.5.3.1.1.9.25')">(IHEEndocrineSystemSection): The value for @root SHALL be '1.3.6.1.4.1.19376.1.5.3.1.1.9.25'.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:id[not(@nullFlavor)]
Item: (IHEEndocrineSystemSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:id[not(@nullFlavor)]" id="d506897e3149-false-d661779e0">
        <extends rule="II"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='II' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEndocrineSystemSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:II", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:code[(@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1')]
Item: (IHEEndocrineSystemSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:code[(@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1')]" id="d506897e3150-false-d661790e0">
        <extends rule="CE"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='CE' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEndocrineSystemSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:CE", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="@nullFlavor or (@code='29307-6' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='ENDOCRINE SYSTEM' and @codeSystemName='LOINC')">(IHEEndocrineSystemSection): The element value SHALL be one of 'code '29307-6' codeSystem '2.16.840.1.113883.6.1' displayName='ENDOCRINE SYSTEM' codeSystemName='LOINC''.</assert>
    </rule>
   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:text[not(@nullFlavor)]
Item: (IHEEndocrineSystemSection)
-->
    <rule context="*[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:text[not(@nullFlavor)]" id="d506897e3152-false-d661806e0">
        <extends rule="SD.TEXT"/>
        <assert role="error" see="https://poc-art-decor.kereval.com/art-decor/decor-templates--ASIP-CSE-?id=1.3.6.1.4.1.19376.1.5.3.1.1.9.25" test="(local-name-from-QName(resolve-QName(@xsi:type,.))='SD.TEXT' and namespace-uri-from-QName(resolve-QName(@xsi:type,.))='urn:hl7-org:v3') or not(@xsi:type)">(IHEEndocrineSystemSection): If an @xsi:type instruction is present it SHALL be valued "{urn:hl7-org:v3}:SD.TEXT", found "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
    </rule>

   <!--
Template derived rules for ID: 1.3.6.1.4.1.19376.1.5.3.1.1.9.25
Context: *[hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]]/hl7:section[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.1.9.25']]/hl7:entry[hl7:observation[hl7:templateId[@root='1.3.6.1.4.1.19376.1.5.3.1.4.5'] and hl7:templateId[@root='2.16.840.1.113883.10.20.1.28']]]
Item: (IHEEndocrineSystemSection)
-->
</pattern>