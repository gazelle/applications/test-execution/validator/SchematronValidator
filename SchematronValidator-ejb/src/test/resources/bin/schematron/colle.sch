<?xml version="1.0" encoding="UTF-8"?><sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:sqf="http://www.schematron-quickfix.com/validator/process" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" queryBinding="xslt3">
    <sch:ns prefix="ip" uri="http://invioprescrittorichiesta.xsd.dem.sanita.finanze.it"/>
    <sch:ns prefix="td" uri="http://tipodati.xsd.dem.sanita.finanze.it"/>

    <sch:pattern>
        <sch:let name="data" value="document('PrescrittoOID.xml')"/>

        <sch:let name="valori_code" value="$data/Codes/CodeType[@name = 'AllergeniType']"/>


        

        
        <sch:rule context="ip:InvioPrescrittoRichiesta/ip:ElencoDettagliPrescrizioni/td:DettaglioPrescrizione/td:descrTestoLiberoNote">

            <sch:assert test="                     (normalize-space(@type) != 'coded' and count(td:code) = 0 and count(td:effectiveTimeRVE) = 0 and count(td:text) = 0)                     or (normalize-space(@type) = 'coded' and count(td:code) &gt;= 0 and count(td:effectiveTimeRVE) &gt;= 1 and count(td:text) = 1)"><sch:value-of select="normalize-space(@type)"/>,<sch:value-of select="count(td:code)"/>,<sch:value-of select="count(td:effectiveTimeRVE)"/>,<sch:value-of select="count(td:text)"/> I campo <sch:name/> non deve
                contenere figli se non continene note strutturate</sch:assert>
        </sch:rule>

        
        <sch:rule context="ip:InvioPrescrittoRichiesta/ip:ElencoDettagliPrescrizioni/td:DettaglioPrescrizione/td:descrTestoLiberoNote/td:code">
            
            <sch:let name="local_code" value="@code"/>
            <sch:let name="local_codeSystem" value="@codeSystem"/>
            <sch:let name="local_display" value="@displayName"/>
            <sch:let name="local_classSchemeAll" value="$valori_code/Code[@code = $local_code]/@classScheme"/>


            

            <xsl:variable name="testCoerenza" as="element()*">
                <xsl:for-each select="parent::td:descrTestoLiberoNote/td:code/@code">
                    <xsl:variable name="testCoerenza">
                        <xsl:value-of select="normalize-space(.)"/>
                    </xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$valori_code/Code[@code = $testCoerenza]/@classScheme = $local_classSchemeAll">
                            <Item>true</Item>
                        </xsl:when>
                        <xsl:otherwise>
                            <Item>false</Item>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:variable>

            <sch:assert test="count($testCoerenza[contains(text(), 'false')]) = 0">Il campo
                <sch:name/> deve contenere dei valori coerenti con gli altri
                <sch:name/></sch:assert>

            <sch:assert test="                     ($local_code = $valori_code/Code/@code and $valori_code/Code[@code = $local_code]/@codingScheme = $local_codeSystem                     and ((count($valori_code/Code[@code = $local_code]/@display) = 1 and $valori_code/Code[@code = $local_code]/@display = $local_display)                     or (count($valori_code/Code[@code = $local_code]/@display) = 0)))">Il campo <sch:name/> deve contenere dei valori corretti e coerenti</sch:assert>


            <sch:assert test="                     (normalize-space(@type) != 'coded' and count(td:code) = 0 and count(td:effectiveTimeRVE) = 0 and count(td:text) = 0)                     or (normalize-space(@type) = 'coded' and count(td:code) &gt; 0 and count(td:effectiveTimeRVE) = 1 and count(td:text) = 1)"><sch:value-of select="normalize-space(@type)"/>,<sch:value-of select="count(td:code)"/>,<sch:value-of select="count(td:effectiveTimeRVE)"/>,<sch:value-of select="count(td:text)"/> I campo <sch:name/> non deve
                contenere figli se non continene note strutturate</sch:assert>
        </sch:rule>
        





        
        <sch:rule context="ip:InvioPrescrittoRichiesta/ip:ElencoDettagliPrescrizioni/td:DettaglioPrescrizione/td:descrTestoLiberoNote/td:effectiveTimeRVE">
            <sch:assert test="                     count(td:low) = 1 and count(td:high) = 1">Il
                campo <sch:name/> deve contenere due figli low e high</sch:assert>
        </sch:rule>

        
        <sch:rule context="ip:InvioPrescrittoRichiesta/ip:ElencoDettagliPrescrizioni/td:DettaglioPrescrizione/td:descrTestoLiberoNote/td:effectiveTimeRVE/td:low">
            <sch:assert test="                     count(@unit) = 1 and @unit = 'g' and number(@value) &gt;= 0"><sch:value-of select="number(@value)"/> Il campo <sch:name/> deve contenere gli
                attributi unit valorizzato a g e value con un intero maggiore o uguale a
                0</sch:assert>
        </sch:rule>


        
        
        <sch:rule context="ip:InvioPrescrittoRichiesta/ip:ElencoDettagliPrescrizioni/td:DettaglioPrescrizione/td:descrTestoLiberoNote/td:effectiveTimeRVE/td:high">
            <sch:assert test="                     count(@unit) = 1 and @unit = 'g' and number(@value) &gt;= 0"><sch:value-of select="number(@value)"/> Il campo <sch:name/> deve contenere gli
                attributi unit valorizzato a g e value con un intero maggiore o uguale a
                0</sch:assert>
        </sch:rule>


        
        

        
        <sch:rule context="ip:InvioPrescrittoRichiesta/ip:ElencoDettagliPrescrizioni/td:DettaglioPrescrizione/td:descrTestoLiberoNote/td:text">
            <xsl:variable name="offendingLines">
                <xsl:for-each select="parent::td:descrTestoLiberoNote/td:code">
                    <xsl:choose>
                        <xsl:when test="position() &gt; 1">
                            <xsl:value-of select="concat(' - ', @codeSystemName, ':', @code)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="concat(@codeSystemName, ':', @code)"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
                
                <xsl:choose>
                    <xsl:when test="count(parent::td:descrTestoLiberoNote/td:code) != 0">
                        <xsl:value-of select="'. '"/>
                    </xsl:when>

                </xsl:choose>
                <xsl:value-of select="                         concat('controllo da eseguire da ',                         parent::td:descrTestoLiberoNote/td:effectiveTimeRVE/td:low/@value,                         parent::td:descrTestoLiberoNote/td:effectiveTimeRVE/td:low/@unit,                         ' a ',                         parent::td:descrTestoLiberoNote/td:effectiveTimeRVE/td:high/@value,                         parent::td:descrTestoLiberoNote/td:effectiveTimeRVE/td:high/@unit,                         '.')"/>
            </xsl:variable>
            <sch:assert test="starts-with(normalize-space(text()), $offendingLines)"> I campo
                <sch:name/> non è valorizzato correttamente. Valore corretto '<sch:value-of select="$offendingLines"/>' </sch:assert>
        </sch:rule>
        
        
    </sch:pattern>
</sch:schema>