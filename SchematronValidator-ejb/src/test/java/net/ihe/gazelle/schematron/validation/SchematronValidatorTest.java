package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.schematron.compiler.SchematronCompiler;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.SchFilePath;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.bind.ValidationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class SchematronValidatorTest {

    private String resourcesDirectory;

    private Schematron schematron;

    @Mock
    private SchFilePath schFilePath;

    @Mock
    private SchematronCompiler schematronCompiler;

    @InjectMocks
    private SchematronValidator schematronValidator;

    @Before
    public void setUp() throws Exception {
        resourcesDirectory = new String("src/test/resources/");
        schematron = new Schematron();

    }

    @Test
    public void validateFileByCompiledSchematronTest() throws JDOMException, IOException, TransformerException {
        SAXBuilder sxb = new SAXBuilder();
        Document fileD = null;

        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");
        fileD = sxb.build(cdaFile);

        File compiledSchematron = new File(resourcesDirectory + "bin/compilations/1.xsl");

        Document result = schematronValidator.validateFileByCompiledSchematron(sxb, fileD, compiledSchematron);

        assertTrue(!result.getContent().isEmpty());
    }

    @Test
    public void validateDocumentPart1Test() throws JDOMException, ValidationException, TransformerException, IOException {
        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");
        String isoexpandpath = "../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_abstract_expand.xsl";
        String isopath = "../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_svrl_for_xslt2.xsl";

        Mockito.when(schFilePath.getSchCompiledPath(schematron)).thenReturn(resourcesDirectory + "bin/compilations/1.xsl");
        Mockito.when(schFilePath.getSchPath(schematron)).thenReturn(resourcesDirectory + "bin/schematron/ASIP-CSE-CarnetSante.sch");

        Document result = schematronValidator.validateDocument(schematron, cdaFile, isoexpandpath, isopath);

        assertTrue(result != null);
        assertTrue(!result.getContent().isEmpty());

    }

    @Test
    public void validateDocumentPart2Test() throws JDOMException, ValidationException, TransformerException, IOException {
        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");

        Mockito.when(schFilePath.getIsoExpandPath()).thenReturn("../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_abstract_expand.xsl");
        Mockito.when(schFilePath.getIsoPath()).thenReturn("../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_svrl_for_xslt2.xsl");
        Mockito.when(schFilePath.getSchCompiledPath(schematron)).thenReturn(resourcesDirectory + "bin/compilations/1.xsl");
        Mockito.when(schFilePath.getSchPath(schematron)).thenReturn(resourcesDirectory + "bin/schematron/ASIP-CSE-CarnetSante.sch");

        Document result = schematronValidator.validateDocument(schematron, cdaFile);

        assertTrue(result != null);
        assertTrue(!result.getContent().isEmpty());

    }

    @Test
    public void getValidationResultValidTest() throws JDOMException, IOException, TransformerException, ValidationException {
        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");
        schematron.setTransformUnknowns(false);


        Mockito.when(schFilePath.getIsoExpandPath()).thenReturn("../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_abstract_expand.xsl");
        Mockito.when(schFilePath.getIsoPath()).thenReturn("../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_svrl_for_xslt2.xsl");
        Mockito.when(schFilePath.getSchCompiledPath(schematron)).thenReturn(resourcesDirectory + "bin/compilations/1.xsl");
        Mockito.when(schFilePath.getSchPath(schematron)).thenReturn(resourcesDirectory + "bin/schematron/ASIP-CSE-CarnetSante.sch");

        List<net.ihe.gazelle.validation.Notification> result = schematronValidator.getValidationResult(schematron, cdaFile);

        assertTrue(!result.isEmpty());
        assertTrue(result != null);
    }

    @Test
    public void validateFileBySchematronValidTest() throws JDOMException, IOException, TransformerException, ValidationException {
        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");
        schematron.setTransformUnknowns(false);

        Mockito.when(schFilePath.getIsoExpandPath()).thenReturn("../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_abstract_expand.xsl");
        Mockito.when(schFilePath.getIsoPath()).thenReturn("../SchematronValidator-ear/src/main/dist/opt/SchematronValidator/bin/schematron/schematron/iso_svrl_for_xslt2.xsl");
        Mockito.when(schFilePath.getSchCompiledPath(schematron)).thenReturn(resourcesDirectory + "bin/compilations/1.xsl");
        Mockito.when(schFilePath.getSchPath(schematron)).thenReturn(resourcesDirectory + "bin/schematron/ASIP-CSE-CarnetSante.sch");

        List<net.ihe.gazelle.validation.Notification> result = schematronValidator.validateFileBySchematron(schematron, cdaFile);

        assertTrue(!result.isEmpty());
        assertTrue(result != null);

    }

    @Test
    public void validateFileBySchematronCDAFileNullTest() throws JDOMException, IOException, TransformerException, ValidationException {

        List<net.ihe.gazelle.validation.Notification> result = schematronValidator.validateFileBySchematron(schematron, null);

        assertTrue(result.isEmpty());
        assertTrue(result != null);

    }

    @Test
    public void validateFileBySchematronSchematronNullTest() throws JDOMException, IOException, TransformerException, ValidationException {
        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");
        List<net.ihe.gazelle.validation.Notification> result = schematronValidator.validateFileBySchematron(null, cdaFile);

        assertTrue(result.isEmpty());
        assertTrue(result != null);

    }
}