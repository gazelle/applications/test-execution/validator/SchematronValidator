package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.schematron.model.ObjectType;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.transformation.DfdlTransformation;
import net.ihe.gazelle.schematron.util.DetailedResultMarshaller;
import net.ihe.gazelle.schematron.util.SchFilePath;
import net.ihe.gazelle.transformation.service.CompilationException;
import net.ihe.gazelle.transformation.service.ObjectNotFoundException;
import net.ihe.gazelle.transformation.service.TransformationException;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import org.jdom.JDOMException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.ValidationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;

@RunWith(MockitoJUnitRunner.class)
public class ValidationProcessTest {

    private String resourcesDirectory;

    private Schematron schematron;

    @Mock
    private SchFilePath schFilePath;

    @Mock
    private SchematronValidator schematronValidator;

    @Mock
    private XMLValidation xmlValidation;

    @Mock
    private DetailedResultMarshaller marshaller;

    @Mock
    private DfdlTransformation dfdlTransformation;

    @InjectMocks
    private ValidationProcess validationProcess;

    @Before
    public void setUp() throws Exception {
        resourcesDirectory = new String("src/test/resources/");
        schematron = new Schematron();

    }


    @Test
    public void getFileToValidateValidTest() throws IOException {
        File cdaFile = new File(resourcesDirectory + "FilesToValidate/cda_asip.xml");
        String cda = cdaFile.toString();
        File result = validationProcess.getFileToValidate(cda);

        assertTrue(result != null);
        assertTrue(result.canRead());
    }

    @Test
    public void validateXMLForEpSOSTest() {
        ObjectType objectType = new ObjectType();
        objectType.setKeyword("CDA");
        schematron.setObjectType(objectType);
        schematron.setName("epSOS");
        schematron.setXsdPath("/path");

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File fileToValidate = new File(filePath);

        DetailedResult res = new DetailedResult();

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("PASSED");

        Mockito.when(xmlValidation.isXMLWellFormed(fileToValidate)).thenReturn(documentWellFormed);
        Mockito.when(xmlValidation.isCDAValidForEpsos(fileToValidate)).thenReturn(documentValidXSD);

        validationProcess.validateXML(res, fileToValidate, schematron);

        assertTrue(res != null);
        assertTrue(res.getDocumentValidXSD().getResult().equals("PASSED"));
    }

    @Test
    public void validateXMLForEpSOSInvalidTest() {
        ObjectType objectType = new ObjectType();
        objectType.setKeyword("CDA");
        schematron.setObjectType(objectType);
        schematron.setName("epSOS");
        schematron.setXsdPath("/path");

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File fileToValidate = new File(filePath);

        DetailedResult res = new DetailedResult();

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("FAILED");

        Mockito.when(xmlValidation.isXMLWellFormed(fileToValidate)).thenReturn(documentWellFormed);
        Mockito.when(xmlValidation.isCDAValidForEpsos(fileToValidate)).thenReturn(documentValidXSD);

        validationProcess.validateXML(res, fileToValidate, schematron);

        assertTrue(res != null);
        assertTrue(res.getDocumentValidXSD().getResult().equals("FAILED"));
    }

    @Test
    public void validateXMLInvalidTest() {
        ObjectType objectType = new ObjectType();
        objectType.setKeyword("CDA");
        schematron.setObjectType(objectType);
        schematron.setName("ihe");
        schematron.setXsdPath("/path");

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File fileToValidate = new File(filePath);

        DetailedResult res = new DetailedResult();

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("FAILED");

        Mockito.when(xmlValidation.isXMLWellFormed(fileToValidate)).thenReturn(documentWellFormed);

        Mockito.when(xmlValidation.isFileValid(fileToValidate, schematron)).thenReturn(documentValidXSD);

        validationProcess.validateXML(res, fileToValidate, schematron);

        assertTrue(res != null);
        assertTrue(res.getDocumentValidXSD().getResult().equals("FAILED"));
    }

    @Test
    public void validateXMLValidXSDTest() {
        ObjectType objectType = new ObjectType();
        objectType.setKeyword("xml");
        schematron.setObjectType(objectType);
        schematron.setName("ihe");
        schematron.setXsdPath("/path");

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File fileToValidate = new File(filePath);

        DetailedResult res = new DetailedResult();

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("PASSED");


        Mockito.when(xmlValidation.isXMLWellFormed(fileToValidate)).thenReturn(documentWellFormed);
        Mockito.when(xmlValidation.isFileValid(fileToValidate, schematron)).thenReturn(documentValidXSD);

        validationProcess.validateXML(res, fileToValidate, schematron);

        assertTrue(res != null);
        assertTrue(res.getDocumentValidXSD().getResult().equals("PASSED"));
    }

    @Test
    public void validateXMLInvalidXSDTest() {
        ObjectType objectType = new ObjectType();
        objectType.setKeyword("xml");
        schematron.setObjectType(objectType);
        schematron.setName("ihe");
        schematron.setXsdPath("/path");

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File fileToValidate = new File(filePath);

        DetailedResult res = new DetailedResult();

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("FAILED");


        Mockito.when(xmlValidation.isXMLWellFormed(fileToValidate)).thenReturn(documentWellFormed);

        Mockito.when(xmlValidation.isFileValid(fileToValidate, schematron)).thenReturn(documentValidXSD);

        validationProcess.validateXML(res, fileToValidate, schematron);

        assertTrue(res != null);
        assertTrue(res.getDocumentValidXSD().getResult().equals("FAILED"));
    }

    @Test
    public void validateXMLNoXSDTest() {
        ObjectType objectType = new ObjectType();
        objectType.setKeyword("xml");
        schematron.setObjectType(objectType);
        schematron.setName("ihe");
        schematron.setXsdPath("");

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File fileToValidate = new File(filePath);

        DetailedResult res = new DetailedResult();

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("FAILED");


        Mockito.when(xmlValidation.isXMLWellFormed(fileToValidate)).thenReturn(documentWellFormed);
        Mockito.when(xmlValidation.handleException(Mockito.isA(String.class))).thenReturn(documentValidXSD);

        validationProcess.validateXML(res, fileToValidate, schematron);

        assertTrue(res != null);
        assertTrue(res.getDocumentValidXSD().getResult().equals("FAILED"));
    }

    @Test
    public void validateDocumentValidTest() throws IOException, JDOMException, TransformerException, ValidationException {
        String result = null;

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        String b64ObjToValidate = DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get(filePath)));

        ObjectType objectType = new ObjectType();
        objectType.setKeyword("CDA");
        schematron.setObjectType(objectType);
        schematron.setName("IHE - CARD - Registry Content Submission - Electrophysiology (RCS-EP)");
        schematron.setDfdlTransformationNeeded(false);

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("PASSED");

        Mockito.when(xmlValidation.isXMLWellFormed(Mockito.isA(File.class))).thenReturn(documentWellFormed);
        Mockito.when(xmlValidation.isFileValid(Mockito.isA(File.class), Mockito.isA(Schematron.class))).thenReturn(documentValidXSD);
        doNothing().when(schematronValidator).validate(Mockito.isA(DetailedResult.class), Mockito.isA(Schematron.class), Mockito.isA(File.class));
        Mockito.when(marshaller.getDetailedResultAsString(Mockito.isA(DetailedResult.class))).thenReturn("<detailedResult>");

        result = validationProcess.validateDocument(b64ObjToValidate, schematron, schematron.getName());
        assertTrue(result != null);
        assertTrue(result.contains("<detailedResult>"));

    }

    @Test
    public void validateDocumentValidTestNeedDfdl() throws IOException, JDOMException, TransformerException, ValidationException, CompilationException, TransformationException, ObjectNotFoundException {
        String result = null;

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        String b64ObjToValidate = DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get(filePath)));

        ObjectType objectType = new ObjectType();
        objectType.setKeyword("CDA");
        schematron.setObjectType(objectType);
        schematron.setName("IHE - CARD - Registry Content Submission - Electrophysiology (RCS-EP)");
        schematron.setDfdlTransformationNeeded(true);

        DocumentWellFormed documentWellFormed = new DocumentWellFormed();
        documentWellFormed.setResult("PASSED");
        DocumentValidXSD documentValidXSD = new DocumentValidXSD();
        documentValidXSD.setResult("PASSED");

        Mockito.when(dfdlTransformation.doDfdlTransformation(Mockito.isA(Schematron.class), Mockito.isA(byte[].class))).thenReturn("dfdlTransformation");
        Mockito.when(xmlValidation.isXMLWellFormed(Mockito.isA(File.class))).thenReturn(documentWellFormed);
        Mockito.when(xmlValidation.isFileValid(Mockito.isA(File.class), Mockito.isA(Schematron.class))).thenReturn(documentValidXSD);
        doNothing().when(schematronValidator).validate(Mockito.isA(DetailedResult.class), Mockito.isA(Schematron.class), Mockito.isA(File.class));
        Mockito.when(marshaller.getDetailedResultAsString(Mockito.isA(DetailedResult.class))).thenReturn("<detailedResult>");

        result = validationProcess.validateDocument(b64ObjToValidate, schematron, schematron.getName());
        assertTrue(result != null);
        assertTrue(result.contains("<detailedResult>"));
    }

    @Test
    public void validateDocumentInvalidTestNeedDfdl() throws IOException, JDOMException, TransformerException, ValidationException, CompilationException, TransformationException, ObjectNotFoundException {
        String result = null;

        String filePath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        String b64ObjToValidate = DatatypeConverter.printBase64Binary(Files.readAllBytes(Paths.get(filePath)));

        ObjectType objectType = new ObjectType();
        objectType.setKeyword("CDA");
        schematron.setObjectType(objectType);
        schematron.setName("IHE - CARD - Registry Content Submission - Electrophysiology (RCS-EP)");
        schematron.setDfdlTransformationNeeded(true);

        MalformedURLException e = new MalformedURLException();

        Mockito.when(dfdlTransformation.doDfdlTransformation(Mockito.isA(Schematron.class), Mockito.isA(byte[].class))).thenThrow(e);
        Mockito.when(marshaller.getDetailedResultAsString(Mockito.isA(DetailedResult.class))).thenReturn("Exception");

        result = validationProcess.validateDocument(b64ObjToValidate, schematron, schematron.getName());
        assertTrue(result != null);
        assertTrue(result.contains("Exception"));
    }
}