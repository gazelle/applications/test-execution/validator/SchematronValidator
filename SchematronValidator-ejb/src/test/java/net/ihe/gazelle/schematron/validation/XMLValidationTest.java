package net.ihe.gazelle.schematron.validation;

import net.ihe.gazelle.preferences.ApplicationConfigurationDAO;
import net.ihe.gazelle.schematron.model.Schematron;
import net.ihe.gazelle.schematron.util.EmailNotificationManager;
import net.ihe.gazelle.schematron.util.SchFilePath;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class XMLValidationTest {

    String resourcesDirectory;
    private static final String PASSED = "PASSED";

    private static final String FAILED = "FAILED";

    @Mock
    private SchFilePath schFilePath;

    @Mock
    private EmailNotificationManager emailNotificationManager;

    @Mock
    private ApplicationConfigurationDAO applicationConfigurationDAO;

    @InjectMocks
    private XMLValidation xMLValidation;

    private Schematron schematron;


    @Before
    public void setUp() throws Exception {
        resourcesDirectory = new String("src/test/resources/");
        schematron = new Schematron();

    }

    @Test
    public void isXMLWellFormedValidFileTest() {

        String xmlPath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File xmlFile = new File(xmlPath);
        DocumentWellFormed response = xMLValidation.isXMLWellFormed(xmlFile);

        assertTrue(response.getResult().equals(PASSED));
    }

    @Test
    public void isXMLWellFormedInvalidFileTest() {

        String xmlPath = resourcesDirectory + "FilesToValidate/fileNotWellFormed.xml";
        File xmlFile = new File(xmlPath);
        DocumentWellFormed response = xMLValidation.isXMLWellFormed(xmlFile);

        assertTrue(response.getResult().equals(FAILED));
    }

    @Test
    public void isXMLWellFormedNullFileTest() {

        DocumentWellFormed response = xMLValidation.isXMLWellFormed(null);

        assertTrue(response.getResult().equals(FAILED));
    }

    @Test
    public void reloadXSDValidFile() {
        String xsdLocation = resourcesDirectory + "XSD-from-wsdl/access_control-xacml-2.0-context-schema-os.xsd";
        Boolean response = xMLValidation.reloadXSD(xsdLocation);
        assertTrue(response);
    }

    @Test
    public void testAdrXDSRequestIsFileValid() throws Exception {

        String xml = resourcesDirectory + "XML/adr_req.xml";
        String xsd = resourcesDirectory + "XSD/access_control-xacml-2.0-saml-protocol-schema-os.xsd";
        File xmlFile = new File(xml);

        DocumentValidXSD response = xMLValidation.validXMLUsingXSD10(xmlFile, xsd);
        assertTrue(response.getResult().equals(PASSED));
    }

    @Test
    public void testAdrPPQRequestIsFileValid() throws Exception {

        String xml = resourcesDirectory + "XML/adr_due_to_PPQ_request_body.xml";
        String xsd = resourcesDirectory + "XSD/access_control-xacml-2.0-saml-protocol-schema-os.xsd";
        File xmlFile = new File(xml);

        DocumentValidXSD response = xMLValidation.validXMLUsingXSD10(xmlFile, xsd);
        assertTrue(response.getResult().equals(PASSED));
    }

    @Test
    public void isFileValidTest() {

        schematron.setXsdPath("/path");
        schematron.setXsdVersion("1.0");
        Mockito.when(schFilePath.getXSDPath(schematron)).thenReturn(resourcesDirectory + "XSD/access_control-xacml-2.0-saml-protocol-schema-os.xsd");
        String xml = resourcesDirectory + "XML/adr_due_to_PPQ_request_body.xml";

        DocumentValidXSD response = xMLValidation.isFileValid(new File(xml), schematron);
        assertTrue(response.getResult().equals(PASSED));
    }

    @Test
    public void isFileValidNullFileErrorTest() {

        schematron.setXsdPath("/path");
        schematron.setXsdVersion("1.0");

        DocumentValidXSD response = xMLValidation.isFileValid(null, schematron);
        assertTrue(response.getResult().equals(FAILED));
    }

    @Test
    public void isFileValidFileNotFoundTest() {

        schematron.setXsdPath("/path");
        schematron.setXsdVersion("1.0");
        Mockito.when(schFilePath.getXSDPath(schematron)).thenReturn("/path");
        String xml = resourcesDirectory + "XML/adr_due_to_PPQ_request_body.xml";

        DocumentValidXSD response = xMLValidation.isFileValid(new File(xml), schematron);
        assertTrue(response.getResult().equals(FAILED));
    }

    @Test
    public void isCDAValidForEpsosValidTest() {
        String cdaPath = resourcesDirectory + "FilesToValidate/cdaValid.xml";
        File cdaFile = new File(cdaPath);
        Mockito.when(applicationConfigurationDAO.getValue("epsos_cda_xsd_path")).thenReturn(resourcesDirectory + "xsd/cda/CDA_extended.xsd");

        DocumentValidXSD response = xMLValidation.isCDAValidForEpsos(cdaFile);

        assertTrue(response.getResult().equals(PASSED));
    }

    @Test
    public void isCDAValidForEpsosInvalidTest() {
        String cdaPath = resourcesDirectory + "FilesToValidate/cdaInvalid.xml";
        File cdaFile = new File(cdaPath);
        Mockito.when(applicationConfigurationDAO.getValue("epsos_cda_xsd_path")).thenReturn(resourcesDirectory + "xsd/cda/CDA_extended.xsd");

        DocumentValidXSD response = xMLValidation.isCDAValidForEpsos(cdaFile);

        assertTrue(response.getResult().equals(FAILED));

    }

    @Test
    public void isCDAValidForEpsosNoXSDFileTest() {
        String cdaPath = resourcesDirectory + "FilesToValidate/cdaInvalid.xml";
        File cdaFile = new File(cdaPath);
        Mockito.when(applicationConfigurationDAO.getValue("epsos_cda_xsd_path")).thenReturn(null);

        DocumentValidXSD response = xMLValidation.isCDAValidForEpsos(cdaFile);

        assertTrue(response.getResult().equals(FAILED));
    }

}